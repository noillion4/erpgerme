import 'dart:async';
import 'dart:html' as html;

import 'dart:html';

import '../classemetier/achat.dart';
import '../classemetier/Client.dart';
import '../classemetier/fournisseur.dart';
import '../classemetier/produit.dart';
import '../classemetier/users.dart';
import '../classemetier/vente.dart';
import '../db.dart';
import 'dart:convert';

int idfournisseur;
List<achat> listechat = new List();
/**
 * appeler au chargment de la page
 * affiche le formualire d'ajout de fournisseur
 **/
void loadgestfournisseur() {
  chargefounisseur();
  querySelector("#wall")
    ..innerHtml = '  <div class="wallleft" id="codep" >'
        '<h2>Ajouter un fournisseur</h2>'
        '<p>Nom :<input id="pname" type="text"></p>'
        '<p>Adresse :<input id="padress" type="text"></p>'
        '<p>code postal :<input id="pcp" type="text"></p>'
        '<p>Tel :<input id="ptel" type="tel"></p>'
        '<p>Email :<input id="pemail" type="email"></p>'
        '<p>Num tva :<input id="ptva" type="text"></p>'
        '<p>banque :<input id="pbanque" type="text"></p>'
        '<button id="addfourni"class="submit">Ajouter le fournisseur</button>'
        '</div>' +
        '<div class="wallright" id="art"><div id="sample_container_id">';
  querySelector("#addfourni").onClick.listen((e) => addfournisseur());
}

/**
 * appeler lord du click sur ajouter le fournisseur
 * ajoute le forunsieur dans la DB
 */
void addfournisseur() async {
  try {
    var four = new Fournisseur(
        0,
        (querySelector("#pbanque") as html.InputElement).value,
        (querySelector("#pname") as html.InputElement).value,
        (querySelector("#ptel") as html.InputElement).value,
        (querySelector("#ptva") as html.InputElement).value,
        (querySelector("#pemail") as html.InputElement).value,
        (querySelector("#pcp") as html.InputElement).value,
        (querySelector("#padress") as html.InputElement).value);
    makeRequest("addfourni", four.json());
    await makeRequest("lisfourni", "");

    loadgestfournisseur();
  } catch (e) {
    window.alert(
        "une erreur est survenue lors de l'ajout d'un fournisseur dans la db");
  }
}

/**
 * affiche la liste des forunisseur
 */
void chargefounisseur() async {
  String ra;

  try {
    ra = "<h2 class='t'>Les Fournisseur :</h2>";
    ra += '<div class ="flexrow">';

    int cont = 0;
    var rows2 = await makeRequest("lisfourni", "") as dynamic;
    List<Fournisseur> lfour = new List();
    List po = jsonDecode(rows2);
    po.forEach((f) => lfour.add(new Fournisseur.J(f)));
    for (var p in lfour) {
      ra += "<button id='four${p.Id}' class='article'  value='${p.Id}'>" +
          p.Nom +
          "</button> ";

      cont++;
    }

    ra += '</div>';
    querySelector("#art")..innerHtml = ra;
    for (var p in lfour) {
      querySelector("#four${p.Id}")
        ..onClick.listen((e) {
          idfournisseur = p.Id;
          listechat.clear();
          querySelector("#codep")
            ..innerHtml = '<div id="infofourni">'
                '<h2>Fournisseur :${p.Nom}</h2>'
                ' <p>Adresse :${p.Adresse}</p>'
                ' <p> Postal :${p.Cp}</p>'
                ' <p>Télephone :${p.Gsm}</p>'
                ' <p>Email ${p.Email}</p>'
                ' <p>Num tva ${p.Tva}</p>'
                ' <p>Banque :${p.Banque}</p></div>'
                // '<button id="addcateg"class="submit">Vente</button>'
                '<button id="btlisetfacture"class="submit">Facture</button>'
                '<button id="btupfacture"class="submit">Modifer Facture</button>'
                '<button id="btadfacture"class="submit">Ajouter Facture</button>'
                '<button id="upfourniseur"class="submit">Modifier Le fournisseur</button>'
                '<button id="btreturn"class="submit">Retour</button>';
          querySelector("#btupfacture").onClick.listen((e) => upfacture());
          querySelector("#btreturn")
              .onClick
              .listen((e) => loadgestfournisseur());
          querySelector("#btadfacture").onClick.listen((e) => btadfacture());
          querySelector("#btlisetfacture")
              .onClick
              .listen((e) => listefactureacha());
          querySelector("#upfourniseur").onClick.listen((e) => upfourniseur(p));
        });
    }
  } catch (e) {
    window.alert(
        "une erreur est survenue lors du chargement des fournisseurs dans la db");
  }
}

/**
 * affiche la page de mise ajour d'une facture d'un fournissuer
 */
void upfacture() async {
  String ra;

  try {
    ra = "<h2 class='t'>Les Factures :</h2>";
    ra += '<div class ="flexrow">';

    int cont = 0;
    var rows2 = await makeRequest("listefactureacha", idfournisseur) as dynamic;

    List po = jsonDecode(rows2);

    for (var p in po) {
      print(p['numfact']);
      ra +=
          "<button id='four${p['id']}' class='article'  value='${p['id']}'> Num facture : " +
              p['numfact'] +
              "</br> date : " +
              p['date'] +
              "</button> ";

      cont++;
    }

    ra += '</div>';
    querySelector("#art")..innerHtml = ra;
    for (var p in po) {
      querySelector("#four${p['id']}")
        ..onClick.listen((e) async {
          listechat.clear();
          List lisp = new List();
//
          var rows2 = await makeRequest("lisprodall", "a") as dynamic;
          List po = jsonDecode(rows2);
          po.forEach((p) => lisp.add(new produit.J(p)));
          var rows4 = await makeRequest("chargeachatfacture()", p['id']) as dynamic;
          print(rows4);
          List po4 = jsonDecode(rows4);
          po4.forEach((p) => listechat.add(new achat.J(p)));
          var opt = '<option value="0" selected>Aucun</option>';
          lisp.forEach(
              (f) => opt += '<option value="${f.Id}"> ${f.Nom} </option>');

          querySelector("#codep").innerHtml = '<h2>Ajouter une facture</h2>'
              '<p>N° Facture "${p['numfact']}"</p>'
              '<p>Payer : <select id="idcat"><option value="${p['payer']}" selected>${p['payer']}</option><option value="oui" >oui</option><option value="non">non</option></select></p>'
              '<div class="factfour">'
              '<p>Nom article :<input id ="noma" type="text" value="0" ></p>'
              '<p>Quantiter : <input id="qta" class="litinput"type="number"  min="0.0" step="0.1" value="0.0">TVA :<input id="tvaa" class="litinput" type="number" value="0"></p>'
              '<p>Prix :<input id ="prixa" type="number"  min="0.0" step="0.1" value="0.0"></p>'
              '<p>Type de plant : <select id="liena">$opt</select><button id="btcrearfact2">Ajouter article</button></p>'
              ''
              '</div>'
              '<div class="contart" id="listart"></div>'
              '<div class="flexrow"></p><h3 id="tothtva">Total HTVA: </h3>'
              '<button id="btcreatfactur" >Modifier la facture</button>'
              '</div>';
          querySelector("#btcreatfactur").onClick.listen((e)async{
            List lis = new List();
            listechat.forEach((f)=> lis.add(f.json()));
            print((querySelector("#idcat") as html.SelectElement).value);
            await makeRequest("upchatfacture()", p['id'].toString() + "&t="+jsonEncode(lis) +"&s="+(querySelector("#idcat") as html.SelectElement).value);
            print("ok");

          });

          Afficheachat();
        });
    }
  } catch (e) {
    window.alert("une erreur est survenue lors du chargement d'une facture");
  }
}

/**
 * ouvre la fenetre de modification de fournisseur
 */
void upfourniseur(Fournisseur p) {
  try {
    querySelector("#codep")
      ..innerHtml = '<h2>Modifier un fournisseur</h2>'
          '<p>Nom :<input id="pname" type="text" value="${p.Nom}"></p>'
          '<p>Adresse :<input id="padress" type="text" value="${p.Adresse}"></p>'
          '<p>code postal :<input id="pcp" type="text" value="${p.Cp}"></p>'
          '<p>Tel :<input id="ptel" type="tel" value="${p.Gsm}"></p>'
          '<p>Email :<input id="pemail" type="email" value="${p.Email}"></p>'
          '<p>Num tva :<input id="ptva" type="text" value="${p.Tva}"></p>'
          '<p>banque :<input id="pbanque" type="text" value="${p.Banque}"></p>'
          '<button id="upfourni"class="submit">Modifier le fournisseur</button>';
    querySelector("#upfourni").onClick.listen((e) async {
      p.Nom = (querySelector("#pname") as html.InputElement).value;
      p.Adresse = (querySelector("#padress") as html.InputElement).value;
      p.Cp = (querySelector("#pcp") as html.InputElement).value;
      p.Gsm = (querySelector("#ptel") as html.InputElement).value;
      p.Email = (querySelector("#pemail") as html.InputElement).value;
      p.Tva = (querySelector("#ptva") as html.InputElement).value;
      p.Banque = (querySelector("#pbanque") as html.InputElement).value;
      await makeRequest("upfourni", p.json());
      loadgestfournisseur();
    });
  } catch (e) {
    window.alert(
        "une erreur est survenue lors du chargement d'un  fournisseurs dans la db");
  }
}

/**
 * affiche les facture d'un fourniseur
 */
void listefactureacha() async {
  String ra;

  try {
    ra = "<h2 class='t'>Les Factures :</h2>";
    ra += '<div class ="flexrow">';

    int cont = 0;
    var rows2 = await makeRequest("listefactureacha", idfournisseur) as dynamic;
    print(rows2);

    List po = jsonDecode(rows2);

    for (var p in po) {
      print(p['numfact']);
      ra +=
          "<button id='four${p['id']}' class='article'  value='${p['id']}'> Num facture : " +
              p['numfact'] +
              "</br> date : " +
              p['date'] +
              "</button> ";

      cont++;
    }

    ra += '</div>';
    querySelector("#art")..innerHtml = ra;
    for (var p in po) {
      querySelector("#four${p['id']}")
        ..onClick.listen((e) async {
          querySelector("#art").innerHtml = '<div class="flexrow2">'
              '<div class="flexrow"><img src="logo.png"><div><h2>Ferme Paque Bartholomé</h2>'
              '<P>Rue de Liége,45</p>'
              '<p>4450 Lantin</p>'
              '<p>Téléphone:04 97 70 95 92</p></div></div><div>' +
              querySelector("#infofourni").innerHtml +
              '</div></div>'
              '<span class="factt"> Facture N° ${p['numfact']} Date : Le ${p['date'].substring(0, 10)}</span>';
          var n = await makeRequest("chargefactureacha", p['id']);
          print(n);
          var r = jsonDecode(n);
          var su = '<table>' +
              "<tr><td>Produit°</td><td>Quantiter</td><td>Total HTVA</td><td>TVA</td><td>Total</td></tr>";
          var thtva = 0, tva = 0, total = 0;
          for (var value in r) {
            su +=
                "<tr><td>${value['nom']}</td><td>${value['quatitier']}</td><td>${value['total'].toStringAsFixed(2)}€</td><td>${(value['ttva'] - value['total']).toStringAsFixed(2)}€</td><td>${value['ttva'].toStringAsFixed(2)}€</td></tr>";
            thtva += value['total'];
            tva += (value['ttva'] - value['total']);
            total += value['ttva'];
          }
          querySelector("#art").innerHtml += '$su' +
              '</table>' +
              '<span class="factt">TOTAL HTVA : ${thtva.toStringAsFixed(2)} € TVA : ${tva.toStringAsFixed(2)} € TOTAL : ${total.toStringAsFixed(2)}€</span>' +
              '<button  class="" id="btim" onclick="">Imprimer</button> ';
          querySelector("#btim").onClick.listen((e) {
            window.print();
          });
        });
    }
  } catch (e) {
    window.alert(
        "une erreur est survenue lors du chargement des Facture dans la db");
  }
}

/**
 * suprimer un achat lors de la creation d'une facture pour un fournisseur
 */
void chargsupr() {
  int cr = 0;
  for (achat value in listechat) {
    querySelector("#btsv$cr")
      ..onClick.listen((e) {
        listechat.remove(value);
        Afficheachat();
      });
    cr++;
  }
}

/**
 * bouton creer une facture pour un fournisseur
 */
void btadfacture() async {
  try {
    List lisp = new List();
    var jscat = await makeRequest("listecategorie", "a") as dynamic;
    var rows2 = await makeRequest("lisprodall", "a") as dynamic;
    List po = jsonDecode(rows2);
    po.forEach((p) => lisp.add(new produit.J(p)));
    var opt = '<option value="0" selected>Aucun</option>';
    lisp.forEach((f) => opt += '<option value="${f.Id}"> ${f.nom} </option>');
    List lcat = jsonDecode(jscat);
    var categori = "";
    lcat.forEach(
        (f) => categori += '<option value="${f['id']}"> ${f['nom']} </option>');
    querySelector("#codep").innerHtml = '<h2>Ajouter une facture</h2>'
        '<p>N° Facture<input id="numfact" type="text" ></p>'
        '<p>Date<input id="fdate" type="date" >'
        '<p>Categorie : <select id="idcat">$categori</select></p>'
        '<div class="factfour">'
        '<p>Nom article :<input id ="noma" type="text" value="0" ></p>'
        '<p>Quantiter : <input id="qta" class="litinput"type="number"  min="0.0" step="0.1" value="0.0">TVA :<input id="tvaa" class="litinput" type="number" value="0"></p>'
        '<p>Prix :<input id ="prixa" type="number"  min="0.0" step="0.1" value="0.0"></p>'
        '<p>Type de plant : <select id="liena">$opt</select><button id="btcrearfact2">Ajouter article</button></p>'
        ''
        '</div>'
        '<div class="contart" id="listart"></div>'
        '<div class="flexrow"></p><h3 id="tothtva">Total HTVA: </h3>'
        '<button id="btcreatfactu" >Creer la facture</button>'
        '</div>';
    btcreefacture();
  } catch (e) {
    window.alert("une erreur est survenue lors de la creation d'une facture");
  }
}

/**
 * gere les methode des bouton de creation de facture
 */
void btcreefacture() {
  querySelector('#btcrearfact2').onClick.listen((e) {
    print("test");
    btcrearfact();
  });
  querySelector("#btcreatfactu").onClick.listen((e) => btcreatfactachat());
}

/**
 * ajoute un achat a une facture d'un fournisseur
 */
void btcrearfact() {
//achat(id, nom, prix, idfacture, qt, tva, idproduit)

  int lien =
      int.tryParse((querySelector("#liena") as html.SelectElement).value);
  achat ach = new achat(
      null,
      (querySelector("#noma") as html.InputElement).value,
      (querySelector("#prixa") as html.NumberInputElement).valueAsNumber,
      0,
      (querySelector("#qta") as html.NumberInputElement).valueAsNumber,
      (querySelector("#tvaa") as html.NumberInputElement).valueAsNumber,
      lien);
  print(ach.Nom + ach.Idproduit.toString() + ach.Tva.toString());
  listechat.add(ach);
  Afficheachat();
}

/**
 * affiche les achats d'une facture en cours de creation
 */
void Afficheachat() async {
  querySelector("#listart")..innerHtml = "";
  double q;
  int p;
  double total = 0;
  var cr = 0;
  for (achat value in listechat) {
    querySelector("#listart")
      ..innerHtml +=
          "<p> ${value.Nom} ${value.Qt} X ${value.Prix.toStringAsFixed(2)}€ = ${(value.Prix * value.Qt).toStringAsFixed(2)} €<button id='btsv$cr' class='btts'>X</button></P>";
    total += value.Prix * value.Qt;
    cr++;
  }
  cr = 0;

  // Total =total;
  querySelector("#tothtva")
    ..innerHtml = "Total HTVA :  ${total.toStringAsFixed(2)} €";
  chargsupr();
}

/**
 * envoi la facture au serveur pour l'enregistrement dans la db
 */
void btcreatfactachat() async {
  try {
    var date = (querySelector("#fdate") as html.InputElement).value;
    var idcategorie =
        int.parse((querySelector("#idcat") as html.SelectElement).value);
    var payer = "n";
    var fact = (querySelector("#numfact") as html.InputElement).value;
    var jsonDataAsString = '''{

   "idategorie":$idcategorie,
   "date": "$date",
   "payer": "$payer",
   "idfournisseur":$idfournisseur,
   "numfact" :"$fact"
   
   
}''';
    List l = new List();
    listechat.forEach((f) => l.add(f.json()));
    makeRequest("addfactachat", "$jsonDataAsString &t=${jsonEncode(l)}");
    loadgestfournisseur();
    listechat.clear();
  } catch (e) {
    window.alert("une erreur est survenue lors de l'enregistrement dans la db");
  }
}
