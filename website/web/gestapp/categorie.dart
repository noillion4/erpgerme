import 'dart:async';
import 'dart:html' as html;
import 'dart:io';
import 'dart:html';
import 'dart:convert';
import '../db.dart';

/**
 * construit la page des categorie
 */
void charecategorie() async {
  querySelector("#wall")
    ..innerHtml = '  <div class="wallleft" id="codep" >'
        '<h2>Ajouter une Categorie</h2>'
        '<p>Nom :<input id="pname" type="text"></p>'
        ' <p>Type: <select id="pfroma" name="tva"><option value="1" selected>Vente</option><option value="2">Autre</option></select></P> '
        '<button id="addcateg"class="submit">Ajouter La categorie </button>'
        '</div>' +
        '<div class="wallright" id="art"><div id="sample_container_id">';
  btaddcat();
  charecategoriel();
}

/**
 * methode du bouton ajouter une categorie
 * appeler au chargement de la page
 */
void btaddcat() {
  querySelector("#addcateg").onClick.listen((e) async {


      await makeRequest(
          'addcategorie',
          "${(html.querySelector("#pname") as html.InputElement).value} &t=" +
              (html.querySelector("#pfroma") as html.SelectElement).value);
      charecategorie();


  });
}

/**
 * Affiche les categorie
 * Appeler au moment du chargement de la page
 */
void charecategoriel() async {
  String ra;

  ra = "<h2 class='t'>Les Categories :</h2>";
  ra += '<div class ="flexrow">';

  int cont = 0;
  try {
    var rows2 = await makeRequest("listecategorie", "a") as dynamic;
    List po = jsonDecode(rows2);

    for (var p in po) {
      var cat;
      if (p['type'] == 1)
        cat = "vente";
      else
        cat = "autre";

      ra += "<button id='cat$cont' class='article'  value='${p['id']}'>" +
          p['nom'] +
          "</br>" +
          cat +
          "</button> ";

      cont++;
    }

    ra += '</div>';
    querySelector("#art")..innerHtml = ra;

    for (var i = 0; i < po.length; i++) {
      querySelector("#cat$i")
        ..onClick.listen((e) {
          var type;
          if (po[i]['type'] == 1)
            type = "Vente";
          else
            type = "Autre";
          querySelector("#codep")
            ..innerHtml = '<h2>Ajouter une Categorie</h2>'
                '<p>Nom :<input id="pname" type="text"></p>'
                ' <p>Type: <select id="pfroma" name="tva"><option value="1" selected>Vente</option><option value="2">Autre</option></select></P> '
                '<button id="addcateg"class="submit">Ajouter La categorie </button>'
                '<h2>Modifer une Categorie</h2>'
                '<p>Nom :<input id="pname2" type="text" value="${po[i]['nom']}"></p>'
                ' <p>Type: <select id="pfroma2" name="tva"   ><option value="${po[i]['type']}" selected>$type</option><option value="1" >Vente</option><option value="2">Autre</option></select></P> '
                '<button id="modcateg"class="submit">Modifier La categorie </button>';
          btaddcat();
          querySelector("#modcateg").onClick.listen((e) async {
            if((html.querySelector("#pname2") as html.InputElement).value ==null)
            {

              throw new FormatException("le nom d'un categorie ne peut pas etre null");

            }
            if((html.querySelector("#pfroma2") as html.InputElement).value ==null)
            {

              throw new FormatException("le forma d'un categorie ne peut pas etre null");

            }
            await makeRequest(
                "upcategorie",
                po[i]['id'].toString() +
                    "&nom=${(html.querySelector("#pname2") as html.InputElement).value} &type=" +
                    (html.querySelector("#pfroma2") as html.SelectElement)
                        .value);

            charecategorie();
          });
        });
    }
  }
  on FormatException catch(e)
  {
    html.window.alert(e.toString());
  }


  catch (e) {
    window.alert("Une erreur est survenue lors du chargment des categories");
  }
}
