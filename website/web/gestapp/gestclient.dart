import 'dart:async';
import 'dart:html' as html;
import 'dart:html';
import '../classemetier/achat.dart';
import '../classemetier/Client.dart';
import '../classemetier/fournisseur.dart';
import '../classemetier/produit.dart';
import '../classemetier/users.dart';
import '../classemetier/vente.dart';
import '../db.dart';
import 'shop.dart';
import 'dart:io';
import 'post.dart';
import 'home.dart';
import 'gestproduit.dart';

import 'dart:convert';
import 'package:http/http.dart' as http;
import 'home.dart';

/**
 * gestion des client
 *
 *
 *
 *
 */
/**
 * declaration variable global
 */
double Total;
List<Vente> Lisa = new List();
List<produit> lis = new List();
int idcli = null;
int idclient = null;
var idcl;
/**
 * charge la fenetre des la gestion des clients
 * ps utiliser pour la gestion des clients
 */
void chargefichecli() async {
//  String nom ,prenom ,email;
//  int id ;
//  int gsm  ,tva ,pourcent;

  querySelector("#wall")
    ..innerHtml = '  <div class="wallleft" id="codep" >'
        '<p>Nom :<input id="nom" type="text"></p>'
        '<p>Prenom :<input id="pren" type="text"></p>'
        '<p>Email :<input id="em" type="email"></p>'
        '<p>GSM :<input id="gsm" type="tel"></p>'
        '<p>num tva :<input id="tva" type="text"></p>'
        '<p>Pourcentage :<input id="pour" type="number" value="0"></p>'
        ' <button  class="submit" id="addcli">Ajouter</button>'
        '</div>' +
        '<div class="wallright" id="art"><div id="sample_container_id">';

  chargelistcli();
  createcli();
}

/**
 * methode pour cree un client
 * ps utiliser pour la gestion des clients
 */
void createcli() {
  querySelector("#addcli").onClick.listen((e) async {
    try {

      String gsm = (querySelector("#gsm") as html.TelephoneInputElement).value;
      String tva = (querySelector("#tva") as html.InputElement).value;
      double pour =
          (querySelector("#pour") as html.NumberInputElement).valueAsNumber;
      print("$tva " + " $gsm " + " " + " $pour");
      await makeRequest(
          "createcli",
          new Client(
                  null,
                  (querySelector("#pren") as html.InputElement).value,
                  (querySelector("#nom") as html.InputElement).value,
                  gsm,
                  tva,
                  (querySelector("#em") as html.InputElement).value,
                  pour.toInt())
              .json());
      chargefichecli();
    }
      on FormatException catch(e)
    {
      html.window.alert(e.toString());
    } catch (e) {
      window.alert("Une erreur est survenue lors de la creation d'un client");
    }
  });
}

/**
 * charge la liste des clients et l'affiche et gere aussi la selection des clients et affiche les menu approprier
 * ps utiliser pour la gestino des clients
 */
void chargelistcli() async {
  String ra;

  List<Client> liscli = new List();
  var rows = await makeRequest("liscli", "a") as dynamic;
  List e = jsonDecode(rows);
  e.forEach((f) => liscli.add(new Client.J(f)));
  ra = "<h2 class='t'>Les Clients :</h2><div id='art'>";
  ra += '<div class ="flexrow">';

  int crt = 0;
  int cont = 0;
  for (var p in liscli) {
    ra += "<button id='cli$cont' class='article'  value='$p'>" +
        p.Nom +
        "</br>" +
        p.Prenom +
        "</button> ";
    if (crt >= 9) {
      //ra+="</div><div class='flexrow'>";
      crt = 0;
    }

    crt++;
    cont++;
  }

  ra += '</div>';
  querySelector("#art")..innerHtml = ra;

  for (var i = 0; i < liscli.length; i++) {
    querySelector("#cli$i")
      ..onClick.listen((e) {
        querySelector("#codep").innerHtml =
            '<div id="infocli"><h2 id="${liscli[i].Id}">Client : ${liscli[i].Nom}  ${liscli[i].Prenom}</h2><p>GSM : ${liscli[i].Gsm}</p><p>Email :${liscli[i].Email}</p><p>TVA: ${liscli[i].Tva}</p> <p>Pourcent: ${liscli[i].Pourcent}</p></div>'
            '<button class="submit" id="BtHome">Vente</button>'
            ' <button  class="submit" id="BTfact">Facture</button>'
            ' <button  class="submit" id="BTcom">Lister Commande/Bon de livraison</button>'
            ' <button  class="submit" id="BTcom2">modifer Commande/Bon de livraison</button>'
            ' <button  class="submit" id="ret">Retour</button>';
        testhelp();
        chargehomecli();
        BTfactu(liscli[i].Id);
        BTcom(liscli[i].Id);
        BTcom2(liscli[i].Id);
        Btvente();
      });
  }
}

/**
 * charge l'home d'un client
 * ps utiliser pour la gestion des client
 */
void Btvente() {
  querySelector("#BtHome").onClick.listen((e) {
    chargehomecli();
  });
}

/**
 * affiche la liste des commandes au click sur l'id BTcom et
 * affiche la commande au click sur l'id fact+index de  la commande dans la liste
 * @param 'l'id du client
 * ps utiliser pour la gestion des clients
 */
void BTcom(id) async {
  querySelector("#BTcom")
    ..onClick.listen((e) async {
      String ra;
      try {
        var rows = await makeRequest("listecommnoncli", id);
        var e = jsonDecode(rows);

        ra = "<h2 class='t'>Les Commandes :</h2><div id='art'>";
        ra += '<div class ="flexrow">';

        int crt = 0;
        int cont = 0;
        for (var p in e) {
          ra += "<button id='fact${cont}' class='article'  value='$p'>" ;
          if(p['blivraison']=="1")
            ra+= "Bon de Livraison N°";
          else
            ra +=  "Commande N°";

          ra+= "${p['id']}  "
              "date : " +
              "${p['date'].substring(0, 10)}" +
              "</br>" +
              "${p['Total'].toStringAsFixed(2)}€" +
              "</button> ";

          if (crt >= 9) {
            //ra+="</div><div class='flexrow'>";
            crt = 0;
          }

          crt++;
          cont++;
        }
//chargecom

        ra += '</div>';
        querySelector("#art")..innerHtml = ra;
        var i = 0;
        for (var p in e) {
          querySelector("#fact$i")
            ..onClick.listen((e) async {
              var com;
              if(p['blivraison']=="1")
                com= "Bon de Livraison N°";
              else
                com=  "Commande N°";
              querySelector("#art").innerHtml = '<div class="flexrow2">'
                  '<div class="flexrow"><img src="logo.png"><div><h2>Ferme Paque Bartholomé</h2>'
                  '<P>Rue de Liége,45</p>'
                  '<p>4450 Lantin</p>'
                  '<p>Téléphone:04 97 70 95 92</p></div></div><div>' +
                  querySelector("#infocli").innerHtml +
                  '</div></div>'
                  '<span class="factt"> $com ${p['id']} Date : Le ${p['date'].substring(0, 10)}</span>';
              var n = await makeRequest("chargecom", p['id']);
              print(n);
              var r = jsonDecode(n);
              var su = '<table>' +
                  "<tr><td>Produit°</td><td>Quantiter</td><td>Total HTVA</td><td>TVA</td><td>Total</td></tr>";
              var thtva = 0, tva = 0, total = 0;
              for (var value in r) {
                su +=
                    "<tr><td>${value['nom']}</td><td>${value['quatitier']}</td><td>${value['total'].toStringAsFixed(2)}€</td><td>${(value['ttva'] - value['total']).toStringAsFixed(2)}€</td><td>${value['ttva'].toStringAsFixed(2)}€</td></tr>";
                thtva += value['total'];
                tva += (value['ttva'] - value['total']);
                total += value['ttva'];
              }
              querySelector("#art").innerHtml += '$su' +
                  '</table>' +
                  '<span class="factt">TOTAL HTVA : ${thtva.toStringAsFixed(2)} € TVA : ${tva.toStringAsFixed(2)} € TOTAL : ${total.toStringAsFixed(2)}€</span>' +
                  '<button  class="" id="btim" onclick="">Imprimer</button> ';
              querySelector("#btim").onClick.listen((e) {
                window.print();
              });
            });
          i++;
        }
      } catch (e) {
        window.alert("Une erreur est survenue lors des commandes");
      }
    });
}

/**
 * Afficha la liste des commande pouvent etre modifier
 * ps utiliser pour la gestion des clients
 */
void BTcom2(id) async {
  querySelector("#BTcom2")
    ..onClick.listen((e) async {
      Listemodificationcom(id);
      idcl = id;
    });
}

/**
 * Afficha la liste des commande pouvent etre modifier
 * pas utiliser pour la gestion des clients
 */
void Listemodificationcom(id) async {
  String ra;

  var rows = await makeRequest("listecommnoncli2", id);
  var e = jsonDecode(rows);

  ra =
      "<h2 class='t'>Les Commandes :</h2><button id ='Btfacturer'>facturer</button><button id='Btsupr'>Supprimer</button><div id='art'>";
  ra += '<div class ="flexrow">';

  int crt = 0;
  int cont = 0;
  for (var p in e) {
    ra += "<button id='fact${cont}' class='article'  value='$p'>" ;

    if(p['blivraison']=="1")
      ra+= "Bon de Livraison N°";
    else
      ra +=  "Commande N°";

       ra+= "${p['id']}  " +
        "date : " +
        "${p['date'].substring(0, 10)}" +
        "</br>" +
        "${p['Total'].toStringAsFixed(2)}€" +
        "</button> " +
        ' <input type="checkbox" id="check$cont" class="checkb" name="scales">';
    if (crt >= 9) {
      //ra+="</div><div class='flexrow'>";
      crt = 0;
    }

    crt++;
    cont++;
  }

  ra += '</div>';
  querySelector("#art")..innerHtml = ra;
  btfacturer(e);
  btsupcomande(e);
}

/**
 * bouton supprimer une commande
 * ps utiliser dans gestions des clients
 */
void btsupcomande(e) {
  html.querySelector("#Btsupr").onClick.listen((f) async {
    try {
      var cont = 0;
      var id_client;
      List v = new List();
      for (var p in e) {
        print((html.querySelector("#check$cont") as html.CheckboxInputElement)
            .checked);
        if ((html.querySelector("#check$cont") as html.CheckboxInputElement)
                .checked ==
            true) {
          await makeRequest('supprcommande', p['id']);
        }

        cont++;
      }

      Listemodificationcom(idcl);
    } catch (e) {
      window.alert("Une erreur est survenue lors de la suppression");
    }
  });
  int cont = 0;
  for (var value in e) {
    print(cont);
    html.querySelector('#fact${cont}').onClick.listen((e) {
      loadupcomm(value['id']);
    });

    cont++;
  }
}

/**
 * utiliser pour facturer des commandes
 * ps utiliser dans gestion des clients
 */
void btfacturer(e) async {
  html.querySelector("#Btfacturer ").onClick.listen((f) async {
    try {
      var cont = 0;
      var id_client;
      List v = new List();
      for (var p in e) {
        print((html.querySelector("#check$cont") as html.CheckboxInputElement)
            .checked);
        if ((html.querySelector("#check$cont") as html.CheckboxInputElement)
                .checked ==
            true) {
          print(p);
          v.add(p);
          print(v);
          id_client = p['idclient'];
        }

        cont++;
      }
      var idf = await makeRequest(
          'facturerdescommande', jsonEncode(v) + '&ci=$id_client');
      print(idf);
      afficherunefacture(jsonDecode(idf), 'non');
    } catch (e) {
      window.alert(
          "Une erreur est survenue lors de la facturation d'une commande");
    }
  });
}

/**
 * bouton pour afficher la liste des factures
 * @param id du client
 * ps utiliser pour la gestion des clients s
 */
void BTfactu(id) async {
  querySelector("#BTfact")
    ..onClick.listen((e) async {
      afficherfacture(id);
    });
}

/**
 * affiche la liste des facture pour un client donner
 * @param l'id du client
 * ps utiliser pour la gestion des clients
 */
void afficherfacture(id) async {
  String ra;
  try {
    var rows = await makeRequest("listefactcli", id);
    var e = jsonDecode(rows);

    ra = "<h2 class='t'>Les Factures :</h2><div id='art'>";
    ra += '<div class ="flexrow">';

    int crt = 0;
    int cont = 0;
    for (var p in e) {
      var pa;
      if (p['payer'] == "o")
        pa = "Oui";
      else
        pa = "Non";
      ra += "<button id='fact${cont}' class='article'  value='$p'>" +
          "Facture N°" +
          "${p['id']}  " +
          "date : " +
          "${p['date'].substring(0, 10)}" +
          "</br>Payer: " +
          pa +
          "</br>" +
          "</button> ";
      if (crt >= 9) {
        //ra+="</div><div class='flexrow'>";
        crt = 0;
      }

      crt++;
      cont++;
    }

    ra += '</div>';
    querySelector("#art")..innerHtml = ra;

    var i = 0;
    for (var p in e) {
      querySelector("#fact$i")
        ..onClick.listen((e) async {
          if (p['payer'] == "o") {
            afficherunefacture(p, 'oui');
          } else {
            afficherunefacture(p, 'non');
          }
        });
      i++;
    }
  } catch (e) {
    window.alert("Une erreur est survenue lors du chargement d'une facutre");
  }
}

/**
 * affiche une facture
 * @param p = la facture
 * @param pay = payer(oui ou non)
 * ps utiliser pour la gestion des clients
 */
void afficherunefacture(p, pay) async {
  try {
    querySelector("#art").innerHtml = '<div class="flexrow2">'
        '<div class="flexrow"><img src="logo.png"><div><h2>Ferme Paque Bartholomé</h2>'
        '<P>Rue de Liége,45</p>'
        '<p>4450 Lantin</p>'
        '<p>Téléphone:04 97 70 95 92</p></div></div><div>' +
        querySelector("#infocli").innerHtml +
        '</div></div>'
        '<span class="factt"> facture N° ${p['id']} Date : Le ${p['date'].substring(0, 10)}</span>';
    var n = await makeRequest("Comoffacture", p['id']);
    print(n);
    var r = jsonDecode(n);
    var su = '<table>' +
        "<tr><td>Bon de Livraison N°</td><td>Date</td><td>Total HTVA</td><td>TVA</td><td>Total</td></tr>";
    var thtva = 0, tva = 0, total = 0;
    for (var value in r) {
      su +=
          "<tr><td>${value['id']}</td><td>${value['date'].substring(0, 10)}</td><td>${value['total'].toStringAsFixed(2)}€</td><td>${(value['totaltva'] - value['total']).toStringAsFixed(2)}€</td><td>${value['totaltva'].toStringAsFixed(2)}€</td></tr>";
      thtva += value['total'];
      tva += (value['totaltva'] - value['total']);
      total += value['totaltva'];
    }
    querySelector("#art").innerHtml += '$su' +
        '</table>' +
        '<span class="factt">TOTAL HTVA : ${thtva.toStringAsFixed(2)} € TVA : ${tva.toStringAsFixed(2)} € TOTAL : ${total.toStringAsFixed(2)}€</span>' +
        '<span class="factt">Payer : $pay</span>'
        '<button  class="hid" id="btim" >Imprimer</button>'
        '';
    if (pay != 'oui')
      querySelector("#art").innerHtml +=
          '<button  class="hid" id="btpayer">Marquer comme payer</button> ';

    querySelector("#btim").onClick.listen((e) {
      window.print();
    });
    btpayerfact(p);
  } catch (e) {
    window.alert("Une erreur est survenue lors du chargement d'une commande");
  }
}

/**
 * methode du bouton btpayer affiche la facture
 */
btpayerfact(p) {
  try {
    querySelector("#btpayer").onClick.listen((e) async {
      makeRequest("upfactpayer", p['id']);
      afficherunefacture(p, 'oui');
    });
  } catch (e) {
    window
        .alert("Une erreur est survenue lors de la modification d'une facture");
  }
}
/**
 * charge l'home d'un client (vente )
 * ps utiliser pour la gestion des clients
 */

void chargehomecli() {
  var now = new DateTime.now();
  var form2;

  var form = "${now.year.toString()}-0${now.month.toString()}-01";
  if (now.month < 11)
    form2 = "${now.year.toString()}-0${(now.month + 1).toString()}-01";
  else {
    form2 = "${(now.year + 1).toString()}-01-01";
  }
  print(form);
  querySelector("#art")
    ..innerHtml = ' ' +
        '<div class="btaff" > '
        '<input id ="datedeb" type="date" value="${form}">'
        ''
        '<input id ="datefin"type="date" value="$form2"  placeholder="$form">'
        '<button id="btaffi">Afficher</button>'
        ''
        '</div>';

  homecli();
}

/**
 * charge l'home d'un client (vente )
 * ps utiliser pour la gestion des clients
 */
void homecli() async {
  try {
    var ret = await makeRequest('factdate2',
        '${(querySelector("#datefin") as html.DateInputElement).valueAsDate}&i=${(querySelector("#datedeb") as html.DateInputElement).valueAsDate} &c=${querySelector("#codep h2").getAttribute("id")}');
    var R = jsonDecode(ret);
    double toth = 0, totc = 0, tva = 0;
    var now = new DateTime.now();
    var form2;

    var form = "${now.year.toString()}-0${now.month.toString()}-01";
    if (now.month < 11)
      form2 = "${now.year.toString()}-0${(now.month + 1).toString()}-01";
    else {
      form2 = "${(now.year + 1).toString()}-01-01";
    }

    var a = ' ' +
        '<div class="btaff" > '
        '<input id ="datedeb" type="date" value="${form}">'
        ''
        '<input id ="datefin"type="date" value="$form2"  placeholder="$form">'
        '<button id="btaffi">Afficher</button>'
        ''
        '</div>';
    ;
    a +=
        "<div class='col'><div class='tab'><div class='fact'><H2>FACTURER et PAYER</H2>"
        "<table><tr><th>NOM</th><th>QUATITER</th><th>TOTAL HTVA</th><th>TOTAL TVAC</th></tr>";
    for (var value in R) {
      a +=
          "<tr><td>${value['nom']}</td> <td>${value['quatitier']}/${value['forma']}</td> <td>${value['total']}€</td ><td>${value['ttva'].toStringAsFixed(2)}€</td></tr>";
      toth += value['total'];
      totc += value['ttva'];
      tva += value['ttva'] - value['total'];
    }

    a +=
        " </table> <h4>TOTAL HTVA : $toth € </h4><h4> TOTAL TVAC :  ${totc.toStringAsFixed(2)}€ </h4><h4>TOTAL TVA ${tva.toStringAsFixed(2)} €</h4></div>";

    ret = await makeRequest('factdatenon2',
        '${(querySelector("#datefin") as html.DateInputElement).valueAsDate}&i=${(querySelector("#datedeb") as html.DateInputElement).valueAsDate} &c=${querySelector("#codep h2").getAttribute("id")}');
    R = jsonDecode(ret);
    toth = 0;
    totc = 0;
    tva = 0;
    a += "<div class='fact'><H2>FACTURER et NON PAYER</H2>"
        "<table><tr><th>NOM</th><th>QUATITER</th><th>TOTAL HTVA</th><th>TOTAL TVAC</th></tr>";
    for (var value in R) {
      a +=
          "<tr><td>${value['nom']}</td> <td>${value['quatitier']}/${value['forma']}</td> <td>${value['total']}€</td ><td>${value['ttva'].toStringAsFixed(2)}€</td></tr>";
      toth += value['total'];
      totc += value['ttva'];
      tva += value['ttva'] - value['total'];
    }

    a +=
        "</table> <h4>TOTAL HTVA : $toth € </h4><h4> TOTAL TVAC :  ${totc.toStringAsFixed(2)}€ </h4><h4>TOTAL TVA ${tva.toStringAsFixed(2)} €</h4></div>";

    ret = await makeRequest('commnon2',
        '${(querySelector("#datefin") as html.DateInputElement).valueAsDate}&i=${(querySelector("#datedeb") as html.DateInputElement).valueAsDate} &c=${querySelector("#codep h2").getAttribute("id")}');
    R = jsonDecode(ret);
    toth = 0;
    totc = 0;
    tva = 0;
    a += "<div class='fact'><H2>COMMANDE NON FACTURER</H2>"
        "<table><tr><th>NOM</th><th>QUATITER</th><th>TOTAL HTVA</th><th>TOTAL TVAC</th></tr>";
    for (var value in R) {
      a +=
          "<tr><td>${value['nom']}</td> <td>${value['quatitier']}/${value['forma']}</td> <td>${value['total']}€</td ><td>${value['ttva'].toStringAsFixed(2)}€</td></tr>";
      toth += value['total'];
      totc += value['ttva'];
      tva += value['ttva'] - value['total'];
    }

    a +=
        "</table> <h4>TOTAL HTVA : $toth € </h4><h4> TOTAL TVAC :  ${totc.toStringAsFixed(2)}€ </h4><h4>TOTAL TVA ${tva.toStringAsFixed(2)} €</h4></div>";
    querySelector("#art").innerHtml = a +
        '</div><button  class="submit2"  id="btim" onclick="">Imprimer</button></div> ';
    querySelector("#btaffi")
      ..onClick.listen((e) {
        homecli();
      });
    querySelector("#btim").onClick.listen((e) {
      window.print();
    });
  } catch (e) {
    window.alert("Une erreur est survenue lors du chargement de la page");
  }
}

/**
 * gere un bouton retourn sur la gestion des clients
 * ps utiliser pour la gestion des clients
 */
void testhelp() {
  querySelector("#ret")
    ..onClick.listen((e) {
      chargefichecli();
    });
}

/**
 * //charge les produit et lesaffiche
 * ps utiliser pour la gestion des client
 */
void loadprod3(id) async {
  try {
    lis.clear();
    String ra;

    ra =
        "<h2 class='t'> Les Produits :</h2><div class='sercrch'><input id='serch'type='text' placeholder='ex: Pomme , Poivron , tommate etc .....'> <input type='button' value='rechercher' id='finda'></div><div id ='clav'></div><div id='art'>";
    ra += '<div class ="flexrow">';

    var rows2 = await makeRequest("lisprod", "a") as dynamic;

    List po = jsonDecode(rows2);
    po.forEach((p) => lis.add(new produit.J(p)));
    int crt = 0;
    int cont = 0;
    for (var p in lis) {
      ra += "<button id='a$cont' class='article'  value='$p'>" +
          p.Nom +
          "</br>" +
          p.Prix.toStringAsFixed(2) +
          "€ /" +
          p.Forma +
          "</button> ";
      if (crt >= 9) {
        //ra+="</div><div class='flexrow'>";
        crt = 0;
      }

      crt++;
      cont++;
    }
    ra += '</div>';
    querySelector("#art")..innerHtml = ra;
    querySelector("#codep")
      ..innerHtml = "<div id='bl'>" +
          "</div>"
          "<div id='idtot'></div> <div id='bTmod'><input type='button' id='Btupdcom' class ='submit'value='Modifier'></divdiv>";

    for (var i = 0; i < lis.length; i++) {
      querySelector("#a$i")
        ..onClick.listen((e) {
          clavier2(querySelector("#a$i"), lis, i);
        });
    }
    // gere le clavier de la recherche
    clavierserch();
    serchart4();
    Afficheprodcom(Lisa);
    Btupdcom(id);
    // gere les moyent de payment
  } catch (e) {
    window.alert("Une erreur est survenue lors du chargement des produit");
  }
}

/**
 * methode du bouton update commande utiliser pour mettre a jour une commande
 */
void Btupdcom(id) {
  try {
    html.querySelector("#Btupdcom").onClick.listen((e) async {
      List l = new List();
      for (var value in Lisa) {
        if(value.Qt==null) {
          html.window.alert("la quatiter ne peut pas etre null");
          throw new FormatException("la quatiter ne peut pas etre null");
        }
        if(value.Prix==null) {
          html.window.alert("la prix ne peut pas etre null");
          throw new FormatException("la prix ne peut pas etre null");
        }
        l.add(value.json());
      }
      if (await makeRequest('upcommand', '${jsonEncode(l)}&i=$id') == 1) ;
      {
        print(id);
        refresh(id);
      }
    });
  }
  on FormatException catch(e)
  {
    html.window.alert(e.toString());
  }
  catch (e) {
    window
        .alert("Une erreur est survenue lors de la mise à jour d'une commande");
  }
}

/**
 * recharge la page
 */
void refresh(id) {
  chargefichecli();
}

/**
 * charge une commande pour la modification
 */
void loadupcomm(id) async {
  try {
    print(id);
    Lisa.clear();
    var json = await makeRequest('chargecomby', '$id');
    print(json);
    var r = jsonDecode(json);
    for (var value in r) {

      Lisa.add(Vente.J(value));
    }
    loadprod3(id);
  }
  on FormatException catch(e)
  {
    html.window.alert(e.toString());
  }catch (e) {
    window.alert("Une erreur est survenue lors du chargement de la commande");
  }
}

/**
 * afficher la commande selectionner
 * ps utiliser pour la gestion des clients
 * @param List Lisa  la liste des produis de la commande
 */
void Afficheprodcom(Lisa) async {
  querySelector("#bl")..innerHtml = "";
  double q;
  int p;
  double total = 0;
  var cr = 0;
  for (Vente value in Lisa) {
    querySelector("#bl")
      ..innerHtml +=
          "<p> ${value.Nom} <input type='text' value='${value.Qt}'class='inputupc'>  X ${value.Prix.toStringAsFixed(2)}€ = ${(value.Prix * value.Qt).toStringAsFixed(2)} €<button id='btsv$cr' class='btts'>X</button></P>";
    total += value.Prix * value.Qt;
    cr++;
  }
  cr = 0;

  Total = total;
  querySelector("#idtot")
    ..innerHtml = "<h2>Total :  ${total.toStringAsFixed(2)} €</H2>";
  // charg();
  charg3();
}

/**
 * affiche un clavier numerique utiliser pour la modification d'une commande
 */
void clavier2(html.Element e, lis, i) {
  querySelector("#bl")
    ..innerHtml = '<input type="number"  id="quatitier"  step="0.01">' +
        '<div>' +
        "<button id='bt1'>1</button>" +
        "<button id='bt2'>2</button>" +
        "<button id='bt3'>3</button>" +
        '</div>' +
        "<div><button id='bt4'>4</button>" +
        "<button id='bt5'>5</button>" +
        "<button id='bt6'>6</button>" +
        '</div>' +
        "<div><button id='bt7'>7</button>" +
        "<button id='bt8'>8</button>" +
        "<button id='bt9'>9</button>" +
        '</div>' +
        '<div>'
        "<button id='btsu'>X</button>" +
        "<button id='bt0'>0</button>" +
        "<button id='valid'>V</button>" +
        '</div>' +
        '<div><button id="btdiv1">XX.X</button></div>' +
        '<div><button id="btdiv">XX.XX</button></div>' +
        '<div><button id="btmulti1">X</button></div>' +
        '<div><button id="btmulti">XX</button></div>';

  (querySelector("#quatitier") as InputElement).value = '0';
  querySelector("#btdiv1")
    ..onClick.listen((e) {
      (querySelector("#quatitier") as NumberInputElement).valueAsNumber =
          (querySelector("#quatitier") as NumberInputElement).valueAsNumber /
              10;
    });
  querySelector("#btdiv")
    ..onClick.listen((e) {
      (querySelector("#quatitier") as NumberInputElement).valueAsNumber =
          (querySelector("#quatitier") as NumberInputElement).valueAsNumber /
              100;
    });
  querySelector("#btmulti")
    ..onClick.listen((e) {
      (querySelector("#quatitier") as NumberInputElement).valueAsNumber =
          (querySelector("#quatitier") as NumberInputElement).valueAsNumber *
              100;
    });
  querySelector("#btmulti1")
    ..onClick.listen((e) {
      (querySelector("#quatitier") as NumberInputElement).valueAsNumber =
          (querySelector("#quatitier") as NumberInputElement).valueAsNumber *
              10;
    });

  querySelector("#bt1")
    ..onClick.listen((e) {
      (querySelector("#quatitier") as InputElement).value += '1';
    });
  querySelector("#btsu")
    ..onClick.listen((e) {
      Afficheprodcom(Lisa);
    });
  querySelector("#bt2")
    ..onClick.listen((e) {
      (querySelector("#quatitier") as InputElement).value += '2';
    });
  querySelector("#bt3")
    ..onClick.listen((e) {
      (querySelector("#quatitier") as InputElement).value += '3';
    });
  querySelector("#bt4")
    ..onClick.listen((e) {
      (querySelector("#quatitier") as InputElement).value += '4';
    });
  querySelector("#bt5")
    ..onClick.listen((e) {
      (querySelector("#quatitier") as InputElement).value += '5';
    });
  querySelector("#bt6")
    ..onClick.listen((e) {
      (querySelector("#quatitier") as InputElement).value += '6';
    });
  querySelector("#bt7")
    ..onClick.listen((e) {
      (querySelector("#quatitier") as InputElement).value += '7';
    });
  querySelector("#bt8")
    ..onClick.listen((e) {
      (querySelector("#quatitier") as InputElement).value += '8';
    });
  querySelector("#bt9")
    ..onClick.listen((e) {
      (querySelector("#quatitier") as InputElement).value += '9';
    });
  querySelector("#bt0")
    ..onClick.listen((e) {
      (querySelector("#quatitier") as InputElement).value += '0';
    });
  querySelector("#valid")
    ..onClick.listen((e) {
      try {
        double q =
            (querySelector("#quatitier") as NumberInputElement).valueAsNumber;
        double p = lis[i].Prix;
        if((q<1 || p<1))
        {
          html.window.alert("LA Quantiter ou le prix entree est incorect ");
          throw new FormatException("LA Quantiter ou le prix  entree est incorect ");

        }
        int ca = q ~/ lis[i].Nb;
        Lisa.add(new Vente(
            0,
            lis[i].Id,
            lis[i].Nom,
            p,
            lis[i].Forma,
            0,
            q,
            0,
            ca));
        Afficheprodcom(Lisa);
      }
      on FormatException catch(e)
      {
        html.window.alert(e.toString());
      }
      catch(e)

      {}
    });
}

void charg3() {
  int cr = 0;
  for (Vente value in Lisa) {
    querySelector("#btsv$cr")
      ..onClick.listen((e) {
        Lisa.remove(value);
        Afficheprodcom(Lisa);
      });
    cr++;
  }
}

/**
 * // s'occupe de la recherche de produit
 * ps utiliser pour la gestion des cient
 */
void serchart4() async {
  try {
    String ra;
    querySelector("#finda")
      ..onClick.listen((e) async {
        List<produit> list2 = new List<produit>();
        ra =
            "<h2 class='t'>  Les Produits :</h2><div class='sercrch'><input id='serch'type='text' placeholder='ex: Pomme , Poivron , tommate etc .....'> <input type='button' value='rechercher' id='finda'></div><div id ='clav'></div><div id='art'>";
        ra += '<div class ="flexrow">';
        var rows = await makeRequest(
                "findprod", (querySelector("#serch") as InputElement).value)
            as dynamic;
        clavierserch();
        List po = jsonDecode(rows);
        po.forEach((p) => list2.add(new produit.J(p)));
        int crt = 0;
        int cont = 0;
        for (var p in list2) {
          ra += "<button id='a$cont' class='article'  value='$p'>" +
              p.Nom +
              "</br>" +
              p.Prix.toStringAsFixed(2) +
              "€ /" +
              p.Forma +
              "</button> ";

          crt++;
          cont++;
        }

        ra += '</div></div>';
        querySelector("#art")..innerHtml = ra;

        for (var i = 0; i < list2.length; i++) {
          querySelector("#a$i")
            ..onClick.listen((e) {
              clavier2(querySelector("#a$i"), list2, i);
            });
        }
        serchart();
        chargercli();
      });
  } catch (e) {
    window.alert("Une erreur est survenue lors du chargement des produits");
  }
}
