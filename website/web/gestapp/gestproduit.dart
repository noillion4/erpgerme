import 'dart:async';
import 'dart:html' as html;

import 'dart:html';

import 'shop.dart';

import '../classemetier/achat.dart';
import '../classemetier/Client.dart';
import '../classemetier/fournisseur.dart';
import '../classemetier/produit.dart';
import '../classemetier/users.dart';
import '../classemetier/vente.dart';
import '../db.dart';
import 'dart:convert';

/**
 * gestion des produit
 * declaration
 */
double Total;
List<Vente> Lisa = new List();
List<produit> lis = new List();
int idcli = null;
int idclient = null;

var idcl;

/**
 * affichage de l'ajout d'un produi
 * ps utiliser pour la gestion des produits
 */
void chareupd() async {
  try {
    List<produit> lisp = new List<produit>();
    //listecategorie
    var rows2 = await makeRequest("lisprod2", "a") as dynamic;
    var jscat = await makeRequest("listecategorie2", "a") as dynamic;
    print(jscat);
    List lcat = jsonDecode(jscat);
    List po = jsonDecode(rows2);
    po.forEach((p) => lisp.add(new produit.J(p)));
    var opt = "";
    var categori = "";
    lisp.forEach((f) => opt += '<option value="${f.Id}"> ${f.Nom} </option>');
    lcat.forEach(
        (f) => categori += '<option value="${f['id']}"> ${f['nom']} </option>');

    querySelector("#wall")
      ..innerHtml = '  <div class="wallleft" id="codep" >'
          '<h2>Ajouter un produit</h2>'
          '<p>Nom :<input id="pname" type="text"></p>'
          '<p>Prix :<input  id ="pprix" min="0.0" step="0.1" value="0.0" type="number">€</p>'
          '<p>Caisse :<input id ="ptind"type="text"></p>'
          '<p>Quantiter par caisse :<input  id ="pqc" min="0.0" step="0.1" value="0.0" type="number"></p>'
          ' <p>Format: <select id="pforma" name="tva"><option value="kg" selected>kg</option><option value="pc">pc</option><option value="botte">botte</option><option value="ravier">ravier</option></select></P> '
          ' <P>Categorie : <select id ="pcat" name="tva">$categori</select></P> '


          ' <P>TVA :<select id ="ptva"name="tva"><option value="0" selected>0%</option><option value="6">6%</option><option value="12">12%</option><option value="21">21%</option></select></P> '

          '<button id="padprodt"class="submit">Ajouter Le produit </button>'
          '<h2>Reactiver un Produit</h2>'
          '<select id="activation">$opt</select>'
          '<button class="submit" id="btactive">Activer le produit </button>'
          '</div>' +
          '<div class="wallright" id="art"><div id="sample_container_id">';
    querySelector("#padprodt").onClick.listen((e) {
      produit p;
      int tva =
          int.tryParse((querySelector("#ptva") as html.SelectElement).value);
      int cate =
          int.tryParse((querySelector("#pcat") as html.SelectElement).value);
      if((querySelector("#pname") as html.NumberInputElement).valueAsNumber ==null)
      {
        throw new FormatException("Le nom ne peut pas etre null");
      }
          if((querySelector("#pprix") as html.NumberInputElement).valueAsNumber ==null)
            {
              throw new FormatException("Le prix ne peut pas etre null");
            }
          if((querySelector("#pprix") as html.NumberInputElement).valueAsNumber<=0)
            {
              html.window.alert("!!!!!!!!!!!!!!!!Attention !!!!!!!!!!!!!!! \n Vous Avez entré un prix Inferieur ou egale a 0 \n  Cela est deconseiller \n le produit a été crée Veiller le modifer !!!!!!!");
            }
      p = new produit(
          0,
          (querySelector("#pname") as html.InputElement).value,
          (querySelector("#pprix") as html.NumberInputElement).valueAsNumber,
          (querySelector("#pforma") as html.SelectElement).value,
          (querySelector("#ptind") as html.InputElement).value,
          tva,
          cate,(querySelector("#pqc") as html.NumberInputElement).valueAsNumber);
      makeRequest("addp", p.json());

      chareupd();
    });
    charearicltforup();
    querySelector("#btactive").onClick.listen((e) {
      makeRequest("acitvep",
          (querySelector("#activation") as html.SelectElement).value);
      chareupd();
    });
  }
  on FormatException catch(e)
  {
    html.window.alert(e.toString());
  }
  catch (e) {
    window.alert(
        "une erreur est survenue lors du chargement des produits de la db");
  }
}

/**
 * affiche la liste des produits pour la modification
 * ps utiliser pour la gestion des produits
 */
void charearicltforup() async {
  try {
    lis.clear();
    String ra;

    ra =
        "<h2 class='t'>Les Produits :</h2><div class='sercrch'><input id='serch'type='text' placeholder='ex: Pomme , Poivron , tommate etc .....'> <input type='button' value='rechercher' id='finda'></div><div id ='clav'></div><div id='art'>";
    ra += '<div class ="flexrow">';

    var rows2 = await makeRequest("lisprod", "a") as dynamic;

    List po = jsonDecode(rows2);
    po.forEach((p) => lis.add(new produit.J(p)));
    int crt = 0;
    int cont = 0;
    for (var p in lis) {
      ra += "<button id='aup$cont' class='article'  value='$p'>" +
          p.Nom +
          "</br>" +
          p.Prix.toStringAsFixed(2) +
          "€ /" +
          p.Forma +
          "</button> ";
      if (crt >= 9) {
        //ra+="</div><div class='flexrow'>";
        crt = 0;
      }

      crt++;
      cont++;
    }

    ra += '</div>';
    querySelector("#art")..innerHtml = ra;

    for (var i = 0; i < lis.length; i++) {
      querySelector("#aup$i")
        ..onClick.listen((e) {
          upart(querySelector("#aup$i"), lis, i);
        });
    }
    clavierserch();
    serchart2();
  } catch (e) {
    window.alert("une erreur est survenue lors de la Liste des produitss");
  }
}

/**
 * affichage de la modification d'un produi
 * ps utiliser pour la gestion des produits
 */
void upart(u, List<produit> lis, i) async {
  try {
    var type;
    var jscat = await makeRequest("listecategorie2", "a") as dynamic;
    var categori = "";

    List lcat = jsonDecode(jscat);
    lcat.forEach(
        (f) => categori += '<option value="${f['id']}"> ${f['nom']} </option>');
    lcat.forEach((f) {
      if (f['id'] == lis[i].Id_cat) {
        type = f['nom'];
      }
    });
    querySelector("#codep")
      ..innerHtml = '<div class="upprod">'
          '<h2>Modifier un produit</h2>'
          '<p>Nom :<input id="pname"type="text" value="${lis[i].Nom}"></p>'
          '<p>Prix :<input id="bprix" type="number" value="${(lis[i].Prix).toStringAsFixed(2)}">€</p>'
          '<p>Caisse:<input id="pidi" type="text" value="${lis[i].Indicat}"></p>'
          ' <p>Format: <select id= pforma name="tva"><option value="${lis[i].Forma}" selected>${lis[i].Forma}</option><option value="kg" >kg</option><option value="pc">pc</option><option value="botte">botte</option><option value="ravier">ravier</option></select></P> '
          '<p>Quantiter par caisse :<input  id ="piqc" min="0.0" step="0.1"  value="${lis[i].Nb}" type="number"></p>'
          '<P>Categorie : :<select id ="pcat"name="tva"><option value="${lis[i].Id_cat}" selected>$type</option>$categori</select></P> '
          ' <P>TVA :<select id="ptva" name="tva"><option value="${lis[i].Tva}" selected>${lis[i].Tva}%</option><option value="0" >0%</option><option value="6">6%</option><option value="12">12%</option><option value="21">21%</option></select></P> '

          '<button id="pudprodt" class="submit">Modifier Le produit </button>'
          '<button id="btdescat" class="submit">Desactiver Le produit </button>'
          '<button  class="submit" id="ret">Retour</button>'
          '</div>';
    querySelector("#ret").onClick.listen((e) {
      chareupd();
    });
    querySelector("#pudprodt").onClick.listen((e) {
      produit p;
      int tva =
          int.tryParse((querySelector("#ptva") as html.SelectElement).value);
      int cate =
          int.tryParse((querySelector("#pcat") as html.SelectElement).value);
      if((querySelector("#bprix") as html.NumberInputElement).valueAsNumber ==null)
      {
        throw new FormatException("Le prix ne peut pas etre null");
      }
      if((querySelector("#pname") as html.NumberInputElement).valueAsNumber ==null)
      {
        throw new FormatException("Le nom ne peut pas etre null");
      }
      if((querySelector("#bprix") as html.NumberInputElement).valueAsNumber<=0)
      {
        html.window.alert("!!!!!!!!!!!!!!!!Attention !!!!!!!!!!!!!!! \n Vous Avez entré un prix Inferieur ou egale a 0 \n  Cela est deconseiller \n le produit a été modifier  Veiller le remodifer !!!!!!!");
      }
      p = new produit(
          lis[i].Id,
          (querySelector("#pname") as html.InputElement).value,
          (querySelector("#bprix") as html.NumberInputElement).valueAsNumber,
          (querySelector("#pforma") as html.SelectElement).value,
          (querySelector("#pidi") as html.InputElement).value,
          tva,
          cate,(querySelector("#piqc") as html.NumberInputElement).valueAsNumber);
      makeRequest("upp", p.json());

      chareupd();
    });
    querySelector("#btdescat").onClick.listen((e) {
      makeRequest("descacitvep", lis[i].Id);
      chareupd();
    });
  }
  on FormatException catch(e)
  {
    html.window.alert(e.toString());
  }
  catch (e) {
    window
        .alert("une erreur est survenue lors de la modifiaction d'un produit");
  }
}

/**
 * utiliser pour afficher les produit
 * ps utiliser dans gestion des produits
 */
void serchart2() async {
  try {
    String ra;
    querySelector("#finda")
      ..onClick.listen((e) async {
        List<produit> list2 = new List<produit>();
        ra =
            "<h2 class='t'>  Les Produits :</h2><div class='sercrch'><input id='serch'type='text' placeholder='ex: Pomme , Poivron , tommate etc .....'> <input type='button' value='rechercher' id='finda'></div><div id ='clav'></div><div id='art'>";
        ra += '<div class ="flexrow">';
        var rows = await makeRequest(
                "findprod", (querySelector("#serch") as InputElement).value)
            as dynamic;
        clavierserch();
        List po = jsonDecode(rows);
        po.forEach((p) => list2.add(new produit.J(p)));
        int crt = 0;
        int cont = 0;
        for (var p in list2) {
          ra += "<button id='aup$cont' class='article'  value='$p'>" +
              p.Nom +
              "</br>" +
              p.Prix.toStringAsFixed(2) +
              "€ /" +
              p.Forma +
              "</button> ";

          crt++;
          cont++;
        }

        ra += '</div></div>';
        querySelector("#art")..innerHtml = ra;

        for (var i = 0; i < list2.length; i++) {
          querySelector("#aup$i")
            ..onClick.listen((e) {
              upart(querySelector("#aup$i"), list2, i);
            });
        }
        serchart2();
      });
  } catch (e) {
    window.alert("une erreur est survenue lors du chargement des produits");
  }
}
