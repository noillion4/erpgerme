import 'dart:async';
import 'dart:html' as html;

import 'dart:html';

import '../classemetier/achat.dart';
import '../classemetier/Client.dart';
import '../classemetier/fournisseur.dart';
import '../classemetier/produit.dart';
import '../classemetier/users.dart';
import '../classemetier/vente.dart';
import '../db.dart';

import 'dart:convert';

import 'package:crypto/crypto.dart';

/**
 * charge la page des utilisateur
 */
void chargeficheuser() async {
//  String nom ,prenom ,email;
//  int id ;
//  int gsm  ,tva ,pourcent;

  querySelector("#wall")
    ..innerHtml = '  <div class="wallleft" id="codep" >'
        '<h2>Ajouter un utilisateur</h2>'
        '<p>Nom :<input id="nom" type="text"></p>'
        '<p>Password :<input id="password" type="password"></p>'
        ' <p>Autoriser l' +
        "'" +
        'acces a la page home <input type="checkbox" id="checkhome"  name="scales"></p>'
        ' <p>Autoriser l' +
        "'" +
        'acces a la page Shop<input type="checkbox" id="checkshop"  name="scales"></p>'
        ' <p>Autoriser l' +
        "'" +
        'acces a la page Gest client<input type="checkbox" id="checkclient"  name="scales"></p>'
        ' <p>Autoriser l' +
        "'" +
        'acces a la page Gest user<input type="checkbox" id="checkuser"  name="scales"></p>'
        ' <p>Autoriser l' +
        "'" +
        'acces a la page Gest fournisseur<input type="checkbox" id="checkfounisseur"  name="scales"></p>'
        ' <p>Autoriser l' +
        "'" +
        'acces a la page Stat<input type="checkbox" id="checkstat"  name="scales"></p>'
        ' <p>Autoriser l' +
        "'" +
        'acces a la page Gest produit<input type="checkbox" id="checkproduit"  name="scales"></p>'
        ' <p>Autoriser l' +
        "'" +
        'acces a la page Gest categorie<input type="checkbox" id="checkcategorie"  name="scales"></p>'
        ' <button  class="submit" id="addUser">Ajouter</button>'
        '</div>' +
        '<div class="wallright" id="art"><div id="sample_container_id">';

  createuser();
}

/**
 * methode lors du click sur le bouton ajouter un utilisteur
 */
void createuser() {
  try {
    querySelector("#addUser").onClick.listen((e) async {
      //String gsm = (querySelector("#gsm")as html.TelephoneInputElement).value;
      //String tva =  (querySelector("#tva")as html.InputElement).value;
      //double pour =(querySelector("#pour")as html.NumberInputElement).valueAsNumber;
      DateTime D = DateTime.now();
      String d = D.toString();
      if((querySelector('#password') as html.InputElement).value==null)
        throw new FormatException("le mot de pass ne peut pas etre null");
      if((querySelector('#nom') as html.InputElement).value==null)
        throw new FormatException("le nom ne peut pas etre null");
      var bytes =
          utf8.encode((querySelector('#password') as html.InputElement).value);
      var digest = sha256.convert(bytes);
      print(digest);
      var nom = (querySelector("#nom") as html.InputElement).value;
      var home =
          (querySelector("#checkhome") as html.CheckboxInputElement).checked;
      var shop =
          (querySelector("#checkshop") as html.CheckboxInputElement).checked;
      var user =
          (querySelector("#checkuser") as html.CheckboxInputElement).checked;
      var stat =
          (querySelector("#checkstat") as html.CheckboxInputElement).checked;
      var founisseur =
          (querySelector("#checkfounisseur") as html.CheckboxInputElement)
              .checked;
      var client =
          (querySelector("#checkclient") as html.CheckboxInputElement).checked;
      var produit =
          (querySelector("#checkproduit") as html.CheckboxInputElement).checked;
      var categorie =
          (querySelector("#checkcategorie") as html.CheckboxInputElement)
              .checked;
      User u = new User(0, digest.toString(), nom, d, shop, home, user, stat,
          founisseur, client, produit, categorie);
      await makeRequest("adduser()", u.json());
      //await makeRequest("createcli", new Client(null, (querySelector("#pren") as html.InputElement).value, (querySelector("#nom")as html.InputElement).value,gsm ,tva, (querySelector("#em")as html.InputElement).value,pour.toInt() ).json());
      print(u.user);
      listeruser();
    });
    listeruser();

  }
  on FormatException catch(e)
  {
    html.window.alert(e.toString());
  }catch (e) {
    window.alert("une erreur est survenue lors de l'ajout d'un utilisateur "+e);
  }
}

/**
 * methode qui liste les utilisateur
 */
void listeruser() async {
  //      case "listeuser()":
  String ra;
  try {
    List<User> liuser = new List();
    var rows = await makeRequest("listeuser()", "a") as dynamic;
    print(rows);
    List e = jsonDecode(rows);
    e.forEach((f) => liuser.add(new User.J(f)));
    ra = "<h2 class='t'>Les Utilistateur :</h2><div id='art'>";
    ra += '<div class ="flexrow">';

    int crt = 0;
    int cont = 0;
    for (var p in liuser) {
      ra += "<div id='user$cont' class='article'  value='$p'>" +
          p.Nom +
          "</br>" +
          "<button id='sup${p.Id}' class='sumbit'>Supprimer</button>" +
          "</div>";
      if (crt >= 9) {
        //ra+="</div><div class='flexrow'>";
        crt = 0;
      }

      crt++;
      cont++;
    }

    ra += '</div>';
    querySelector("#art")..innerHtml = ra;
    for (var p in liuser) {
      querySelector("#sup${p.Id}").onClick.listen((e) async {
        await makeRequest("suppruser", p.Id);
        listeruser();
      });
    }
  } catch (e) {
    window.alert(
        "une erreur est survenue lors du chargement des utilisateur de la db");
  }
}
