import 'dart:async';
import 'dart:html' as html;
import '../classemetier/achat.dart';
import '../classemetier/Client.dart';
import '../classemetier/fournisseur.dart';
import '../classemetier/produit.dart';
import '../classemetier/users.dart';
import '../classemetier/vente.dart';
import '../db.dart';
import 'dart:html';
import 'metierexception.dart';
import 'dart:convert';

/**
 * Methode Pour le Shop
 * Developper Julien Mievis
 * Modification:11/01/2019
 *
 *
 *
 *
 *
 */
double Total;
List<Vente> Lisa = new List();
List<produit> lis = new List();
int idcli = null;
int idclient = null;
var idcl;

/**
 * gere la selection du client
 * ps utiliser pour le shop
 */
void chargercli() async {

    List<Client> liscli = new List();
    var rows = await makeRequest("liscli", "a") as dynamic;
    List e = jsonDecode(rows);
    e.forEach((f) => liscli.add(new Client.J(f)));
    querySelector("#btcli")
      ..onClick.listen((e) {
        querySelector("#bl").innerHtml = "";
        var inti = 0;
        for (var value in liscli) {
          querySelector("#bl").innerHtml +=
              "<button id='in$inti' class='btpayemnt'>" +
                  value.Prenom +
                  " " +
                  value.Nom +
                  " </button>";

          inti++;
        }
        nomcli(liscli);
      });
  }


//
/**
 * clavier utiliser pour la recherche
 * ps utiliser dans shop
 */
void clavierserch() async {
  querySelector("#serch")
    ..onClick.listen((e) {
      querySelector("#clav").innerHtml = '<div> <button>A</button>' +
          '<button>Z</button>' +
          '<button>E</button>' +
          '<button>R</button>' +
          '<button>T</button>' +
          '<button>Y</button>' +
          '<button>U</button>' +
          '<button>I</button>' +
          '<button>O</button>' +
          '<button>P</button></div>' +
          '<div> <button>Q</button>' +
          '<button>S</button>' +
          '<button>D</button>' +
          '<button>F</button>' +
          '<button>G</button>' +
          '<button>H</button>' +
          '<button>J</button>' +
          '<button>K</button>' +
          '<button>L</button>' +
          '<button>M</button></div>' +
          '<div><button>W</button>' +
          '<button>X</button>' +
          '<button>C</button>' +
          '<button>V</button>' +
          '<button>B</button>' +
          '<button>N</button><button id="bclose">Fermer</button><button id="btsubc"><</button></div>';

      querySelector("#bclose")
        ..onClick.listen((e) {
          querySelector("#clav").innerHtml = " ";
          String s;
          s.replaceRange(s.length - 1, s.length - 1, " ");
        });
      querySelector("#btsubc")
        ..onClick.listen((e) {
          (querySelector("#serch") as html.InputElement).value = " ";
        });

      ;
    });
}

//
/**
 * gere la selection du client
 * ps utiliser pour le shop
 * @param liscli la liste des clients
 */
void nomcli(liscli) async {
  var inti = 0;
  for (var value in liscli) {
    querySelector("#in$inti")
      ..onClick.listen((e) {
        querySelector("#btcli").innerHtml =
            " ${value.Id} " + value.Nom + "  " + value.Prenom;
        idcli = value.Id;
      });

    inti++;
  }
}

/**
 * affiche les moyens de payements
 * ps utiliser pour le shop
 */
void chargerbanc() {
  try {
    querySelector("#btbanc")
      ..onClick.listen((e) {
        clavierp("Bancontact");
      });
    querySelector("#btvisa")
      ..onClick.listen((e) {
        clavierp("Visa");
      });
    querySelector("#btliqu")
      ..onClick.listen((e) {
        clavierp("Liquide");
      });
    querySelector("#btautre")
      ..onClick.listen((e) {
        clavierp("Autre");
      });
    querySelector("#btfact")
      ..onClick.listen((e) async {
        List s = new List();
        for (final row in Lisa) {
          s.add(row.json());
        }
        var lin = jsonDecode(
            await makeRequest("facture", jsonEncode(s) + '&i=${idcli}'));
        print(lin);
        AFFicherFacture2(lin);
        Lisa.clear();
      });
    querySelector("#btcom")
      ..onClick.listen((e) async {
        List s = new List();
        for (final row in Lisa) {
          s.add(row.json());
        }

        AFFicherFacture(
            await makeRequest("com", jsonEncode(s) + '&i=${idcli}'));
        Lisa.clear();
      });
  } catch (e) {
    window.alert(
        "une erreur est survenue lors du chargement des moyens de payement");
  }
}

onButtonClicked(MouseEvent) {
  querySelector("#click").text = "ok";
  // do somethingonData
}

/**
 * clavier des moyens de payment
 * ps utiliser pour le shop
 */
void clavierp(String typ) async {
  try {
    querySelector("#bl")
      ..innerHtml = '<input type="number"  id="quatitier"  step="0.01">' +
          '<div>' +
          "<button id='bt1'>1</button>" +
          "<button id='bt2'>2</button>" +
          "<button id='bt3'>3</button>" +
          '</div>' +
          "<div><button id='bt4'>4</button>" +
          "<button id='bt5'>5</button>" +
          "<button id='bt6'>6</button>" +
          '</div>' +
          "<div><button id='bt7'>7</button>" +
          "<button id='bt8'>8</button>" +
          "<button id='bt9'>9</button>" +
          '</div>' +
          '<div>'
          "<button id='btsu'>X</button>" +
          "<button id='bt0'>0</button>" +
          "<button id='valid'>V</button>" +
          '</div>' +
          '<div><button id="btdiv1">XX.X</button></div>' +
          '<div><button id="btdiv">XX.XX</button></div>' +
          '<div><button id="btmulti1">X</button></div>' +
          '<div><button id="btmulti">XX</button></div>';
    (querySelector("#quatitier") as InputElement).value = '0';

    querySelector("#btdiv1")
      ..onClick.listen((e) {
        (querySelector("#quatitier") as NumberInputElement).valueAsNumber =
            (querySelector("#quatitier") as NumberInputElement).valueAsNumber /
                10;
      });
    querySelector("#btdiv")
      ..onClick.listen((e) {
        (querySelector("#quatitier") as NumberInputElement).valueAsNumber =
            (querySelector("#quatitier") as NumberInputElement).valueAsNumber /
                100;
      });
    querySelector("#btmulti")
      ..onClick.listen((e) {
        (querySelector("#quatitier") as NumberInputElement).valueAsNumber =
            (querySelector("#quatitier") as NumberInputElement).valueAsNumber *
                100;
      });
    querySelector("#btmulti1")
      ..onClick.listen((e) {
        (querySelector("#quatitier") as NumberInputElement).valueAsNumber =
            (querySelector("#quatitier") as NumberInputElement).valueAsNumber *
                10;
      });

    querySelector("#bt1")
      ..onClick.listen((e) {
        (querySelector("#quatitier") as InputElement).value += '1';
      });
    querySelector("#btsu")
      ..onClick.listen((e) {
        Afficheprod(Lisa);
      });
    querySelector("#bt2")
      ..onClick.listen((e) {
        (querySelector("#quatitier") as InputElement).value += '2';
      });
    querySelector("#bt3")
      ..onClick.listen((e) {
        (querySelector("#quatitier") as InputElement).value += '3';
      });
    querySelector("#bt4")
      ..onClick.listen((e) {
        (querySelector("#quatitier") as InputElement).value += '4';
      });
    querySelector("#bt5")
      ..onClick.listen((e) {
        (querySelector("#quatitier") as InputElement).value += '5';
      });
    querySelector("#bt6")
      ..onClick.listen((e) {
        (querySelector("#quatitier") as InputElement).value += '6';
      });
    querySelector("#bt7")
      ..onClick.listen((e) {
        (querySelector("#quatitier") as InputElement).value += '7';
      });
    querySelector("#bt8")
      ..onClick.listen((e) {
        (querySelector("#quatitier") as InputElement).value += '8';
      });
    querySelector("#bt9")
      ..onClick.listen((e) {
        (querySelector("#quatitier") as InputElement).value += '9';
      });
    querySelector("#bt0")
      ..onClick.listen((e) {
        (querySelector("#quatitier") as InputElement).value += '0';
      });
    querySelector("#valid")
      ..onClick.listen((e) async {
        double q =
            (querySelector("#quatitier") as NumberInputElement).valueAsNumber;
        if(Total<=0)
          {
            html.window.alert("LA commande ne peut etre inferieur ou egale a 0");
            throw new FormatException("LA commande ne peut etre inferieur ou egale a 0");

          }

        if (q == Total) {
          querySelector("#btcli").innerHtml = "Clients";
          Lisa.add(new Vente(0, 0, typ, q, "", 1, 1, 1,0));
          List s = new List();
          for (final row in Lisa) {
            s.add(row.json());
          }
          AFFicherFacture(
              await makeRequest("commande", jsonEncode(s) + '&i=${idcli}'));
          Lisa.clear();
        } else {
          if (Total < 0)
            Lisa.add(new Vente(0, 0, typ, q, "", 1, 1, 1,0));
          else
            Lisa.add(new Vente(0, 0, typ, -q, "", 1, 1, 1,0));
          Afficheprod(Lisa);
        }
      });

  }
  on FormatException catch(e)
  {
    html.window.alert(e.toString());
  }
  catch (e) {
    window.alert("une erreur est survenue lors du payement d'une commande");
  }
}

/**
 * clavier des commandes
 * ps utiliser pour le shop et la gestion des articles
 */
void clavier(html.Element e, lis, i) {
  try {
    querySelector("#bl")
      ..innerHtml = '<input type="number"  id="quatitier"  step="0.01">' +
          '<div>' +
          "<button id='bt1'>1</button>" +
          "<button id='bt2'>2</button>" +
          "<button id='bt3'>3</button>" +
          '</div>' +
          "<div><button id='bt4'>4</button>" +
          "<button id='bt5'>5</button>" +
          "<button id='bt6'>6</button>" +
          '</div>' +
          "<div><button id='bt7'>7</button>" +
          "<button id='bt8'>8</button>" +
          "<button id='bt9'>9</button>" +
          '</div>' +
          '<div>'
              "<button id='btsu'>X</button>" +
          "<button id='bt0'>0</button>" +
          "<button id='valid'>V</button>" +
          '</div>' +
          '<div><button id="btdiv1">XX.X</button></div>' +
          '<div><button id="btdiv">XX.XX</button></div>' +
          '<div><button id="btmulti1">X</button></div>' +
          '<div><button id="btmulti">XX</button></div>'
          +
          '<div><button id="btcaisse">Caisse</button></div>';

    (querySelector("#quatitier") as InputElement).value = '0';
    querySelector("#btdiv1")
      ..onClick.listen((e) {
        (querySelector("#quatitier") as NumberInputElement).valueAsNumber =
            (querySelector("#quatitier") as NumberInputElement).valueAsNumber /
                10;
      });
    querySelector("#btdiv")
      ..onClick.listen((e) {
        (querySelector("#quatitier") as NumberInputElement).valueAsNumber =
            (querySelector("#quatitier") as NumberInputElement).valueAsNumber /
                100;
      });
    querySelector("#btcaisse")
      ..onClick.listen((ei) {
        clavierbycaisse(e, lis, i);
      });
    querySelector("#btmulti")
      ..onClick.listen((e) {
        (querySelector("#quatitier") as NumberInputElement).valueAsNumber =
            (querySelector("#quatitier") as NumberInputElement).valueAsNumber *
                100;
      });
    querySelector("#btmulti1")
      ..onClick.listen((e) {
        (querySelector("#quatitier") as NumberInputElement).valueAsNumber =
            (querySelector("#quatitier") as NumberInputElement).valueAsNumber *
                10;
      });

    querySelector("#bt1")
      ..onClick.listen((e) {
        (querySelector("#quatitier") as InputElement).value += '1';
      });
    querySelector("#btsu")
      ..onClick.listen((e) {
        Afficheprod(Lisa);
      });
    querySelector("#bt2")
      ..onClick.listen((e) {
        (querySelector("#quatitier") as InputElement).value += '2';
      });
    querySelector("#bt3")
      ..onClick.listen((e) {
        (querySelector("#quatitier") as InputElement).value += '3';
      });
    querySelector("#bt4")
      ..onClick.listen((e) {
        (querySelector("#quatitier") as InputElement).value += '4';
      });
    querySelector("#bt5")
      ..onClick.listen((e) {
        (querySelector("#quatitier") as InputElement).value += '5';
      });
    querySelector("#bt6")
      ..onClick.listen((e) {
        (querySelector("#quatitier") as InputElement).value += '6';
      });
    querySelector("#bt7")
      ..onClick.listen((e) {
        (querySelector("#quatitier") as InputElement).value += '7';
      });
    querySelector("#bt8")
      ..onClick.listen((e) {
        (querySelector("#quatitier") as InputElement).value += '8';
      });
    querySelector("#bt9")
      ..onClick.listen((e) {
        (querySelector("#quatitier") as InputElement).value += '9';
      });
    querySelector("#bt0")
      ..onClick.listen((e) {
        (querySelector("#quatitier") as InputElement).value += '0';
      });
    querySelector("#valid")
      ..onClick.listen((e) {
        double q =
            (querySelector("#quatitier") as NumberInputElement).valueAsNumber;

        if((q<1))
        {
          html.window.alert("Le produit ne peut etre inferieur ou egale a 0");
          throw new FormatException("Le produit ne peut etre inferieur ou egale a 0");

        }

        double p = lis[i].Prix;
        if((p<1))
        {
          html.window.alert("Le prix ne peut etre inferieur ou egale a 0");
          throw new FormatException("Le prix ne peut etre inferieur ou egale a 0");

        }

        int ca = q ~/ lis[i].Nb;
        Lisa.add(new Vente(
            0,
            lis[i].Id,
            lis[i].Nom,
            p,
            lis[i].Forma,
            0,
            q,
            0,
            ca));
        Afficheprod(Lisa);
      });
  }
  on FormatException catch(e)
  {
    html.window.alert(e.toString());
  }
  catch(e)
  {

  }
}

/**
 * clavier des commandes par caisse
 * ps utiliser pour le shop et la gestion des articles
 */
void clavierbycaisse(html.Element e, lis, i) {
  querySelector("#bl")
    ..innerHtml = '<input type="number"  id="quatitier"  step="1"  min="0">' +
        '<div>' +
        "<button id='bt1'>1</button>" +
        "<button id='bt2'>2</button>" +
        "<button id='bt3'>3</button>" +
        '</div>' +
        "<div><button id='bt4'>4</button>" +
        "<button id='bt5'>5</button>" +
        "<button id='bt6'>6</button>" +
        '</div>' +
        "<div><button id='bt7'>7</button>" +
        "<button id='bt8'>8</button>" +
        "<button id='bt9'>9</button>" +
        '</div>' +
        '<div>'
            "<button id='btsu'>X</button>" +
        "<button id='bt0'>0</button>" +
        "<button id='valid'>V</button>" +
        '</div>'+
        '<div><button id="btquatiter">Quantiter</button></div>'
       ;

  (querySelector("#quatitier") as InputElement).value = '0';
  querySelector("#btquatiter")
    ..onClick.listen((ei) {
      clavier( e, lis, i);
    });
  querySelector("#bt1")
    ..onClick.listen((e) {
      (querySelector("#quatitier") as InputElement).value += '1';
    });
  querySelector("#btsu")
    ..onClick.listen((e) {
      Afficheprod(Lisa);
    });
  querySelector("#bt2")
    ..onClick.listen((e) {
      (querySelector("#quatitier") as InputElement).value += '2';
    });
  querySelector("#bt3")
    ..onClick.listen((e) {
      (querySelector("#quatitier") as InputElement).value += '3';
    });
  querySelector("#bt4")
    ..onClick.listen((e) {
      (querySelector("#quatitier") as InputElement).value += '4';
    });
  querySelector("#bt5")
    ..onClick.listen((e) {
      (querySelector("#quatitier") as InputElement).value += '5';
    });
  querySelector("#bt6")
    ..onClick.listen((e) {
      (querySelector("#quatitier") as InputElement).value += '6';
    });
  querySelector("#bt7")
    ..onClick.listen((e) {
      (querySelector("#quatitier") as InputElement).value += '7';
    });
  querySelector("#bt8")
    ..onClick.listen((e) {
      (querySelector("#quatitier") as InputElement).value += '8';
    });
  querySelector("#bt9")
    ..onClick.listen((e) {
      (querySelector("#quatitier") as InputElement).value += '9';
    });
  querySelector("#bt0")
    ..onClick.listen((e) {
      (querySelector("#quatitier") as InputElement).value += '0';
    });
  querySelector("#valid")
    ..onClick.listen((e) {
      try {
        int cat =
            (querySelector("#quatitier") as NumberInputElement).valueAsNumber;
        double p = lis[i].Prix;

        if(p<1)
        {
          html.window.alert("Le prix ne peut etre inferieur ou egale a 0");
          throw new FormatException("Le prix ne peut etre inferieur ou egale a 0");

        }
        int q = lis[i].Nb * cat;
        if((q<1))
        {
          html.window.alert("Le produit ne peut etre inferieur ou egale a 0");
          throw new FormatException("Le produit ne peut etre inferieur ou egale a 0");

        }
        Lisa.add(new Vente(
            0,
            lis[i].Id,
            lis[i].Nom,
            p,
            lis[i].Forma,
            0,
            q,
            0,
            cat));
        Afficheprod(Lisa);
      }
      on FormatException catch(e)
      {
        html.window.alert(e.toString());
      }
      catch(e) {}
    });
}
/**
 * afficher la commande  en coure
 * ps utiliser pour le shop
 * @param List Lisa  la liste des produis de la commande
 */
void Afficheprod(Lisa) async {
  querySelector("#bl")
    ..innerHtml ="";
  double q;
  int p;
  double total = 0;
  var cr = 0;
  for (Vente value in Lisa) {
    querySelector("#bl")
      ..innerHtml +=
          "<p> ${value.Nom} ${value.Caisse} C = ${value.Qt} X ${value.Prix.toStringAsFixed(2)}€ = ${(value.Prix * value.Qt).toStringAsFixed(2)} €<button id='btsv$cr' class='btts'>X</button></P>";
    total += value.Prix * value.Qt;
    cr++;
  }
  cr = 0;

  Total = total;
  querySelector("#idtot")
    ..innerHtml = "<h2>   Total :  ${total.toStringAsFixed(2)} €</H2>";
  charg();
}

/**
 * defini les bouton suprimmer de la commande encours
 * ps utiliser pour le shop
 */
void charg() {
  int cr = 0;
  for (Vente value in Lisa) {
    querySelector("#btsv$cr")
      ..onClick.listen((e) {
        Lisa.remove(value);
        Afficheprod(Lisa);
      });
    cr++;
  }
}

/**
 * affiche les boutom du payemnts
 * ps utiliser pour le shop
 */
void affichepayem() {
  querySelector("#bp")
    ..onClick.listen((e) {
      querySelector("#bl").innerHtml =
          '<button id="btbanc" class="btpayemnt">Bancontact</button>' +
              '<button  id ="btvisa" class="btpayemnt">VISA</button>' +
              '<button  id="btliqu" class="btpayemnt">Liquide</button>' +
              '<button id="btautre" class="btpayemnt">Autre</button>' +
              '<button  class="btpayemnt "id="btcom">Commande</button>' +
              '<button  id="btfact"class="btpayemnt">Facture</button>';
      chargerbanc();
    });
}
/**
 * //charge les produit et lesaffiche version ordi
 * ps utiliser pour le shop
 */
void loadprodvordi() async {
  try {
    lis.clear();
    String ra;


    ra =
        "<h2 class='t'>  <button id='btcli' class='btclient'>Clients</button> <button id='bttactile' class='btclient'>Vtactile</button>  Les Produits :</div><div id ='clav'></div>" +

            "<div id='art'>";
    ra += '<div class ="flexcolcenter"><table>';

    var rows2 = await makeRequest("lisprod", "a") as dynamic;

    List po = jsonDecode(rows2);
    po.forEach((p) => lis.add(new produit.J(p)));
    int crt = 0;
    int cont = 0;
    for (var p in lis) {
      ra += "<div class='flexcenterb'>" +"<tr><td >" + p.Nom +"</td><td>"+p.Prix.toStringAsFixed(2) +
          "€/ " +
          p.Forma+" </td><td><p>"+p.Nb.toString()+ p.Forma +"/Caisse "+ p.Indicat+"</p></td><td><input id='input$cont' type='number'>/Caissse <input id='inputE$cont' type='number'>/"+p.Forma+"<button id='add$cont' class=''  value='$p'>Ajouter</button></td></tr>"
         " </div>";
      if (crt >= 9) {
        //ra+="</div><div class='flexrow'>";
        crt = 0;
      }

      crt++;
      cont++;
    }
    ra += '</div>';
    querySelector("#art")..innerHtml = ra+"</table>";
    querySelector("#bttactile").onClick.listen((e){loadprod();});


    for (var i = 0; i < lis.length; i++) {
      querySelector("#add$i")
        ..onClick.listen((e) {
          int cat =
              (querySelector("#input$i") as NumberInputElement).valueAsNumber;
          double p = lis[i].Prix;
          int q =   (querySelector("#inputE$i") as NumberInputElement).valueAsNumber;
          if((cat<1 || p<1))
          {
            html.window.alert("LA Quantiter ou le nobre de caisse entree est incorect ");
            throw new FormatException("LA Quantiter ou le nobre de caisse entree est incorect ");

          }
          Lisa.add(new Vente(0, lis[i].Id, lis[i].Nom, p, lis[i].Forma, 0, q, 0,cat));
          Afficheprod(Lisa);

        });
    }
    // gere le clavier de la recherche

    chargercli();
    // gere les moyent de payment
    affichepayem();
  }
  on FormatException catch(e)
  {
    html.window.alert(e.toString());
  }catch (e) {
    window.alert(
        "une erreur est survenue lors du chargement des Produits dans la db");
  }
}

/**
 * //charge les produit et lesaffiche
 * ps utiliser pour le shop
 */
void loadprod() async {
  try {
    lis.clear();
    String ra;
    var categori = "";
    var jscat = await makeRequest("listecategorie2", "a") as dynamic;
    List lcat = jsonDecode(jscat);
    lcat.forEach((f) =>
        categori += '<button id="cate${f['id']}"> ${f['nom']} </button>');

    ra =
        "<h2 class='t'>  <button id='btcli' class='btclient'>Clients</button><button id='btordi' class='btclient'>Vordi</button>  Les Produits :</h2><div class='sercrch'><input id='serch'type='text' placeholder='ex: Pomme , Poivron , tommate etc .....'> <input type='button' value='rechercher' id='finda'></div><div id ='clav'></div>" +
            categori +
            "<div id='art'>";
    ra += '<div class ="flexrow">';

    var rows2 = await makeRequest("lisprod", "a") as dynamic;

    List po = jsonDecode(rows2);
    po.forEach((p) => lis.add(new produit.J(p)));
    int crt = 0;
    int cont = 0;
    for (var p in lis) {
      ra += "<button id='a$cont' class='article'  value='$p'>" +
          p.Nom +
          "</br>" +
          p.Prix.toStringAsFixed(2) +
          "€ /" +
          p.Forma + "</br>"+ p.Nb.toString() + p.Forma +" / Caisse "+ p.Indicat +
          "</button> ";
      if (crt >= 9) {
        //ra+="</div><div class='flexrow'>";
        crt = 0;
      }

      crt++;
      cont++;
    }
    ra += '</div>';
    querySelector("#art")..innerHtml = ra;
    querySelector("#codep")
      ..innerHtml = "<div id='bl'>" +
          "</div>"
          "<div id='idtot'></div> <div id='bp'><input type='button' value='Payement'></divdiv>";
    int cot = 0;
    for (var value in lcat) {
      html.querySelector("#cate${value['id']}").onClick.listen((e) async {
        print(value);
        catechart(value['id']);
      });
      cot++;
    }
    for (var i = 0; i < lis.length; i++) {
      querySelector("#a$i")
        ..onClick.listen((e) {
          clavierbycaisse(querySelector("#a$i"), lis, i);
        });
    }
    querySelector("#btordi").onClick.listen((e){loadprodvordi();});
    // gere le clavier de la recherche
    clavierserch();
    serchart();
    chargercli();
    // gere les moyent de payment
    affichepayem();
    Afficheprod(Lisa);
  } catch (e) {
    window.alert(
        "une erreur est survenue lors du chargement des Produits dans la db");
  }
}

/**
 * @param la categorie
 * affiche les article pour une categorie
 */
void catechart(cat) async {
  try {
    print(cat);
    String ra;
    var categori = "";
    var jscat = await makeRequest("listecategorie2", "a") as dynamic;
    List lcat = jsonDecode(jscat);
    lcat.forEach((f) =>
        categori += '<button id="cate${f['id']}"> ${f['nom']} </button>');
    List<produit> list2 = new List<produit>();
    ra =
        "<h2 class='t'> <button id='btcli' class='btclient'>Clients</button> Les Produits :</h2><div class='sercrch'><input id='serch'type='text' placeholder='ex: Pomme , Poivron , tommate etc .....'> <input type='button' value='rechercher' id='finda'></div>$categori<div id ='clav'></div><div id='art'>";
    ra += '<div class ="flexrow">';

    var rows = await makeRequest("findprodcat", cat.toString()) as dynamic;
    clavierserch();
    List po = jsonDecode(rows);
    po.forEach((p) => list2.add(new produit.J(p)));
    int crt = 0;
    int cont = 0;
    for (var p in list2) {
      ra += "<button id='a$cont' class='article'  value='$p'>" +
          p.Nom +
          "</br>" +
          p.Prix.toStringAsFixed(2) +
          "€ /" +
          p.Forma + "</br>"+ p.Nb.toString() + p.Forma +" / Caisse "+ p.Indicat +
          "</button> ";

      crt++;
      cont++;
    }

    ra += '</div></div>';
    querySelector("#art")..innerHtml = ra;

    for (var i = 0; i < list2.length; i++) {
      querySelector("#a$i")
        ..onClick.listen((e) {
          clavierbycaisse(querySelector("#a$i"), list2, i);
        });
    }
    int cot = 0;
    for (var value in lcat) {
      html.querySelector("#cate${value['id']}").onClick.listen((e) async {
        print(value);
        catechart(value['id']);
      });
      cot++;
    }
    serchart();
    clavierserch();
    chargercli();
  } catch (e) {
    window.alert(
        "une erreur est survenue lors du chargement des produits par categorie");
  }
}

/**
 * // s'occupe de la recherche de produit
 * ps utiliser pour le shop
 */
void serchart() async {
  try {
    String ra;
    querySelector("#finda")
      ..onClick.listen((e) async {
        var categori = "";
        var jscat = await makeRequest("listecategorie2", "a") as dynamic;
        List lcat = jsonDecode(jscat);
        lcat.forEach((f) =>
            categori += '<button id="cate${f['id']}"> ${f['nom']} </button>');
        List<produit> list2 = new List<produit>();
        ra =
            "<h2 class='t'> <button id='btcli' class='btclient'>Clients</button> Les Produits :</h2><div class='sercrch'><input id='serch'type='text' placeholder='ex: Pomme , Poivron , tommate etc .....'> <input type='button' value='rechercher' id='finda'></div>$categori<div id ='clav'></div><div id='art'>";
        ra += '<div class ="flexrow">';
        var rows = await makeRequest(
                "findprod", (querySelector("#serch") as InputElement).value)
            as dynamic;
        clavierserch();
        List po = jsonDecode(rows);
        po.forEach((p) => list2.add(new produit.J(p)));
        int crt = 0;
        int cont = 0;
        for (var p in list2) {
          ra += "<button id='a$cont' class='article'  value='$p'>" +
              p.Nom +
              "</br>" +
              p.Prix.toStringAsFixed(2) +
              "€ /" +
              p.Forma + "</br>"+ p.Nb.toString() + p.Forma +" / Caisse "+ p.Indicat +
              "</button> ";

          crt++;
          cont++;
        }

        ra += '</div></div>';
        querySelector("#art")..innerHtml = ra;

        for (var i = 0; i < list2.length; i++) {
          querySelector("#a$i")
            ..onClick.listen((e) {
              clavierbycaisse(querySelector("#a$i"), list2, i);
            });
        }
        serchart();
        clavierserch();
        chargercli();
        int cot = 0;
        for (var value in lcat) {
          html.querySelector("#cate${value['id']}").onClick.listen((e) async {
            print(value);
            catechart(value['id']);
          });
          cot++;
        }
      });
  } catch (e) {
    window.alert(
        "une erreur est survenue lors du chargement de la recherche d'un produit dans la db");
  }
}

/**
 * charge le programme de vente
 * utiliser pour le shop
 */
void loadvente() {
  querySelector("#wall")
    ..innerHtml = '  <div class="wallleft" id="codep" >test</div>' +
        '<div class="wallright" id="art"><div id="sample_container_id">';
  //charge les produit
  loadprod();
  // s'occupe des client
  chargercli();
  // affiche la commande en cours
  Afficheprod(Lisa);
}

/**
 *@param la liste des facture
 * affiche la facture selectionner par utiliser
 * (en fonction du click)
 */
void AFFicherFacture2(list) async {
  try {
    querySelector("#bl")
      ..innerHtml = "<button id='btretour' class='submit'>Retour</button>";
    querySelector('#bl').onClick.listen((e) => loadvente());
    querySelector("#art").innerHtml = '<div class="flexrow2">'
        '<div class="flexrow"><img src="logo.png"><div><h2>Ferme Paque Bartholomé</h2>'
        '<P>Rue de Liége,45</p>'
        '<p>4450 Lantin</p>'
        '<p>Téléphone:04 97 70 95 92</p></div></div><div>' +
        '</div></div>'
        '<span class="factt"> Facture N° ${list[0]} Date : Le ${(DateTime.now()).toString().substring(0, 10)}</span>';
    var n = await makeRequest("chargecom", "${list[1]}");
    print(n);
    var r = jsonDecode(n);
    var su = '<table>' +
        "<tr><td>Produit°</td><td>Quantiter</td><td>Total HTVA</td><td>TVA</td><td>Total</td></tr>";
    var thtva = 0, tva = 0, total = 0;
    for (var value in r) {
      su +=
          "<tr><td>${value['nom']}</td><td>${value['quatitier']}</td><td>${value['total'].toStringAsFixed(2)}€</td><td>${(value['ttva'] - value['total']).toStringAsFixed(2)}€</td><td>${value['ttva'].toStringAsFixed(2)}€</td></tr>";
      thtva += value['total'];
      tva += (value['ttva'] - value['total']);
      total += value['ttva'];
    }
    querySelector("#art").innerHtml += '$su' +
        '</table>' +
        '<span class="factt">TOTAL HTVA : ${thtva.toStringAsFixed(2)} € TVA : ${tva.toStringAsFixed(2)} € TOTAL : ${total.toStringAsFixed(2)}€</span>' +
        '<button  class="" id="btim" onclick="">Imprimer</button> ';
    querySelector("#btim").onClick.listen((e) {
      window.print();
    });
  } catch (e) {
    window.alert("une erreur est survenue lors du chargement d'une facture");
  }
}

/**
 *@param id de la commande
 * affiche la commande selectionner par utiliser
 * (en fonction du click)
 */
void AFFicherFacture(id) async {
  try {
    querySelector("#bl")
      ..innerHtml = "<button id='btretour' class='submit'>Retour</button>";
    querySelector('#bl').onClick.listen((e) => loadvente());
    querySelector("#art").innerHtml = '<div class="flexrow2">'
        '<div class="flexrow"><img src="logo.png"><div><h2>Ferme Paque Bartholomé</h2>'
        '<P>Rue de Liége,45</p>'
        '<p>4450 Lantin</p>'
        '<p>Téléphone:04 97 70 95 92</p></div></div><div>' +
        '</div></div>'
        '<span class="factt"> Commande N° $id Date : Le ${(DateTime.now()).toString().substring(0, 10)}</span>';
    var n = await makeRequest("chargecom", id);
    print(n);
    var r = jsonDecode(n);
    var su = '<table>' +
        "<tr><td>Produit°</td><td>Quantiter</td><td>Nb Caisse</td><td>Caisse</td><td>Embalage</td><td>Total HTVA</td><td>TVA</td><td>Total</td><td>Recolter</td></tr>";
    var thtva = 0, tva = 0, total = 0;
    for (var value in r) {
      su +=
          "<tr><td>${value['nom']}</td><td>${value['quatitier']}</td><td>${value['caisse']}</td><td>${value['indication']} </td><td>${value['nb']}${value['forma']}</td><td>${value['total'].toStringAsFixed(2)}€</td><td>${(value['ttva'] - value['total']).toStringAsFixed(2)}€</td><td>${value['ttva'].toStringAsFixed(2)}€</td><td></td></tr>";
      thtva += value['total'];
      tva += (value['ttva'] - value['total']);
      total += value['ttva'];
    }
    querySelector("#art").innerHtml += '$su' +
        '</table>' +
        '<span class="factt">TOTAL HTVA : ${thtva.toStringAsFixed(2)} € TVA : ${tva.toStringAsFixed(2)} € TOTAL : ${total.toStringAsFixed(2)}€</span>' +
        '<button  class="" id="btim" onclick="">Imprimer</button> ';
    querySelector("#btim").onClick.listen((e) {
      window.print();
    });
  } catch (e) {
    window.alert("une erreur est survenue lors du chargement d'une commande");
  }
}
