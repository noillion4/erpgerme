import 'dart:convert';
import 'dart:html' as html;
import 'dart:html';

import '../classemetier/produit.dart';
import '../classemetier/vente.dart';
import '../db.dart';

/**
 * accuille
 * affiche les ventes
 */
/**
 * declaration
 */
double Total;
List<Vente> Lisa = new List();
List<produit> lis = new List();
int idcli = null;
int idclient = null;
var idcl;
/**
 * methode qui gere l'accuille du site (affiche les vente )
 * ps utiliser pour accuille
 */
void chargehome() {
  querySelector("#home")
    ..onClick.listen((e) {
      var now = new DateTime.now();
      var form2;
      var form = "${now.year.toString()}-0${now.month.toString()}-01";
      if (now.month < 11)
        form2 = "${now.year.toString()}-0${(now.month + 1).toString()}-01";
      else {
        form2 = "${(now.year + 1).toString()}-01-01";
      }
      querySelector("#wall")
        ..innerHtml = ' ' +
            '<div class="wallright" id="art">'
            '<div class="btaff" > '
            '<input id ="datedeb" type="date" value="${form}">'
            ''
            '<input id ="datefin"type="date" value="$form2" data-date-format="DD MMMM YYYY" placeholder="$form">'
            '<button id="btaffi">Afficher</button>'
            ''
            '</div>'
            ''
            ''
            ''
            ''
            ''
            ''
            ''
            ''
            ''
            ''
            ''
            '</div>';

      home();
    });
}

/**
 * methode qui gere l'accuille du site (affiche les vente )
 * ps utiliser pour accuille
 */
void home() async {
  try {
    var ret = await makeRequest('factdate',
        '${(querySelector("#datefin") as html.DateInputElement).valueAsDate}&i=${(querySelector("#datedeb") as html.DateInputElement).valueAsDate}');
    var R = jsonDecode(ret);
    double toth = 0, totc = 0, tva = 0;
    var now = new DateTime.now();
    var form2;
    var form = "${now.year.toString()}-0${now.month.toString()}-01";
    if (now.month < 11)
      form2 = "${now.year.toString()}-0${(now.month + 1).toString()}-01";
    else {
      form2 = "${(now.year + 1).toString()}-01-01";
    }
    var a = ' ' +
        '<div class="wallright" id="art">'
        '<div class="btaff" > '
        '<input id ="datedeb" type="date" value="${form}">'
        ''
        '<input id ="datefin"type="date" value="$form2" data-date-format="DD MMMM YYYY" placeholder="$form">'
        '<button id="btaffi">Afficher</button>'
        ''
        '</div>'
        ''
        ''
        ''
        ''
        ''
        ''
        ''
        ''
        ''
        ''
        ''
        '</div>';

    a +=
        "<div class='tab  page-breaker'><div class='fact'><H2>FACTURER et PAYER</H2>"
        "<table><tr><th>NOM</th><th>QUATITER</th><th>TOTAL HTVA</th><th>TOTAL TVAC</th></tr>";
    for (var value in R) {
      a +=
          "<tr><td>${value['nom']}</td> <td>${value['quatitier']}/${value['forma']}</td> <td>${value['total']}€</td ><td>${value['ttva'].toStringAsFixed(2)}€</td></tr>";
      toth += value['total'];
      totc += value['ttva'];
      tva += value['ttva'] - value['total'];
    }

    a +=
        " </table> <h4>TOTAL HTVA : $toth € </h4><h4> TOTAL TVAC :  ${totc.toStringAsFixed(2)}€ </h4><h4>TOTAL TVA ${tva.toStringAsFixed(2)} €</h4></div>";

    ret = await makeRequest('factdatenon',
        '${(querySelector("#datefin") as html.DateInputElement).valueAsDate}&i=${(querySelector("#datedeb") as html.DateInputElement).valueAsDate}');
    R = jsonDecode(ret);
    toth = 0;
    totc = 0;
    tva = 0;
    a += "<div class='fact'><H2>FACTURER et NON PAYER</H2>"
        "<table><tr><th>NOM</th><th>QUATITER</th><th>TOTAL HTVA</th><th>TOTAL TVAC</th></tr>";
    for (var value in R) {
      a +=
          "<tr><td>${value['nom']}</td> <td>${value['quatitier']}/${value['forma']}</td> <td>${value['total']}€</td ><td>${value['ttva'].toStringAsFixed(2)}€</td></tr>";
      toth += value['total'];
      totc += value['ttva'];
      tva += value['ttva'] - value['total'];
    }

    a +=
        "</table> <h4>TOTAL HTVA : $toth € </h4><h4> TOTAL TVAC :  ${totc.toStringAsFixed(2)}€ </h4><h4>TOTAL TVA ${tva.toStringAsFixed(2)} €</h4></div>";

    ret = await makeRequest('commnon',
        '${(querySelector("#datefin") as html.DateInputElement).valueAsDate}&i=${(querySelector("#datedeb") as html.DateInputElement).valueAsDate}');
    R = jsonDecode(ret);
    toth = 0;
    totc = 0;
    tva = 0;
    a += "<div class='fact'><H2>COMMANDE NON FACTURER</H2>"
        "<table><tr><th>NOM</th><th>QUATITER</th><th>TOTAL HTVA</th><th>TOTAL TVAC</th></tr>";
    for (var value in R) {
      a +=
          "<tr><td>${value['nom']}</td> <td>${value['quatitier']}/${value['forma']}</td> <td>${value['total']}€</td ><td>${value['ttva'].toStringAsFixed(2)}€</td></tr>";
      toth += value['total'];
      totc += value['ttva'];
      tva += value['ttva'] - value['total'];
    }

    a +=
        "</table> <h4>TOTAL HTVA : $toth € </h4><h4> TOTAL TVAC :  ${totc.toStringAsFixed(2)}€ </h4><h4>TOTAL TVA ${tva.toStringAsFixed(2)} €</h4></div>";
    querySelector("#art").innerHtml =
        a + '<button  class="" id="btim" onclick="">Imprimer</button> ';
    querySelector("#btaffi")
      ..onClick.listen((e) {
        home();
      });

    querySelector("#btim").onClick.listen((e) {
      window.print();
    });
  } catch (e) {
    window.alert("une erreur est survenue lors du chargement de l'accueil");
  }
}
