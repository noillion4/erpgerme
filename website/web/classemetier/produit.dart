import 'dart:convert' as JSON;

class produit {
  /**
   * declaration
   */
  String _nom;

  double _prix;
  int _tva;
  int _id, _id_cat;
  String _forma;
  String _indicat;
  int _nb;

  /**
   * getter and setter
   */
  String get Indicat => _indicat;

  set Indicat(String value) {
    _indicat = value;
  }

  int get Nb => _nb;

  set Nb(int value) {
    _nb = value;
  }

  int get Tva => _tva;

  set Tva(int value) {
    _tva = value;
  }

  String get Nom => _nom;

  set Nom(String value) {
    _nom = value;
  }

  get Id_cat => _id_cat;

  set Id_cat(value) {
    _id_cat = value;
  }

  int get Id => _id;

  set Id(int value) {
    _id = value;
  }

  String get Forma => _forma;

  set Forma(String value) {
    _forma = value;
  }

  double get Prix => _prix;

  set Prix(double value) {
    _prix = value;
  }

  /**
   * contructor
   */
  produit(id, nom, prix, forma, indicat, tva, id_cat,nb) {
    try {
      this._id = id;
      this._prix = prix;
      this._forma = forma;
      this._indicat = indicat;
      this._nom = nom;
      this._tva = (tva as double).toInt();
      this._id_cat = id_cat;
      this._nb=nb;
    } catch (e) {
      this._id = id;
      this._prix = prix;
      this._forma = forma;
      this._indicat = indicat;
      this._nom = nom;
      this._tva = (tva as double).toInt();
      this._id_cat = int.parse(id_cat);
      this._nb=int.parse(nb);
    }
  }

  /**
   * Constructor with json
   */
  produit.J(json2) {
    print(json2);
    Map jsonData = JSON.jsonDecode(json2);
    _id = jsonData['id'];
    _nom = jsonData['nom'];
    _prix = jsonData['prix'].toDouble();
    _forma = jsonData['forma'];
    _indicat = jsonData['indicat'];
    _tva = jsonData['tva'];
    _id_cat = jsonData['id_cat'];
    _nb =jsonData['nb'];
  }

  /**
   * return the obj in forma json
   */
  String json() {
    var jsonDataAsString = '''{
   "id":$_id,
   "nom":"$_nom",
   "prix": $_prix,
   "forma":"$_forma",
   "indicat":"$_indicat",
   "tva":$_tva,
   "id_cat": $_id_cat,
   "nb" : $_nb
}''';
    return jsonDataAsString;
  }
}
