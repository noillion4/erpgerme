import 'dart:convert' as JSON;

class Vente {
  /**
   * declaration
   */
  String _nom;

  double _prix, _qt;

  String _forma;
  int _idcomande, _indicat, _id, _idp ,_caisse;



  /**
   * getter and setter
   */

  get Caisse => _caisse;

  set Caisse(value) {
    _caisse = value;
  }
  get Indicat => _indicat;

  set Indicat(value) {
    _indicat = value;
  }

  get Idp => _idp;

  set Idp(value) {
    _idp = value;
  }

  String get Nom => _nom;

  set Nom(String value) {
    _nom = value;
  }

  int get Idcomande => _idcomande;

  set Idcomande(int value) {
    _idcomande = value;
  }

  get Id => _id;

  set Id(value) {
    _id = value;
  }

  String get Forma => _forma;

  set Forma(String value) {
    _forma = value;
  }

  double get Prix => _prix;

  set Prix(double value) {
    _prix = value;
  }

  get Qt => _qt;

  set Qt(value) {
    _qt = value;
  }

  /**
   * constructeur
   */
  Vente(id, idp, nom, prix, forma, idcomande, qt, indicat ,caisse) {
    this._id = id;
    this._idp = idp;
    this._prix = prix;
    this._forma = forma;
    this._idcomande = idcomande;
    this._nom = nom;
    this._qt = qt;
    this.Caisse =caisse;
    this._indicat = indicat;
  }

  /**
   * constructeur avec json
   */
  Vente.J(json2) {
    print(json2);
    Map jsonData = JSON.jsonDecode(json2);
    _id = jsonData['id'] as int;

    _indicat = jsonData['indicat'];

    _idcomande = jsonData['idcomande'];
    _idp = jsonData['idp'];

    _qt = jsonData['qt'].toDouble();
    _prix = jsonData['prix'].toDouble();

    _nom = jsonData['nom'];

    _forma = jsonData['forma'];
    Caisse =jsonData['caisse'];
  }

  /**
   * return l'obj au forma json
   */
  String json() {
    var jsonDataAsString = '''{
   "id":$_id,
   "nom":"$_nom",
   "prix":$_prix,
   "forma":"$_forma",
   "indicat":$_indicat,
   "qt":$_qt,
   "idp":$_idp,
   "idcomande":$_idcomande,
   "caisse":$Caisse
   
}''';
    return jsonDataAsString;
  }
}
