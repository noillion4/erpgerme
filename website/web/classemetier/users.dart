import 'dart:convert' as JSON;

class User {
  /**
   * declaration
   */
  String _nom, _password;
  int _id;
  String _date;
  bool _shop, _home, _user, _stat, _founisseur, _client, _produit, _categorie;

  /**
   *
   * getter and setter
   */
  get Founisseur => _founisseur;

  set Founisseur(value) {
    _founisseur = value;
  }

  bool get Shop => _shop;

  set Shop(bool value) {
    _shop = value;
  }

  String get Date => _date;

  set Date(String value) {
    _date = value;
  }

  get Stat => _stat;

  set Stat(value) {
    _stat = value;
  }

  String get Nom => _nom;

  set Nom(String value) {
    _nom = value;
  }

  get user => _user;

  set user(value) {
    _user = value;
  }

  get Home => _home;

  set Home(value) {
    _home = value;
  }

  get Client => _client;

  set Client(value) {
    _client = value;
  }

  get Password => _password;

  set Password(value) {
    _password = value;
  }

  int get Id => _id;

  set Id(int value) {
    _id = value;
  }

  get Categorie => _categorie;

  set Categorie(value) {
    _categorie = value;
  }

  get Produit => _produit;

  set Produit(value) {
    _produit = value;
  }

  /**
   * contructor
   */
  User.v() {}

  User(id, password, nom, date, shop, home, user, stat, founisseur, client,
      produit, categorie) {
    this._id = id;
    this._password = password;
    this._date = date;
    this._shop = shop;
    this._home = home;
    this._nom = nom;
    this._user = user;
    this._stat = stat;
    this._founisseur = founisseur;
    this._client = client;
    this._produit = produit;
    this._categorie = categorie;
  }

  /**
   * contructeur avec json
   */
  User.J(json2) {
    print(json2);
    Map jsonData = JSON.jsonDecode(json2);
    _id = jsonData['id'];
    _nom = jsonData['nom'];
    _password = jsonData['password'];
    _date = jsonData['date'];
    _shop = jsonData['shop'];
    _home = jsonData['home'];
    _user = jsonData['user'];
    _stat = jsonData['stat'];
    _founisseur = jsonData['founisseur'];
    _client = jsonData['client'];
    _categorie = jsonData['categorie'];
    _produit = jsonData['produit'];
  }

  /**
   * return l'obj au forma json
   */

  String json() {
    var jsonDataAsString = '''{
   "id":$_id,
   "nom":"$_nom",
   "password": "$_password",
   "date":"_$_date",
   "shop":$_shop,
   "home":$_home,
   "user":$_user,
   "stat": $_stat,
   "founisseur": $_founisseur,
   "client":$_client ,
   "categorie":$_categorie,
   "produit":$_produit
}''';
    return jsonDataAsString;
  }
}
