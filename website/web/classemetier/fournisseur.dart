import 'dart:convert' as JSON;

class Fournisseur {
  /**
   * declaration
   */
  String _nom, _email, _adresse, _cp;
  int _id;

  String _gsm, _tva, _banque;

  /**
   * setter et getter
   */

  get Adresse => _adresse;

  set Adresse(value) {
    _adresse = value;
  }

  get Tva => _tva;

  set Tva(value) {
    _tva = value;
  }

  get Cp => _cp;

  set Cp(value) {
    _cp = value;
  }

  String get Nom => _nom;

  set Nom(String value) {
    _nom = value;
  }

  get Email => _email;

  set Email(value) {
    _email = value;
  }

  int get Id => _id;

  set Id(int value) {
    _id = value;
  }

  String get Gsm => _gsm;

  set Gsm(String value) {
    _gsm = value;
  }

  get Banque => _banque;

  set Banque(value) {
    _banque = value;
  }

  /**
   * Constructeur
   */
  Fournisseur(id, banque, nom, gsm, tva, email, cp, adresse) {
    this._id = id;
    this._adresse = adresse;
    this._gsm = gsm;
    this._tva = tva;
    this._email = email;
    this._nom = nom;
    this._cp = cp;
    this._banque = banque;
  }

  /**
   * Construceur avec json
   */
  Fournisseur.J(json2) {
    print(json2);
    Map jsonData = JSON.jsonDecode(json2);
    _id = jsonData['id'];
    _nom = jsonData['nom'];
    _gsm = jsonData['gsm'];
    _tva = jsonData['tva'];
    _email = jsonData['email'];
    _adresse = jsonData['adresse'];
    _banque = jsonData['banque'];
    _cp = jsonData['cp'];
  }

  /**
   * return l'obj au forma json
   */
  String json() {
    var jsonDataAsString = '''{
   "id":$_id,
   "adresse":"$_adresse",
   "gsm": "$_gsm",
   "tva":"$_tva",
   "nom":"$_nom",
   "email":"$_email",
   "banque":"$_banque",
   "cp":"$_cp"
}''';
    return jsonDataAsString;
  }
}
