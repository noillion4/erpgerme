import 'dart:convert' as JSON;

class Client {
  /**
   * declaration
   */
  String _nom, _prenom, _email;
  int _id;

  String _gsm, _tva;

  int _pourcent;

  /**
   * getter et setter
   */

  get Tva => _tva;

  set Tva(value) {
    _tva = value;
  }

  String get Nom => _nom;

  set Nom(String value) {
    _nom = value;
  }

  int get Id => _id;

  set Id(int value) {
    _id = value;
  }

  int get Pourcent => _pourcent;

  set Pourcent(int value) {
    _pourcent = value;
  }

  get Email => _email;

  set Email(value) {
    _email = value;
  }

  get Prenom => _prenom;

  set Prenom(value) {
    _prenom = value;
  }

  String get Gsm => _gsm;

  set Gsm(String value) {
    _gsm = value;
  }

  /**
   * constructeur
   */
  Client(id, prenom, nom, gsm, tva, email, pourcent) {
    this._id = id;
    this._prenom = prenom;
    this._gsm = gsm;
    this._tva = tva;
    this._email = email;
    this._nom = nom;
    this._pourcent = pourcent;
  }

  /**
   * constructeur json
   */
  Client.J(json2) {
    print(json2);
    Map jsonData = JSON.jsonDecode(json2);
    _id = jsonData['id'];
    _nom = jsonData['nom'];
    _gsm = jsonData['gsm'];
    _tva = jsonData['tva'];
    _email = jsonData['email'];
    _prenom = jsonData['prenom'];
    _pourcent = jsonData['pourcent'];
  }

  /**
   * return l'obj en forma json
   */
  String json() {
    var jsonDataAsString = '''{
   "id":$_id,
   "prenom":"$_prenom",
   "gsm": "$_gsm",
   "tva":"$_tva",
   "nom":"$_nom",
   "email":"$_email",
   "pourcent":$_pourcent
}''';
    return jsonDataAsString;
  }
}
