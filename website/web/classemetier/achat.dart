import 'dart:convert' as JSON;

class achat {
  /**
   * declaration
   */
  String _nom;
  double _prix;
  int _tva;
  int _id, _idfacture, _idproduit;
  double _qt;

  /**
   * get et setter
   */
  String get Nom => _nom;

  int get Tva => _tva;

  set Tva(int value) {
    _tva = value;
  }

  get Idfacture => _idfacture;

  set I(value) {
    _idfacture = value;
  }

  set Nom(String value) => _nom = value;

  get Idproduit => _idproduit;

  int get Id => _id;

  double get Qt => _qt;

  set Qt(double value) {
    _qt = value;
  }

  double get Prix => _prix;

  set Prix(double value) {
    _prix = value;
  }

  set Id(int value) {
    _id = value;
  }

  set idproduit(value) {
    _idproduit = value;
  }

  /**
   * constructeur
   */

  achat(id, nom, prix, idfacture, qt, tva, idproduit) {
    try {
      this._id = id;
      this._prix = prix;
      this._idproduit = idproduit;
      this._idfacture = idfacture;
      this._nom = nom;
      this._tva = tva;
      this._qt = qt;
    } catch (e) {
      this._id = id;
      this._prix = prix;
      this._idfacture = idfacture;
      this._idproduit = idproduit;
      this._nom = nom;
      this._tva = (tva as double).toInt();
      this._qt = qt;
    }
  }

  /**
   * constructeur json
   */
  achat.J(json2) {
    print(json2);
    Map jsonData = JSON.jsonDecode(json2);
    _id = jsonData['id'];
    _nom = jsonData['nom'];
    _prix = jsonData['prix'].toDouble();
    _idproduit = jsonData['idproduit'];
    _idfacture = jsonData['idfacture'];
    _tva = jsonData['tva'];
    _qt = jsonData['qt'].toDouble();
  }

  /**
   * return l'obj en forma json
   */
  String json() {
    var jsonDataAsString = '''{
   "id":$_id,
   "nom":"$_nom",
   "prix": $_prix,
   "idfacture":$_idfacture,
   "idproduit":$_idproduit,
   "tva":$_tva,
   "qt": $_qt
}''';
    return jsonDataAsString;
  }
}
