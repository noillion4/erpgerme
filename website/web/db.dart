import 'dart:html';
import 'dart:convert';
import 'dart:html' as html;

/**
 *se connecte au serveur pour l'acces a la db
 */

/**
 * requete au serveur
 * @ parame methode : la methode utiliser sur le serveur
 * @ param obj : l'obj a trasmetre au serveur
 */
Future<String> makeRequest(methode, obj) async {
  var path = 'http://localhost:4040/?m=${methode}&o=${obj}';
  try {
    // Make the GET request

    final jsonString = await html.HttpRequest.getString(path);
    // The request succeeded. Process the JSON.
    return jsonString;
  } catch (e) {
    // The GET request failed. Handle the error.
    print('Couldn\'t open $path');
    return "false";
  }
}
