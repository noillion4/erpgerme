import 'dart:io';
import 'dart:convert';
import 'dart:async';
import 'package:sqljocky5/auth/auth_handler.dart';
import 'package:sqljocky5/auth/character_set.dart';
import 'package:sqljocky5/auth/handshake_handler.dart';
import 'package:sqljocky5/auth/ssl_handler.dart';
import 'package:sqljocky5/common/logging.dart';
import 'package:sqljocky5/comm/buffered_socket.dart';
import 'package:sqljocky5/comm/comm.dart';
import 'package:sqljocky5/comm/common.dart';
import 'package:sqljocky5/comm/receiver.dart';
import 'package:sqljocky5/comm/sender.dart';
import 'package:sqljocky5/connection/connection.dart';
import 'package:sqljocky5/connection/impl.dart';
import 'package:sqljocky5/connection/settings.dart';
import 'package:sqljocky5/constants.dart';
import 'package:sqljocky5/exceptions/client_error.dart';
import 'package:sqljocky5/exceptions/exceptions.dart';
import 'package:sqljocky5/exceptions/mysql_exception.dart';
import 'package:sqljocky5/exceptions/protocol_error.dart';
import 'package:sqljocky5/handlers/debug_handler.dart';
import 'package:sqljocky5/handlers/handler.dart';
import 'package:sqljocky5/handlers/ok_packet.dart';
import 'package:sqljocky5/handlers/parameter_packet.dart';
import 'package:sqljocky5/handlers/ping_handler.dart';
import 'package:sqljocky5/handlers/quit_handler.dart';
import 'package:sqljocky5/handlers/use_db_handler.dart';
import 'package:sqljocky5/prepared_statements/close_statement_handler.dart';
import 'package:sqljocky5/prepared_statements/execute_query_handler.dart';
import 'package:sqljocky5/prepared_statements/prepared_query.dart';
import 'package:sqljocky5/prepared_statements/prepare_handler.dart';
import 'package:sqljocky5/prepared_statements/prepare_ok_packet.dart';
import 'package:sqljocky5/query/query_stream_handler.dart';
import 'package:sqljocky5/query/result_set_header_packet.dart';
import 'package:sqljocky5/results/binary_data_packet.dart';
import 'package:sqljocky5/results/blob.dart';
import 'package:sqljocky5/results/field.dart';
import 'package:sqljocky5/results/results.dart';
import 'package:sqljocky5/results/row.dart';
import 'package:sqljocky5/results/standard_data_packet.dart';
import 'package:sqljocky5/sqljocky.dart';
import 'package:sqljocky5/utils/buffer.dart';
import '../../website/web/classemetier/Client.dart';
import '../../website/web/classemetier/vente.dart';
import '../../website/web/classemetier/achat.dart';
import '../../website/web/classemetier/users.dart';
import '../../website/web/classemetier/produit.dart';
import '../../website/web/classemetier/fournisseur.dart';

import 'package:postgres/postgres.dart';

class ProduitDAO {

  var _Sqlcont = ConnectionSettings(
    user: "caisse",
    password: "8767ce246C",
    host: "localhost",
    port: 3306,
    db: "dart",
  );

  /**
   * @return la liste des  activer
   */
  Future<List<produit>> listeproduit() async {
    List<produit> c = new List();
    MySqlConnection conn;
    // create a connection
    var s =_Sqlcont;
    conn = await MySqlConnection.connect(s);
    Results results =
    (await conn.prepared("SELECT * FROM `produit`  where status=1", [1]));
    results.forEach((Row row) {
      // Access columns by index
      print('Name: ${row.byName('id')}, email: ${row[1]}');
      // new Client(id, prenom, nom, gsm, tva, email, pourcent)

      produit l =
      new produit(row[0], row[1], row[2], row[3], row[4], row[5], row[7],row[8]);
      // Access columns by name
      c.add(l);
    });
    return c;
  }

  /**
   * @return la liste de tous les porduits
   */
  Future<List<produit>> listeproduitall() async {
    List<produit> c = new List();
    MySqlConnection conn;
    // create a connection
    var s =_Sqlcont;
    conn = await MySqlConnection.connect(s);
    Results results = (await conn.prepared("SELECT * FROM `produit`  ", [1]));
    results.forEach((Row row) {
      // Access columns by index
      print('Name: ${row.byName('id')}, email: ${row[1]}');
      // new Client(id, prenom, nom, gsm, tva, email, pourcent)

      produit l =
      new produit(row[0], row[1], row[2], row[3], row[4], row[5], row[7],row[8]);
      // Access columns by name
      c.add(l);
    });
    return c;
  }

  /**
   * @return la liste des produit descativer
   */
  Future<List<produit>> listeproduit2() async {
    List<produit> c = new List();
    MySqlConnection conn;
    // create a connection
    var s =_Sqlcont;
    conn = await MySqlConnection.connect(s);
    Results results =
    (await conn.prepared("SELECT * FROM `produit`  where status!=1", [1]));
    results.forEach((Row row) {
      // Access columns by index
      print('Name: ${row.byName('id')}, email: ${row[1]}');
      // new Client(id, prenom, nom, gsm, tva, email, pourcent)

      produit l =
      new produit(row[0], row[1], row[2], row[3], row[4], row[5], row[7],row[8]);
      // Access columns by name
      c.add(l);
    });
    return c;
  }
/**
 * @ param string : nom du produit
 * @ return une liste de produit contenant le nom
 */
  Future<List<produit>> findproduit(name) async {
    List<produit> c = new List();
    MySqlConnection conn;
    // create a connection
    var s =_Sqlcont;
    conn = await MySqlConnection.connect(s);
    Results results = (await conn.prepared(
        "SELECT * FROM `produit` where UPPER(nom) LIKE UPPER('%$name%')and status =1",
        [1]));
    results.forEach((Row row) {
      // Access columns by index
      print('Name: ${row.byName('id')}, email: ${row[1]}');
      // new Client(id, prenom, nom, gsm, tva, email, pourcent)

      produit l =
      new produit(row[0], row[1], row[2], row[3], row[4], row[5], row[7],row[8]);
      // Access columns by name
      c.add(l);
    });
    return c;
  }
/**
 * @param id de la categorie
 * @return la liste de produit de la categorie
 */
  Future<List<produit>> findproduitcat(idcat) async {
    List<produit> c = new List();
    MySqlConnection conn;
    // create a connection
    var s =_Sqlcont;
    conn = await MySqlConnection.connect(s);
    Results results = (await conn.prepared(
        "SELECT * FROM `produit` where id_categorie = $idcat and status =1",
        [1]));
    results.forEach((Row row) {
      // Access columns by index
      print('Name: ${row.byName('id')}, email: ${row[1]}');
      // new Client(id, prenom, nom, gsm, tva, email, pourcent)

      produit l =
      new produit(row[0], row[1], row[2], row[3], row[4], row[5], row[7],row[8]);
      // Access columns by name
      c.add(l);
    });
    return c;
  }
/**
 * ajoute un produit dans la db
 * @param le produit a rajouter au fromat json
 * @return le restulat
 */
  Future addproduit(j) async {
    print(j);
    produit p = new produit.J(j);

    MySqlConnection conn;
    // create a connection
    var s = ConnectionSettings(
        user: "caisse",
        password: "8767ce246C",
        host: "localhost",
        port: 3306,
        db: "dart");
    conn = await MySqlConnection.connect(s);
    Results results;

    DateTime d = DateTime.now();
    results = (await conn.execute(
        "insert INTO produit VALUES (null,'${p.Nom}' ,${p.Prix},'${p.Forma}','${p.Indicat}',${p.Tva},1,${p.Id_cat},${p.Nb});"));

    return results;
  }
/**
 * met ajoure un produit das la db
 * @param le produit au forma json
 * @return le resultat
 */
  Future uppproduit(j) async {
    print(j);
    produit p = new produit.J(j);

    MySqlConnection conn;
    // create a connection
    var s =_Sqlcont;
    conn = await MySqlConnection.connect(s);
    Results results;

    DateTime d = DateTime.now();
    results = (await conn.execute(
        "update produit set nom= '${p.Nom}' , prix= ${p.Prix}, forma= '${p.Forma}', indica='${p.Indicat}',tva=${p.Tva},id_categorie=${p.Id_cat} ,nb=${p.Nb} where id =${p.Id};"));

    return results;
  }
/**
 * met ajoure le status d'un produits
 * @param id = id du produit
 * @param i = le status
 * @return la resulta
 */
  Future upstat(String id, int i) async {
    int it = int.parse(id);
    MySqlConnection conn;
    // create a connection
    var s =_Sqlcont;
    conn = await MySqlConnection.connect(s);
    Results results;

    DateTime d = DateTime.now();
    results =
    (await conn.execute("update produit set status =$i where id= ${it} ;"));

    return results;
  }}