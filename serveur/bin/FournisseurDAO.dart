import 'dart:io';
import 'dart:convert';
import 'dart:async';
import 'package:sqljocky5/auth/auth_handler.dart';
import 'package:sqljocky5/auth/character_set.dart';
import 'package:sqljocky5/auth/handshake_handler.dart';
import 'package:sqljocky5/auth/ssl_handler.dart';
import 'package:sqljocky5/common/logging.dart';
import 'package:sqljocky5/comm/buffered_socket.dart';
import 'package:sqljocky5/comm/comm.dart';
import 'package:sqljocky5/comm/common.dart';
import 'package:sqljocky5/comm/receiver.dart';
import 'package:sqljocky5/comm/sender.dart';
import 'package:sqljocky5/connection/connection.dart';
import 'package:sqljocky5/connection/impl.dart';
import 'package:sqljocky5/connection/settings.dart';
import 'package:sqljocky5/constants.dart';
import 'package:sqljocky5/exceptions/client_error.dart';
import 'package:sqljocky5/exceptions/exceptions.dart';
import 'package:sqljocky5/exceptions/mysql_exception.dart';
import 'package:sqljocky5/exceptions/protocol_error.dart';
import 'package:sqljocky5/handlers/debug_handler.dart';
import 'package:sqljocky5/handlers/handler.dart';
import 'package:sqljocky5/handlers/ok_packet.dart';
import 'package:sqljocky5/handlers/parameter_packet.dart';
import 'package:sqljocky5/handlers/ping_handler.dart';
import 'package:sqljocky5/handlers/quit_handler.dart';
import 'package:sqljocky5/handlers/use_db_handler.dart';
import 'package:sqljocky5/prepared_statements/close_statement_handler.dart';
import 'package:sqljocky5/prepared_statements/execute_query_handler.dart';
import 'package:sqljocky5/prepared_statements/prepared_query.dart';
import 'package:sqljocky5/prepared_statements/prepare_handler.dart';
import 'package:sqljocky5/prepared_statements/prepare_ok_packet.dart';
import 'package:sqljocky5/query/query_stream_handler.dart';
import 'package:sqljocky5/query/result_set_header_packet.dart';
import 'package:sqljocky5/results/binary_data_packet.dart';
import 'package:sqljocky5/results/blob.dart';
import 'package:sqljocky5/results/field.dart';
import 'package:sqljocky5/results/results.dart';
import 'package:sqljocky5/results/row.dart';
import 'package:sqljocky5/results/standard_data_packet.dart';
import 'package:sqljocky5/sqljocky.dart';
import 'package:sqljocky5/utils/buffer.dart';
import '../../website/web/classemetier/Client.dart';
import '../../website/web/classemetier/vente.dart';
import '../../website/web/classemetier/achat.dart';
import '../../website/web/classemetier/users.dart';
import '../../website/web/classemetier/produit.dart';
import '../../website/web/classemetier/fournisseur.dart';

import 'package:postgres/postgres.dart';

class FournisseurDAO {

  var _Sqlcont = ConnectionSettings(
    user: "caisse",
    password: "8767ce246C",
    host: "localhost",
    port: 3306,
    db: "dart",
  );

  /**
   * ajoute un fournisseur
   * @param json : le fournisseur au forma json
   * @return le resulta
   */
  Future addfournis(json) async {
    List<Map> c = new List();
    MySqlConnection conn;
    // create a connection
    var s =_Sqlcont;
    var f = new Fournisseur.J(json);
    conn = await MySqlConnection.connect(s);
    Results results = (await conn.prepared(
        "insert into fournisseur Values (null,'${f.Nom}','${f.Adresse}','${f.Gsm}','${f.Email}','${f.Tva}','${f.Banque}','${f.Cp}')",
        [1])) as Results;

    return results;
  }
/**
 * @return la liste des fournisseurs
 */
  Future<List<Fournisseur>> listfourni() async {
    List<Fournisseur> c = new List();
    MySqlConnection conn;
    // create a connection
    var s =_Sqlcont;

    conn = await MySqlConnection.connect(s);
    Results results = (await conn.prepared("select * from fournisseur", [1])) as Results;
    results.forEach((Row row) {
      // Access columns by index
      print('Name: ${row.byName('id')}, email: ${row[1]}');
      // new Client(id, prenom, nom, gsm, tva, email, pourcent)
      //Fournisseur(id, banque, nom, gsm, tva, email, cp, adresse)
      //             0    6     1     3    5   4      7     2
      //(null,'${f.nom}','${f.adresse}','${f.gsm}','${f.email}','${f.tva}','${f.banque}','${f.cp}')
      var l = new Fournisseur(
          row[0], row[6], row[1], row[3], row[5], row[4], row[7], row[2]);
      // Access columns by name
      c.add(l);
    });

    return c;
  }
/**
 * modifie un fournisseur dans la db
 * @param fourn : le fournisseur au forma json
 * @return le resulta
 */
  Future upfourni(fourn) async {
    Fournisseur f = new Fournisseur.J(fourn);
    MySqlConnection conn;
    // create a connection
    var s =_Sqlcont;

    conn = await MySqlConnection.connect(s);
    //(`id`, `nom`, `adress`, `tel`, `email`, `tva`, `banque`, `cp`)
    Results results = (await conn.prepared(
        "update  fournisseur set nom = '${f.Nom}', adress='${f.Adresse}', tel ='${f.Gsm}' , email = '${f.Email}',tva ='${f.Tva}' ,banque ='${f.Banque}' , cp = '${f.Cp}' where id=${f.Id}; ",
        [1])) as Results;
    //NSERT INTO `dart`.`facturefour` (`id`, `date`, `idcategorie`, `payer`, `idfournisseur`) VALUES ('1', '2', '1', 'o', '1');
    return results;
  }
}