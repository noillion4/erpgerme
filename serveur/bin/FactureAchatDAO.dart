import 'dart:io';
import 'dart:convert';
import 'dart:async';
import 'package:sqljocky5/auth/auth_handler.dart';
import 'package:sqljocky5/auth/character_set.dart';
import 'package:sqljocky5/auth/handshake_handler.dart';
import 'package:sqljocky5/auth/ssl_handler.dart';
import 'package:sqljocky5/common/logging.dart';
import 'package:sqljocky5/comm/buffered_socket.dart';
import 'package:sqljocky5/comm/comm.dart';
import 'package:sqljocky5/comm/common.dart';
import 'package:sqljocky5/comm/receiver.dart';
import 'package:sqljocky5/comm/sender.dart';
import 'package:sqljocky5/connection/connection.dart';
import 'package:sqljocky5/connection/impl.dart';
import 'package:sqljocky5/connection/settings.dart';
import 'package:sqljocky5/constants.dart';
import 'package:sqljocky5/exceptions/client_error.dart';
import 'package:sqljocky5/exceptions/exceptions.dart';
import 'package:sqljocky5/exceptions/mysql_exception.dart';
import 'package:sqljocky5/exceptions/protocol_error.dart';
import 'package:sqljocky5/handlers/debug_handler.dart';
import 'package:sqljocky5/handlers/handler.dart';
import 'package:sqljocky5/handlers/ok_packet.dart';
import 'package:sqljocky5/handlers/parameter_packet.dart';
import 'package:sqljocky5/handlers/ping_handler.dart';
import 'package:sqljocky5/handlers/quit_handler.dart';
import 'package:sqljocky5/handlers/use_db_handler.dart';
import 'package:sqljocky5/prepared_statements/close_statement_handler.dart';
import 'package:sqljocky5/prepared_statements/execute_query_handler.dart';
import 'package:sqljocky5/prepared_statements/prepared_query.dart';
import 'package:sqljocky5/prepared_statements/prepare_handler.dart';
import 'package:sqljocky5/prepared_statements/prepare_ok_packet.dart';
import 'package:sqljocky5/query/query_stream_handler.dart';
import 'package:sqljocky5/query/result_set_header_packet.dart';
import 'package:sqljocky5/results/binary_data_packet.dart';
import 'package:sqljocky5/results/blob.dart';
import 'package:sqljocky5/results/field.dart';
import 'package:sqljocky5/results/results.dart';
import 'package:sqljocky5/results/row.dart';
import 'package:sqljocky5/results/standard_data_packet.dart';
import 'package:sqljocky5/sqljocky.dart';
import 'package:sqljocky5/utils/buffer.dart';
import '../../website/web/classemetier/Client.dart';
import '../../website/web/classemetier/vente.dart';
import '../../website/web/classemetier/achat.dart';
import '../../website/web/classemetier/users.dart';
import '../../website/web/classemetier/produit.dart';
import '../../website/web/classemetier/fournisseur.dart';

import 'package:postgres/postgres.dart';

class FactureAchatDAO {

  var _Sqlcont = ConnectionSettings(
    user: "caisse",
    password: "8767ce246C",
    host: "localhost",
    port: 3306,
    db: "dart",
  );

  /**
   * @return la liste des achat pour un fournisseur
   * @param id du fournissseur
   */
  Future<List> listefactureacha(id) async {
    List<Map> c = new List();
    MySqlConnection conn;
    // create a connection
    var s =_Sqlcont;
    conn = await MySqlConnection.connect(s);
    Results results =  await conn
        .prepared("SELECT * FROM facturefour where idfournisseur=$id;", [1]) as Results;
    results.forEach((Row row) {
      // Access columns by index
      Map fact = {
        "idategorie": row[2],
        "date": "${row[1]}",
        "payer": "${row[3]}",
        "idfournisseur": row[4],
        "numfact": "${row[5]}",
        "id": row[0]
      };
      c.add(fact);
      // new Client(id, prenom, nom, gsm, tva, email, pourcent)
    });
    return c;
  }
  /**
   * charge la facture et ses achat
   * @param id : l'id de la facture
   * @return la facture
   */
  Future<List> chargefactureacha(id) async {
    List<produit> c = new List();
    MySqlConnection conn;
    List<Map> l = new List();
    // create a connection
    var s =_Sqlcont;
    print(id);
    conn = await MySqlConnection.connect(s);
    Results results = (await conn.prepared(
        " select v.nom ,v.qt , v.qt*v.prix,  v.qt*v.prix+ v.qt*v.prix/100*v.tva,c.id , c.date from facturefour c ,achat v  where c.id = $id and v.idfacture =c.id ",
        [1])) as Results;
    print(results.length);

    results.forEach((f) {
      print(f[0]);
      print("ttt");
      Map fact = {
        'nom': '${f[0]}',
        'quatitier': f[1],
        'total': f[2],
        'ttva': f[3],
        'id': f[4],
        'date': '${f[5]}'
      };
      l.add(fact);
    });
    print(l);
    return l;
  }
  /**
   * charge les achat de la facture
   * @param id : l'id de la facture
   * @return les achat
   */
  Future<List<achat>> chargeacha(id) async {
    List<produit> c = new List();
    MySqlConnection conn;
    List<achat> l = new List<achat>();
    // create a connection
    var s =_Sqlcont;
    print(id);
    conn = await MySqlConnection.connect(s);
    Results results = (await conn.prepared(
        " select v.id ,v.nom ,v.qt  ,v.prix ,v.tva ,v.idfacture, v.idproduit From facturefour c ,achat v  where c.id = $id and v.idfacture =c.id ",
        [1])) as Results;
    print(results.length);

    results.forEach((f) {
      print(f[0] +f[3]+f[5]+f[2]+f[4]+f[6]);
      print("ttt");
      //new achat(id, nom, prix, idfacture, qt, tva, idproduit)
      achat fact = new achat(f[0], f[1], f[3] , f[5], f[2], f[4], f[6]);
      l.add(fact);
    });
    print(l);
    return l;
  }
  /**
   * Modifier une facture achat
   * @param : une facture au forma json
   * @param : une liste d'achat au forma json
   * @return le resulta
   */
  Future upfactachat(id, jlachat,sta) async {

    print(jlachat);

    var json2 = jlachat;
    List<achat> lis = new List<achat>();
    List j = jsonDecode(json2);
    j.forEach((f) => lis.add(new achat.J(f)));
    MySqlConnection conn;
    // create a connection
    var s =_Sqlcont;
    conn = await MySqlConnection.connect(s);
    Results results;
    print(id);

    //NSERT INTO `dart`.`facturefour` (`id`, `date`, `idcategorie`, `payer`, `idfournisseur`) VALUES ('1', '2', '1', 'o', '1');
    await conn.execute(
        "delete from achat where idfacture =$id;");
    await conn.execute(
        "update facturefour set payer = '$sta'  where id =$id;");

    lis.forEach((f) async =>
    results = (await conn.prepared(
        "INSERT INTO `achat` VALUES (0,'${f.Nom}',${f.Qt},${f.Prix},${f.Tva},$id,${f.Idproduit})",
        [2])) as Results);

    // Access columns by index
//SELECT p.nom ,sum(v.qt) , sum(v.qt*v.prix)from facture f ,commade c ,vente v ,produit p where f.id=c.id_facture and v.Id_commade =c.id and p.id = v.id_produit group by p.nom;
    // new Client(id, prenom, nom, gsm, tva, email, pourcent)

    return 0;
  }
  /**
   * creer une facture achat
   * @param : une facture au forma json
   * @param : une liste d'achat au forma json
   * @return le resulta
   */
  Future createfactachat(jfacture, jlachat) async {
    Map js = jsonDecode(jfacture);
    print(jlachat);
    print(jfacture);
    var json2 = jlachat;
    List<achat> lis = new List<achat>();
    List j = jsonDecode(json2);
    j.forEach((f) => lis.add(new achat.J(f)));
    MySqlConnection conn;
    // create a connection
    var s =_Sqlcont;
    conn = await MySqlConnection.connect(s);
    Results results;
    int id;
    var id2;
    var date = js['date'];
    var idcategorie = js['idategorie'];
    var payer = js['payer'];
    var idfournisseur = js['idfournisseur'];
    DateTime d = DateTime.now();
    results =
    //NSERT INTO `dart`.`facturefour` (`id`, `date`, `idcategorie`, `payer`, `idfournisseur`) VALUES ('1', '2', '1', 'o', '1');
    (await conn.execute(
        "insert INTO facturefour VALUES (null, '${js['date']}' ,${js['idategorie']},'${js['payer']}',${js['idfournisseur']},'${js['numfact']}');")) as Results;
    results = (await conn.prepared(
        "select id from facturefour where id=last_insert_id();", [2])) as Results;
    id2 = results.elementAt(0);

    id = id2[0];
    print(id);
    lis.forEach((f) async =>
    //INSERT INTO `dart`.`achat` (`id`, `nom`, `qt`, `prix`, `tva`, `idfacture`, `idproduit`) VALUES ('1', '1', '1', '1', '1', '1', '1');
    //achat(id, nom, prix, idfacture, qt, tva, idproduit)
    results = (await conn.prepared(
        "INSERT INTO `achat` VALUES (0,'${f.Nom}',${f.Qt},${f.Prix},${f.Tva},$id,${f.Idproduit})",
        [2])) as Results);

    // Access columns by index
//SELECT p.nom ,sum(v.qt) , sum(v.qt*v.prix)from facture f ,commade c ,vente v ,produit p where f.id=c.id_facture and v.Id_commade =c.id and p.id = v.id_produit group by p.nom;
    // new Client(id, prenom, nom, gsm, tva, email, pourcent)

    return 0;
  }




}