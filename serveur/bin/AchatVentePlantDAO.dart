import 'dart:io';
import 'dart:convert';
import 'dart:async';
import 'package:sqljocky5/auth/auth_handler.dart';
import 'package:sqljocky5/auth/character_set.dart';
import 'package:sqljocky5/auth/handshake_handler.dart';
import 'package:sqljocky5/auth/ssl_handler.dart';
import 'package:sqljocky5/common/logging.dart';
import 'package:sqljocky5/comm/buffered_socket.dart';
import 'package:sqljocky5/comm/comm.dart';
import 'package:sqljocky5/comm/common.dart';
import 'package:sqljocky5/comm/receiver.dart';
import 'package:sqljocky5/comm/sender.dart';
import 'package:sqljocky5/connection/connection.dart';
import 'package:sqljocky5/connection/impl.dart';
import 'package:sqljocky5/connection/settings.dart';
import 'package:sqljocky5/constants.dart';
import 'package:sqljocky5/exceptions/client_error.dart';
import 'package:sqljocky5/exceptions/exceptions.dart';
import 'package:sqljocky5/exceptions/mysql_exception.dart';
import 'package:sqljocky5/exceptions/protocol_error.dart';
import 'package:sqljocky5/handlers/debug_handler.dart';
import 'package:sqljocky5/handlers/handler.dart';
import 'package:sqljocky5/handlers/ok_packet.dart';
import 'package:sqljocky5/handlers/parameter_packet.dart';
import 'package:sqljocky5/handlers/ping_handler.dart';
import 'package:sqljocky5/handlers/quit_handler.dart';
import 'package:sqljocky5/handlers/use_db_handler.dart';
import 'package:sqljocky5/prepared_statements/close_statement_handler.dart';
import 'package:sqljocky5/prepared_statements/execute_query_handler.dart';
import 'package:sqljocky5/prepared_statements/prepared_query.dart';
import 'package:sqljocky5/prepared_statements/prepare_handler.dart';
import 'package:sqljocky5/prepared_statements/prepare_ok_packet.dart';
import 'package:sqljocky5/query/query_stream_handler.dart';
import 'package:sqljocky5/query/result_set_header_packet.dart';
import 'package:sqljocky5/results/binary_data_packet.dart';
import 'package:sqljocky5/results/blob.dart';
import 'package:sqljocky5/results/field.dart';
import 'package:sqljocky5/results/results.dart';
import 'package:sqljocky5/results/row.dart';
import 'package:sqljocky5/results/standard_data_packet.dart';
import 'package:sqljocky5/sqljocky.dart';
import 'package:sqljocky5/utils/buffer.dart';
import '../../website/web/classemetier/Client.dart';
import '../../website/web/classemetier/vente.dart';
import '../../website/web/classemetier/achat.dart';
import '../../website/web/classemetier/users.dart';
import '../../website/web/classemetier/produit.dart';
import '../../website/web/classemetier/fournisseur.dart';

import 'package:postgres/postgres.dart';

class AchaVentePlantSDAO {

  var _Sqlcont = ConnectionSettings(
    user: "caisse",
    password: "8767ce246C",
    host: "localhost",
    port: 3306,
    db: "dart",
  );
  /**
   * @param dtfn : date de fin
   * @param dtd :date de debut
   * @return en liste json le total des achats pour chaque plant entre deux date
   */
  Future<List> listachatplant(dtfin, dtd) async {
    List<Map> c = new List();
    MySqlConnection conn;
    // create a connection
    var s =_Sqlcont;
    conn = await MySqlConnection.connect(s);
    Results results = (await conn.prepared(
        "SELECT distinct p.id ,p.nom ,if( sum(v.qt)>=(select sum(qt) from vente ) , 0, sum(v.qt))"
            " FROM produit p , facturefour f , achat v "
            " where "
            " (not exists (select b.id from achat b "
            " where p.id =b.idproduit ) or "
            " v.idfacture =f.id and v.idproduit=p.id) "
            " and f.date between '2019-01-01' and '2019-02-01' "
            " group by p.id"
            ";",
        [1])) as Results;
    results.forEach((Row row) {
      // Access columns by index
      Map fact = {'nom': '${row[1]}', 'total': row[2]};
      c.add(fact);
      // new Client(id, prenom, nom, gsm, tva, email, pourcent)
    });
    return c;
  }

  /**
   * @param dtfn : date de fin
   * @param dtd :date de debut
   * @return en liste json le total des vente pour chaque plant entre deux date
   */

  Future<List> listventeplant(dtfin, dtd) async {
    List<Map> c = new List();
    MySqlConnection conn;
    // create a connection
    var s =_Sqlcont;
    conn = await MySqlConnection.connect(s);
    Results results = (await conn.prepared(
        "SELECT distinct p.id ,p.nom ,if( sum(v.qt)>=(select sum(qt) from vente ) , 0, sum(v.qt))"
            " FROM produit p , commade f , vente v"
            " where"
            " (not exists (select b.id from achat b"
            " where p.id =b.idproduit ) or"
            " v.Id_commade =f.id and v.id_produit=p.id)"
            " and f.date  between '$dtd' and '$dtfin' "
            "  group by p.id"
            ";",
        [1])) as Results;
    results.forEach((Row row) {
      // Access columns by index
      Map fact = {'nom': '${row[1]}', 'total': row[2]};
      c.add(fact);
      // new Client(id, prenom, nom, gsm, tva, email, pourcent)
    });
    return c;
  }

}