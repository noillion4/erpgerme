import 'dart:io';
import 'dart:convert';
import 'dart:async';
import 'package:sqljocky5/auth/auth_handler.dart';
import 'package:sqljocky5/auth/character_set.dart';
import 'package:sqljocky5/auth/handshake_handler.dart';
import 'package:sqljocky5/auth/ssl_handler.dart';
import 'package:sqljocky5/common/logging.dart';
import 'package:sqljocky5/comm/buffered_socket.dart';
import 'package:sqljocky5/comm/comm.dart';
import 'package:sqljocky5/comm/common.dart';
import 'package:sqljocky5/comm/receiver.dart';
import 'package:sqljocky5/comm/sender.dart';
import 'package:sqljocky5/connection/connection.dart';
import 'package:sqljocky5/connection/impl.dart';
import 'package:sqljocky5/connection/settings.dart';
import 'package:sqljocky5/constants.dart';
import 'package:sqljocky5/exceptions/client_error.dart';
import 'package:sqljocky5/exceptions/exceptions.dart';
import 'package:sqljocky5/exceptions/mysql_exception.dart';
import 'package:sqljocky5/exceptions/protocol_error.dart';
import 'package:sqljocky5/handlers/debug_handler.dart';
import 'package:sqljocky5/handlers/handler.dart';
import 'package:sqljocky5/handlers/ok_packet.dart';
import 'package:sqljocky5/handlers/parameter_packet.dart';
import 'package:sqljocky5/handlers/ping_handler.dart';
import 'package:sqljocky5/handlers/quit_handler.dart';
import 'package:sqljocky5/handlers/use_db_handler.dart';
import 'package:sqljocky5/prepared_statements/close_statement_handler.dart';
import 'package:sqljocky5/prepared_statements/execute_query_handler.dart';
import 'package:sqljocky5/prepared_statements/prepared_query.dart';
import 'package:sqljocky5/prepared_statements/prepare_handler.dart';
import 'package:sqljocky5/prepared_statements/prepare_ok_packet.dart';
import 'package:sqljocky5/query/query_stream_handler.dart';
import 'package:sqljocky5/query/result_set_header_packet.dart';
import 'package:sqljocky5/results/binary_data_packet.dart';
import 'package:sqljocky5/results/blob.dart';
import 'package:sqljocky5/results/field.dart';
import 'package:sqljocky5/results/results.dart';
import 'package:sqljocky5/results/row.dart';
import 'package:sqljocky5/results/standard_data_packet.dart';
import 'package:sqljocky5/sqljocky.dart';
import 'package:sqljocky5/utils/buffer.dart';
import '../../website/web/classemetier/Client.dart';
import '../../website/web/classemetier/vente.dart';
import '../../website/web/classemetier/achat.dart';
import '../../website/web/classemetier/users.dart';
import '../../website/web/classemetier/produit.dart';
import '../../website/web/classemetier/fournisseur.dart';

import 'package:postgres/postgres.dart';

class CategorieDAO {

  var _Sqlcont = ConnectionSettings(
    user: "caisse",
    password: "8767ce246C",
    host: "localhost",
    port: 3306,
    db: "dart",
  );

  /**
   * ajoute une categorie
   * @param nom : le nom de la categorie
   * @param type : le type de la categorie
   * @return le resultat
   */

  Future addcategorie(nom, type) async {
    List<Map> c = new List();
    MySqlConnection conn;
    // create a connection
    var s =_Sqlcont;
    conn = await MySqlConnection.connect(s);
    Results results = (await conn
        .prepared("insert into Categorie Values (null,'$nom',$type)", [1])) as Results;

    return results;
  }
  /**
   * mat ajoure une categorie
   * @param id : l'id de la categorie
   * @param nom : le nom de la categorie
   * @param type : le type de la categorie
   * @return le resultat
   */
  Future upcategorie(id, nom, type) async {
    List<Map> c = new List();
    MySqlConnection conn;
    // create a connection
    var s =_Sqlcont;
    print(nom);
    conn = await MySqlConnection.connect(s);
    Results results = (await conn.prepared(
        "update  Categorie set nom = '$nom' , type =$type where id=$id; ", [1])) as Results;

    return results;
  }
/**
 * @return la liste des categorie
 */
  Future<List> listecategorie() async {
    List<Map> c = new List();
    MySqlConnection conn;
    // create a connection
    var s =_Sqlcont;
    conn = await MySqlConnection.connect(s);
    Results results = (await conn.prepared("SELECT * FROM Categorie;", [1])) as Results;
    results.forEach((Row row) {
      // Access columns by index
      Map fact = {'id': row[0], 'nom': '${row[1]}', 'type': row[2]};
      c.add(fact);
      // new Client(id, prenom, nom, gsm, tva, email, pourcent)
    });
    return c;
  }

  /**
   * @return la liste des categorie de vente
   */
  Future<List> listecategorie2() async {
    List<Map> c = new List();
    MySqlConnection conn;
    // create a connection
    var s =_Sqlcont;
    conn = await MySqlConnection.connect(s);
    Results results =
    (await conn.prepared("SELECT * FROM Categorie where type=1;", [1])) as Results;
    results.forEach((Row row) {
      // Access columns by index
      Map fact = {'id': row[0], 'nom': '${row[1]}', 'type': row[2]};
      c.add(fact);
      // new Client(id, prenom, nom, gsm, tva, email, pourcent)
    });
    return c;
  }












}
