import 'dart:io';
import '../../website/web/classemetier/Client.dart';
import '../../website/web/classemetier/vente.dart';
import '../../website/web/classemetier/achat.dart';
import '../../website/web/classemetier/users.dart';
import '../../website/web/classemetier/produit.dart';
import '../../website/web/classemetier/fournisseur.dart';
import 'db.dart';

import 'dart:convert';

var ret;

Future main() async {
  var server = await HttpServer.bind(
    InternetAddress.loopbackIPv4,
    4040,
  );
  print('Listening on localhost:${server.port}');
  await for (var request in server) {
    handleRequest(request);
  }

  await for (HttpRequest request in server) {
    request.response
      ..write('Hello, world! ')
      ..close();
  }
}

void handleRequest(HttpRequest request) {
  try {
    if (request.method == 'GET') {
      handleGet(request);
    } else {
      request.response
        ..statusCode = HttpStatus.methodNotAllowed
        ..write('Unsupported request: ${request.method}.')
        ..close();
    }
  } catch (e) {
    print('Exception in handleRequest: $e');
  }
  print('Request handled.');
}

void handleGet(HttpRequest request) async {
  var guess = request.uri.queryParameters['q'];
  request.response.headers.add("Access-Control-Allow-Origin", "*");
  request.response.headers
      .add("Access-Control-Allow-Methods", "POST,GET,DELETE,PUT,OPTIONS");
  request.response.statusCode = HttpStatus.OK;
  try {
    switch (request.uri.queryParameters['m']) {
      case "user":
        request.response
          ..write('${request.uri.queryParameters['o']}')
          ..close();
        break;
      case "liscli":
        List<Client> c = await db.instance.getclientDao().listeclient();
        List s = new List();
        for (final row in c) {
          s.add(row.json());
        }
        request.response
          ..write(jsonEncode(s))
          ..close();
        break;
      case "listeuser()":
        List<User> c = await db.instance.getUserDao().listeuser();
        List s = new List();
        for (final row in c) {
          s.add(row.json());
        }
        request.response
          ..write(jsonEncode(s))
          ..close();
        break;
      case "lisfourni":
        List c = await db.instance.getFournisseurDAO().listfourni();
        List s = new List();
        for (final row in c) {
          s.add(row.json());
        }
        request.response
          ..write(jsonEncode(s))
          ..close();
        break;
      case "lisprod":
        List<produit> c = await db.instance.getProduitDao().listeproduit();
        List s = new List();
        for (final row in c) {
          s.add(row.json());
        }
        request.response
          ..write(jsonEncode(s))
          ..close();
        break;
      case "chargeachatfacture()":
        List<achat> c = await db.instance.getFactureAchatDAO().chargeacha('${request.uri.queryParameters['o']}');
        List s = new List();
        for (final row in c) {
          s.add(row.json());
        }
        request.response
          ..write(jsonEncode(s))
          ..close();
        break;
      case "lisprodall":
        List<produit> c = await db.instance.getProduitDao().listeproduit();
        List s = new List();
        for (final row in c) {
          s.add(row.json());
        }
        request.response
          ..write(jsonEncode(s))
          ..close();
        break;
      case "chargecomby":
        List<Vente> c = await db.instance
            .getCommandeVenteDAO().chargecomby('${request.uri.queryParameters['o']}');
        List s = new List();
        for (final row in c) {
          s.add(row.json());
        }
        request.response
          ..write(jsonEncode(s))
          ..close();
        break;
      case "lisprod2":
        List<produit> c = await db.instance.getProduitDao().listeproduit2();
        List s = new List();
        for (final row in c) {
          s.add(row.json());
        }
        request.response
          ..write(jsonEncode(s))
          ..close();
        break;
      case "addp":
        request.response
          ..write(await db.instance
              .getProduitDao().addproduit('${request.uri.queryParameters['o']}'))
          ..close();
        break;
      case "addfourni":
        request.response
          ..write(await db.instance
              .getFournisseurDAO().addfournis('${request.uri.queryParameters['o']}'))
          ..close();
        break;
      case "addfactachat":
        request.response
          ..write(await db.instance.getFactureAchatDAO().createfactachat(
              '${request.uri.queryParameters['o']}',
              request.uri.queryParameters['t']))
          ..close();
        break;
      case "upchatfacture()":
        request.response
          ..write(await db.instance.getFactureAchatDAO().upfactachat(
              '${request.uri.queryParameters['o']}',
              request.uri.queryParameters['t'], request.uri.queryParameters['s']))
          ..close();
        break;
      case "addcategorie":
        request.response
          ..write(await db.instance.getCategorieDao().addcategorie(
              '${request.uri.queryParameters['o']}',
              request.uri.queryParameters['t']))
          ..close();
        break;
      case "upcategorie":
        request.response
          ..write(await db.instance.getCategorieDao().upcategorie(
              request.uri.queryParameters['o'],
              '${request.uri.queryParameters['nom']}',
              request.uri.queryParameters['type']))
          ..close();
        break;
      case "listecategorie":
        request.response
          ..write(jsonEncode(await db.instance.getCategorieDao().listecategorie()))
          ..close();
        break;
      case "listachatplant()":
        request.response
          ..write(jsonEncode(await db.instance.getAchaVentePlantsDAO().listachatplant(
              request.uri.queryParameters['o'],
              request.uri.queryParameters['i'])))
          ..close();
        break;
      case ".listventeplant()":
        request.response
          ..write(jsonEncode(await db.instance.getAchaVentePlantsDAO().listventeplant(
              request.uri.queryParameters['o'],
              request.uri.queryParameters['i'])))
          ..close();
        break;
      case "listecategorie2":
        request.response
          ..write(jsonEncode(await db.instance.getCategorieDao().listecategorie2()))
          ..close();
        break;
      case "listecategorietotdachat()":
        request.response
          ..write(jsonEncode(await db.instance.getAachatVenteCategorieDAO().listecategorietotdachat(
              request.uri.queryParameters['o'],
              request.uri.queryParameters['i'])))
          ..close();
        break;
      case ".listecategorietotdnovente()":
        request.response
          ..write(jsonEncode(await db.instance.getAachatVenteCategorieDAO().listecategorietotdnovente(
              request.uri.queryParameters['o'],
              request.uri.queryParameters['i'])))
          ..close();
        break;
      case "listecategorietotdvente()":
        request.response
          ..write(jsonEncode(await db.instance.getAachatVenteCategorieDAO().listecategorietotdvente(
              request.uri.queryParameters['o'],
              request.uri.queryParameters['i'])))
          ..close();
        break;
      case "upp":
        request.response
          ..write(await db.instance
              .getProduitDao().uppproduit('${request.uri.queryParameters['o']}'))
          ..close();
        break;
      case "acitvep":
        request.response
          ..write(await db.instance.getProduitDao().upstat(request.uri.queryParameters['o'], 1))
          ..close();
        break;
      case "descacitvep":
        request.response
          ..write(await db.instance.getProduitDao().upstat(request.uri.queryParameters['o'], 0))
          ..close();
        break;

      case "findprod":
        List<produit> c = await db.instance
            .getProduitDao().findproduit('${request.uri.queryParameters['o']}');
        List s = new List();
        for (final row in c) {
          s.add(row.json());
        }
        request.response
          ..write(jsonEncode(s))
          ..close();
        break;
      case "findprodcat":
        List<produit> c = await db.instance
            .getProduitDao().findproduitcat('${request.uri.queryParameters['o']}');
        List s = new List();
        for (final row in c) {
          s.add(row.json());
        }
        request.response
          ..write(jsonEncode(s))
          ..close();
        break;
      case "commande":
        print(request.uri.queryParameters['o']);
        int c = await db.instance.getFactureCommandeDAO().createcomfac(
            '${request.uri.queryParameters['o']}',
            request.uri.queryParameters['i']);

        request.response
          ..write(c)
          ..close();
        break;
      case "com":
        print(request.uri.queryParameters['o']);
        int c = await db.instance.getCommandeVenteDAO().createcom(
            '${request.uri.queryParameters['o']}',
            request.uri.queryParameters['i']);

        request.response
          ..write(c)
          ..close();
        break;
      case "upcommand":
        print(request.uri.queryParameters['o']);
        int c = await db.instance.getCommandeVenteDAO().upcommand(
            '${request.uri.queryParameters['o']}',
            request.uri.queryParameters['i']);

        request.response
          ..write(c)
          ..close();
        break;
      case "facture":
        print(request.uri.queryParameters['o']);
        List c = await db.instance.getFactureCommandeDAO().createfac(
            '${request.uri.queryParameters['o']}',
            request.uri.queryParameters['i']);

        request.response
          ..write(jsonEncode(c))
          ..close();
        break;
      case "factdate":
        request.response
          ..write(jsonEncode(await db.instance.getFactureCommandeDAO().factpayer(
              request.uri.queryParameters['o'],
              request.uri.queryParameters['i'])))
          ..close();
        break;
      case "factdatenon":
        request.response
          ..write(jsonEncode(await db.instance.getFactureCommandeDAO().factnon(
              request.uri.queryParameters['o'],
              request.uri.queryParameters['i'])))
          ..close();
        break;
      case "commnon":
        request.response
          ..write(jsonEncode(await db.instance.getCommandeVenteDAO().commnon(
              request.uri.queryParameters['o'],
              request.uri.queryParameters['i'])))
          ..close();
        break;
      case "Comoffacture":
        request.response
          ..write(jsonEncode(
              await db.instance.getFactureCommandeDAO().Comoffacture(request.uri.queryParameters['o'])))
          ..close();
        break;
      case "chargecom":
        request.response
          ..write(jsonEncode(
              await db.instance.getCommandeVenteDAO().chargecom(request.uri.queryParameters['o'])))
          ..close();
        break;
      case "chargefactureacha":
        request.response
          ..write(jsonEncode(await db.instance
              .getFactureAchatDAO().chargefactureacha(request.uri.queryParameters['o'])))
          ..close();
        break;
      case "factdate2":
        print(request.uri.queryParameters['c']);
        request.response
          ..write(jsonEncode(await db.instance.getFactureCommandeDAO().factpayer2(
              request.uri.queryParameters['o'],
              request.uri.queryParameters['i'],
              request.uri.queryParameters['c'])))
          ..close();
        break;
      case "factdatenon2":
        request.response
          ..write(jsonEncode(await db.instance.getFactureCommandeDAO().factnon2(
              request.uri.queryParameters['o'],
              request.uri.queryParameters['i'],
              request.uri.queryParameters['c'])))
          ..close();
        break;
      case "commnon2":
        request.response
          ..write(jsonEncode(await db.instance.getCommandeVenteDAO().commnon2(
              request.uri.queryParameters['o'],
              request.uri.queryParameters['i'],
              request.uri.queryParameters['c'])))
          ..close();
        break;
      case "createcli":
        print(request.uri.queryParameters['c']);
        request.response
          ..write(
              await db.instance.getclientDao().Createclient(request.uri.queryParameters['o']))
          ..close();
        break;
      case "adduser()":
        print(request.uri.queryParameters['c']);
        request.response
          ..write(await db.instance.getUserDao().adduser(request.uri.queryParameters['o']))
          ..close();
        break;
      case "supprcommande":
        request.response
          ..write(
              await db.instance.getCommandeDAO().supprcommande(request.uri.queryParameters['o']))
          ..close();
        break;
      case "suppruser":
        request.response
          ..write(await db.instance.getUserDao().suppruser(request.uri.queryParameters['o']))
          ..close();
        break;
      case "unit34":
        request.response
          ..write(
              await db.instance.getUserDao().Authenticate(request.uri.queryParameters['o']))
          ..close();
        break;
      case "upfactpayer":
        request.response
          ..write(
              await db.instance.getFactureDAO().upfactpayer(request.uri.queryParameters['o']))
          ..close();
        break;
      case "upfourni":
        request.response
          ..write(await db.instance.getFournisseurDAO().upfourni(request.uri.queryParameters['o']))
          ..close();
        break;
      case "facturerdescommande":
        print(request.uri.queryParameters['c']);
        request.response
          ..write(jsonEncode(await db.instance.getFactureCommandeDAO().createfacdescomande(
              request.uri.queryParameters['o'],
              request.uri.queryParameters['ci'])))
          ..close();
        break;
      case "listefactcli":
        request.response
          ..write(jsonEncode(await db.instance
              .getFactureDAO().listefactureclient(request.uri.queryParameters['o'])))
          ..close();
        break;
      case "listecommnoncli":
        request.response
          ..write(jsonEncode(await db.instance
              .getCommandeVenteDAO().listecommclient(request.uri.queryParameters['o'])))
          ..close();
        break;
      case "listecommnoncli2":
        request.response
          ..write(jsonEncode(await db.instance
              .getCommandeVenteDAO().listecommclient2(request.uri.queryParameters['o'])))
          ..close();
        break;
      case "listefactureacha":
        request.response
          ..write(jsonEncode(await db.instance
              .getFactureAchatDAO().listefactureacha(request.uri.queryParameters['o'])))
          ..close();
        break;
      default:
        request.response
          ..write('error')
          ..close();
    }
  } catch (e) {
    request.response
      ..write(
          '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! '
          '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
          'error message :$e'
          '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
          '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
      ..close();
    print(
        "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    print(
        "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    print("error = $e");
    print(
        "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    print(
        "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
  }
}
