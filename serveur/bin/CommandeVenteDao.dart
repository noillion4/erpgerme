import 'dart:io';
import 'dart:convert';
import 'dart:async';
import 'package:sqljocky5/auth/auth_handler.dart';
import 'package:sqljocky5/auth/character_set.dart';
import 'package:sqljocky5/auth/handshake_handler.dart';
import 'package:sqljocky5/auth/ssl_handler.dart';
import 'package:sqljocky5/common/logging.dart';
import 'package:sqljocky5/comm/buffered_socket.dart';
import 'package:sqljocky5/comm/comm.dart';
import 'package:sqljocky5/comm/common.dart';
import 'package:sqljocky5/comm/receiver.dart';
import 'package:sqljocky5/comm/sender.dart';
import 'package:sqljocky5/connection/connection.dart';
import 'package:sqljocky5/connection/impl.dart';
import 'package:sqljocky5/connection/settings.dart';
import 'package:sqljocky5/constants.dart';
import 'package:sqljocky5/exceptions/client_error.dart';
import 'package:sqljocky5/exceptions/exceptions.dart';
import 'package:sqljocky5/exceptions/mysql_exception.dart';
import 'package:sqljocky5/exceptions/protocol_error.dart';
import 'package:sqljocky5/handlers/debug_handler.dart';
import 'package:sqljocky5/handlers/handler.dart';
import 'package:sqljocky5/handlers/ok_packet.dart';
import 'package:sqljocky5/handlers/parameter_packet.dart';
import 'package:sqljocky5/handlers/ping_handler.dart';
import 'package:sqljocky5/handlers/quit_handler.dart';
import 'package:sqljocky5/handlers/use_db_handler.dart';
import 'package:sqljocky5/prepared_statements/close_statement_handler.dart';
import 'package:sqljocky5/prepared_statements/execute_query_handler.dart';
import 'package:sqljocky5/prepared_statements/prepared_query.dart';
import 'package:sqljocky5/prepared_statements/prepare_handler.dart';
import 'package:sqljocky5/prepared_statements/prepare_ok_packet.dart';
import 'package:sqljocky5/query/query_stream_handler.dart';
import 'package:sqljocky5/query/result_set_header_packet.dart';
import 'package:sqljocky5/results/binary_data_packet.dart';
import 'package:sqljocky5/results/blob.dart';
import 'package:sqljocky5/results/field.dart';
import 'package:sqljocky5/results/results.dart';
import 'package:sqljocky5/results/row.dart';
import 'package:sqljocky5/results/standard_data_packet.dart';
import 'package:sqljocky5/sqljocky.dart';
import 'package:sqljocky5/utils/buffer.dart';
import '../../website/web/classemetier/Client.dart';
import '../../website/web/classemetier/vente.dart';
import '../../website/web/classemetier/achat.dart';
import '../../website/web/classemetier/users.dart';
import '../../website/web/classemetier/produit.dart';
import '../../website/web/classemetier/fournisseur.dart';

import 'package:postgres/postgres.dart';

class CommandeVenteDAO {

  var _Sqlcont = ConnectionSettings(
    user: "caisse",
    password: "8767ce246C",
    host: "localhost",
    port: 3306,
    db: "dart",
  );
  /**
   * met ajour une commande
   * @parama json : le contenue de la commande
   * @param id = id de la commande
   * @return le resulta
   *
   */

  Future upcommand(json, id) async {
    print(json);
    List<Vente> lv = new List();
    var ljson = jsonDecode(json);
    for (var value in ljson) {
      lv.add(new Vente.J(value));
    }
    MySqlConnection conn;
    // create a connection
    var s =_Sqlcont;
    conn = await MySqlConnection.connect(s);
    Results results;

    results = (await conn.execute("delete from vente where Id_commade =$id; update commade set blivraison = 1 where id = $id;")) as Results;

    lv.forEach((f) async => results = (await conn.prepared(
        "INSERT INTO `vente` VALUES (0,${f.Idp},'${f.Nom}',${f.Prix},'${f.Forma}',$id,${f.Qt},${f.Indicat},${f.Caisse})",
        [2])) as Results);

    return 1;
  }

  /**
   * @param l'id de la commande
   * @return les produit cauposent une commande
   */
  Future<List> chargecomby(id) async {
    List<produit> c = new List();
    MySqlConnection conn;
    List<Vente> l = new List();
    // create a connection
    var s =_Sqlcont;
    print(id);
    conn = await MySqlConnection.connect(s);

    Results results = (await conn.prepared(
        "SELECT v.id ,p.id , p.nom,  v.prix , p.forma ,c.id , v.qt,v.indicat , v.caisse from commade c ,vente v ,produit p where c.id = $id and v.Id_commade =c.id and p.id = v.id_produit ",
        [1])) as Results;
    print(results.length);

    results.forEach((f) {
      print(f[0]);
      print("ttt");

      l.add(new Vente(f[0], f[1], f[2], f[3], f[4], f[5], f[6], f[7],f[8]));
    });
    print(l);
    return l;
  }
  /**
   * @param l'id de la commande
   * @return les produit cauposent une commande avec le total
   */
  Future<List> chargecom(id) async {
    List<produit> c = new List();
    MySqlConnection conn;
    List<Map> l = new List();
    // create a connection
    var s =_Sqlcont;
    print(id);
    conn = await MySqlConnection.connect(s);
    Results results = (await conn.prepared(
        "SELECT p.nom ,v.qt , v.qt*v.prix,  v.qt*v.prix+ v.qt*v.prix/100*p.tva,p.forma ,c.id , c.date ,p.indica  , p.nb ,v.caisse from commade c ,vente v ,produit p where c.id = $id and v.Id_commade =c.id and p.id = v.id_produit; ",
        [1])) as Results;
    print(results.length);

    results.forEach((f) {
      print(f[0]);
      print("ttt");
      Map fact = {
        'nom': '${f[0]}',
        'quatitier': f[1],
        'total': f[2],
        'ttva': f[3],
        'forma': f[4],
        'id': f[5],
        'date': '${f[6]}',
        'indication' :'${f[7]}',
        'nb' : f[8],
        'caisse' : f[9]
      };
      l.add(fact);
    });
    print(l);
    return l;
  }
  /**
   * @param l'id du client
   *@return la liste des commande  non facturer pour un client au forma json
   */

  Future<List> listecommclient2(id) async {
    List<Map> c = new List();
    MySqlConnection conn;
    // create a connection
    var s =_Sqlcont;
    conn = await MySqlConnection.connect(s);
    Results results = (await conn.prepared(
        "SELECT c.id ,c.date ,c.id_client ,c.id_facture , sum(v.prix*v.qt),c.blivraison FROM commade c  ,vente v where $id = c.id_client and c.id=v.Id_commade  and (id_facture is null or id_facture=0) and v.indicat != 1 group by c.id",
        [1])) as Results;
    results.forEach((Row row) {
      // Access columns by index
      Map fact = {
        'id': row[0],
        'date': '${row[1]}',
        'idclient': row[2],
        'idfacture': row[3],
        'Total': row[4],
        'blivraison' :row[5]
      };
      c.add(fact);
      // new Client(id, prenom, nom, gsm, tva, email, pourcent)
    });
    return c;
  }
  /**
   * @param l'id du client
   *@return la liste des commande pour un client au forma json
   */

  Future<List> listecommclient(id) async {
    List<Map> c = new List();
    MySqlConnection conn;
    // create a connection
    var s =_Sqlcont;
    conn = await MySqlConnection.connect(s);
    Results results = (await conn.prepared(
        "SELECT c.id ,c.date ,c.id_client ,c.id_facture , sum(v.prix*v.qt) ,c.blivraison FROM commade c ,vente v where $id = c.id_client and c.id=v.Id_commade and v.indicat != 1 group by c.id",
        [1])) as Results;
    results.forEach((Row row) {
      // Access columns by index
      Map fact = {
        'id': row[0],
        'date': '${row[1]}',
        'idclient': row[2],
        'idfacture': row[3],
        'Total': row[4],
        'blivraison' :row[5]
      };
      c.add(fact);
      // new Client(id, prenom, nom, gsm, tva, email, pourcent)
    });
    return c;
  }
  /**
   * @param dtd : la date de debut
   * @param dtfin : la date de fin
   *@return la liste de commande entre deux date
   */
  Future<List> commnon(dtfin, dtd) async {
    List<produit> c = new List();
    MySqlConnection conn;
    List<Map> l = new List();
    // create a connection
    var s =_Sqlcont;
    conn = await MySqlConnection.connect(s);
    Results results = (await conn.prepared(
        "SELECT p.nom ,sum(v.qt) , sum(v.qt*v.prix),  sum(v.qt*v.prix+ v.qt*v.prix/100*p.tva),p.forma from commade c ,vente v ,produit p where v.Id_commade =c.id and p.id = v.id_produit and (id_facture is null or id_facture=0) and c.date between '$dtd' and '$dtfin' group by p.nom  ,p.forma;",
        [1])) as Results;
    print(results.length);
    print(dtd);
    results.forEach((f) {
      print(f[0]);
      print("ttt");
      Map fact = {
        'nom': '${f[0]}',
        'quatitier': f[1],
        'total': f[2],
        'ttva': f[3],
        'forma': f[4]
      };
      l.add(fact);
    });
    print(l);
    return l;
  }
  /**
   * @param C : l'id du  client
   * @param dtd : la date de debut
   * @param dtfin : la date de fin
   *@return la liste de commande entre deux date pour un client
   */
  Future<List> commnon2(dtfin, dtd, C) async {
    List<produit> c = new List();
    MySqlConnection conn;
    List<Map> l = new List();
    // create a connection
    var s =_Sqlcont;
    conn = await MySqlConnection.connect(s);
    Results results = (await conn.prepared(
        "SELECT p.nom ,sum(v.qt) , sum(v.qt*v.prix),  sum(v.qt*v.prix+ v.qt*v.prix/100*p.tva),p.forma from commade c ,vente v ,produit p where c.id_client=$C and v.Id_commade =c.id and p.id = v.id_produit and (id_facture is null or id_facture=0) and c.date between '$dtd' and '$dtfin' group by p.nom  ,p.forma;",
        [1])) as Results;
    print(results.length);
    print(dtd);
    results.forEach((f) {
      print(f[0]);
      print("ttt");
      Map fact = {
        'nom': '${f[0]}',
        'quatitier': f[1],
        'total': f[2],
        'ttva': f[3],
        'forma': f[4]
      };
      l.add(fact);
    });
    print(l);
    return l;
  }
  /**
   * cree une commande avec une liste de vente
   * @param json : liste de ventre forma json
   * @param client : idclient
   * @return id de la commande
   */
  Future<int> createcom(json, client) async {
    var json2 = json as dynamic;
    List<Vente> lis = new List<Vente>();
    List j = jsonDecode(json2);
    j.forEach((f) => lis.add(new Vente.J(f)));
    MySqlConnection conn;
    // create a connection
    var s =_Sqlcont;
    conn = await MySqlConnection.connect(s);
    Results results;
    int id;
    var id2;
    DateTime d = DateTime.now();
    results = (await conn
        .execute("insert INTO commade VALUES (null,'$d' ,$client,0,0,0);")) as Results;
    results = (await conn
        .prepared("select id from commade where id=last_insert_id();", [2])) as Results;
    id2 = results.elementAt(0);

    id = id2[0];
    print(id);
    lis.forEach((f) async => results = (await conn.prepared(
        "INSERT INTO `vente` VALUES (0,${f.Idp},'${f.Nom}',${f.Prix},'${f.Forma}',$id,${f.Qt},${f.Indicat},${f.Caisse})",
        [2])) as Results);

    // Access columns by index
//SELECT p.nom ,sum(v.qt) , sum(v.qt*v.prix)from facture f ,commade c ,vente v ,produit p where f.id=c.id_facture and v.Id_commade =c.id and p.id = v.id_produit group by p.nom;
    // new Client(id, prenom, nom, gsm, tva, email, pourcent)

    return id;
  }


}