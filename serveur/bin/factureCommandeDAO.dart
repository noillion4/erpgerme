import 'dart:io';
import 'dart:convert';
import 'dart:async';
import 'package:sqljocky5/auth/auth_handler.dart';
import 'package:sqljocky5/auth/character_set.dart';
import 'package:sqljocky5/auth/handshake_handler.dart';
import 'package:sqljocky5/auth/ssl_handler.dart';
import 'package:sqljocky5/common/logging.dart';
import 'package:sqljocky5/comm/buffered_socket.dart';
import 'package:sqljocky5/comm/comm.dart';
import 'package:sqljocky5/comm/common.dart';
import 'package:sqljocky5/comm/receiver.dart';
import 'package:sqljocky5/comm/sender.dart';
import 'package:sqljocky5/connection/connection.dart';
import 'package:sqljocky5/connection/impl.dart';
import 'package:sqljocky5/connection/settings.dart';
import 'package:sqljocky5/constants.dart';
import 'package:sqljocky5/exceptions/client_error.dart';
import 'package:sqljocky5/exceptions/exceptions.dart';
import 'package:sqljocky5/exceptions/mysql_exception.dart';
import 'package:sqljocky5/exceptions/protocol_error.dart';
import 'package:sqljocky5/handlers/debug_handler.dart';
import 'package:sqljocky5/handlers/handler.dart';
import 'package:sqljocky5/handlers/ok_packet.dart';
import 'package:sqljocky5/handlers/parameter_packet.dart';
import 'package:sqljocky5/handlers/ping_handler.dart';
import 'package:sqljocky5/handlers/quit_handler.dart';
import 'package:sqljocky5/handlers/use_db_handler.dart';
import 'package:sqljocky5/prepared_statements/close_statement_handler.dart';
import 'package:sqljocky5/prepared_statements/execute_query_handler.dart';
import 'package:sqljocky5/prepared_statements/prepared_query.dart';
import 'package:sqljocky5/prepared_statements/prepare_handler.dart';
import 'package:sqljocky5/prepared_statements/prepare_ok_packet.dart';
import 'package:sqljocky5/query/query_stream_handler.dart';
import 'package:sqljocky5/query/result_set_header_packet.dart';
import 'package:sqljocky5/results/binary_data_packet.dart';
import 'package:sqljocky5/results/blob.dart';
import 'package:sqljocky5/results/field.dart';
import 'package:sqljocky5/results/results.dart';
import 'package:sqljocky5/results/row.dart';
import 'package:sqljocky5/results/standard_data_packet.dart';
import 'package:sqljocky5/sqljocky.dart';
import 'package:sqljocky5/utils/buffer.dart';
import '../../website/web/classemetier/Client.dart';
import '../../website/web/classemetier/vente.dart';
import '../../website/web/classemetier/achat.dart';
import '../../website/web/classemetier/users.dart';
import '../../website/web/classemetier/produit.dart';
import '../../website/web/classemetier/fournisseur.dart';

import 'package:postgres/postgres.dart';

class FactureCommandeDAO {

  var _Sqlcont = ConnectionSettings(
    user: "caisse",
    password: "8767ce246C",
    host: "localhost",
    port: 3306,
    db: "dart",
  );
/**
 * creet un facture appartire d'une liste de vente
 * @param json liste de vente au forma json
 * @param client l'id du client
 * @return l'id de la commande
 */
  Future<int> createcomfac(json, client) async {
    var json2 = json as dynamic;
    List<Vente> lis = new List<Vente>();
    List j = jsonDecode(json2);
    j.forEach((f) => lis.add(new Vente.J(f)));
    MySqlConnection conn;
    // create a connection
    var s =_Sqlcont;
    conn = await MySqlConnection.connect(s);
    Results results;
    int id;
    var id2;
    DateTime d = DateTime.now();
    results = (await conn
        .execute("insert INTO facture VALUES (null,'$d' ,$client,'o');"));
    results = (await conn
        .prepared("select max(id) from facture", [2]));
    id2 = results.elementAt(0);

    id = id2[0];
    print(id);
    results = (await conn
        .execute("insert INTO commade VALUES (null,'$d' ,$client,$id,0,'0');"));
    results = (await conn
        .prepared("select max(id) from commade;", [2]));
    id2 = results.elementAt(0);

    id = id2[0];
    print(id);
    lis.forEach((f) async => results = (await conn.prepared(
        "INSERT INTO vente VALUES (0,${f.Idp},'${f.Nom}',${f.Prix},'${f.Forma}',$id,${f.Qt},${f.Indicat},${f.Caisse})",
        [2])));

    // Access columns by index

    // new Client(id, prenom, nom, gsm, tva, email, pourcent)

    return id;
  }

  /**
   * creet un facture appartire d'une liste de vente
   * @param json liste de vente au forma json
   * @param client l'id du client
   * @return l'id de la commande et l'id de la facture
   */
  Future<List> createfac(json, client) async {
    var json2 = json as dynamic;
    List<Vente> lis = new List<Vente>();
    List j = jsonDecode(json2);
    List ini = new List();
    j.forEach((f) => lis.add(new Vente.J(f)));
    MySqlConnection conn;
    // create a connection
    var s =_Sqlcont;
    conn = await MySqlConnection.connect(s);
    Results results;
    int id;
    var id2;
    DateTime d = DateTime.now();
    results = (await conn
        .execute("insert INTO facture VALUES (null,'$d' ,$client,'n');"));
    results = (await conn
        .prepared("select max(id) from facture", [2]));
    id2 = results.elementAt(0);

    id = id2[0];
    print(id);
    ini.add(id);
    results = (await conn
        .execute("insert INTO commade VALUES (null,'$d' ,$client,$id,0,'0');"));
    results = (await conn
        .prepared("select max(id) from commade", [2]));
    id2 = results.elementAt(0);

    id = id2[0];
    print(id);
    lis.forEach((f) async => results = (await conn.prepared(
        "INSERT INTO `vente` VALUES (0,${f.Idp},'${f.Nom}',${f.Prix},'${f.Forma}',$id,${f.Qt},${f.Indicat},${f.Caisse})",
        [2])));

    // Access columns by index

    // new Client(id, prenom, nom, gsm, tva, email, pourcent)

    ini.add(id);
    return ini;
  }
/**
 * creer une facutre pour des commande
 * @param une lise de facture au forma json
 * @param client : l'id du client
 * @return la facture
 */
  Future<Map> createfacdescomande(json, client) async {
    var json2 = jsonDecode(json);

    MySqlConnection conn;
    // create a connection
    var s =_Sqlcont;
    conn = await MySqlConnection.connect(s);
    Results results;
    int id;
    var id2;
    DateTime d = DateTime.now();
    results = (await conn
        .execute("insert INTO facture VALUES (null,'$d' ,$client,'n');"));
    results = (await conn
        .prepared("select max(id) from facture;", [2]));
    id2 = results.elementAt(0);

    id = id2[0];
    print(id);
    for (var value in json2) {
      print(value['id']);
      await conn.execute(
          "update commade set id_facture=$id where id=${value['id']};");
    }
    results = (await conn.prepared("select * from facture where id=$id;", [2]));
    id2 = results.elementAt(0);

    Map fact = {
      'id': id2[0],
      'date': '${id2[1]}',
      'idclient': id2[2],
      'payer': '${id2[3]}'
    };
    // Access columns by index

    // new Client(id, prenom, nom, gsm, tva, email, pourcent)

    return fact;
  }

  /**
   * @param dtfin la date de fin
   * @param dtd la date de debut
   * @return la liste des facture payer entre deux date
   */
  Future<List> factpayer(dtfin, dtd) async {
    List<produit> c = new List();
    MySqlConnection conn;
    List<Map> l = new List();
    // create a connection
    var s =_Sqlcont;
    conn = await MySqlConnection.connect(s);
    Results results = (await conn.prepared(
        "SELECT p.nom ,sum(v.qt) , sum(v.qt*v.prix) , sum(v.qt*v.prix+ v.qt*v.prix/100*p.tva),p.forma from facture f ,commade c ,vente v ,produit p where f.id=c.id_facture and v.Id_commade =c.id and p.id = v.id_produit  and f.payer ='o'  and f.date between '$dtd' and '$dtfin' group by p.nom, p.forma",
        [1]));
    print(results.length);
    print(dtd);
    results.forEach((f) {
      print(f[0]);
      print("ttt");
      Map fact = {
        'nom': '${f[0]}',
        'quatitier': f[1],
        'total': f[2],
        'ttva': f[3],
        'forma': f[4]
      };
      l.add(fact);
    });
    print(l);
    return l;
  }


  /**
   * @param dtfin la date de fin
   * @param dtd la date de debut
   * @parma C : id du client
   * @return la liste des facture payer entre deux date pour un client
   */



  Future<List> factpayer2(dtfin, dtd, C) async {
    List<produit> c = new List();
    MySqlConnection conn;
    List<Map> l = new List();
    print(C);
    // create a connection
    var s =_Sqlcont;
    conn = await MySqlConnection.connect(s);
    Results results = (await conn.prepared(
        "SELECT p.nom ,sum(v.qt) , sum(v.qt*v.prix) , sum(v.qt*v.prix+ v.qt*v.prix/100*p.tva),p.forma from facture f ,commade c ,vente v ,produit p where f.client=$C and  f.id=c.id_facture and v.Id_commade =c.id and p.id = v.id_produit  and f.payer ='o'  and f.date between '$dtd' and '$dtfin' group by p.nom, p.forma",
        [1]));
    print(results.length);
    print(dtd);
    results.forEach((f) {
      print(f[0]);
      print("ttt");
      Map fact = {
        'nom': '${f[0]}',
        'quatitier': f[1],
        'total': f[2],
        'ttva': f[3],
        'forma': f[4]
      };
      l.add(fact);
    });
    print(l);
    return l;
  }

  /**
   * @param dtfin la date de fin
   * @param dtd la date de debut
   * @parma C : id du client
   * @return la liste des facture  non payer entre deux date pour un client
   */
  Future<List> factnon2(dtfin, dtd, C) async {
    List<produit> c = new List();
    MySqlConnection conn;
    List<Map> l = new List();
    // create a connection
    var s =_Sqlcont;
    conn = await MySqlConnection.connect(s);
    Results results = (await conn.prepared(
        "SELECT p.nom ,sum(v.qt) , sum(v.qt*v.prix) , sum(v.qt*v.prix+ v.qt*v.prix/100*p.tva),p.forma from facture f ,commade c ,vente v ,produit p where f.client=$C and  f.id=c.id_facture and v.Id_commade =c.id and p.id = v.id_produit  and f.payer ='n'  and f.date between '$dtd' and '$dtfin' group by p.nom  ,p.forma",
        [1]));
    print(results.length);
    print(dtd);
    results.forEach((f) {
      print(f[0]);
      print("ttt");
      Map fact = {
        'nom': '${f[0]}',
        'quatitier': f[1],
        'total': f[2],
        'ttva': f[3],
        'forma': f[4]
      };
      l.add(fact);
    });
    print(l);
    return l;
  }
  /**
   * @param dtfin la date de fin
   * @param dtd la date de debut
   * @return la liste des facture  non payer entre deux date
   */
  Future<List> factnon(dtfin, dtd) async {
    List<produit> c = new List();
    MySqlConnection conn;
    List<Map> l = new List();
    // create a connection
    var s =_Sqlcont;
    conn = await MySqlConnection.connect(s);
    Results results = (await conn.prepared(
        "SELECT p.nom ,sum(v.qt) , sum(v.qt*v.prix) , sum(v.qt*v.prix+ v.qt*v.prix/100*p.tva),p.forma from facture f ,commade c ,vente v ,produit p where f.id=c.id_facture and v.Id_commade =c.id and p.id = v.id_produit  and f.payer ='n'  and f.date between '$dtd' and '$dtfin' group by p.nom  ,p.forma",
        [1]));
    print(results.length);
    print(dtd);
    results.forEach((f) {
      print(f[0]);
      print("ttt");
      Map fact = {
        'nom': '${f[0]}',
        'quatitier': f[1],
        'total': f[2],
        'ttva': f[3],
        'forma': f[4]
      };
      l.add(fact);
    });
    print(l);
    return l;
  }

  /**
   * @param id de la facture
   * @return un liste au forma json de commande avec leurs totals
   */
  Future<List> Comoffacture(id) async {
    List<produit> c = new List();
    MySqlConnection conn;
    List<Map> l = new List();
    // create a connection
    var s =_Sqlcont;
    conn = await MySqlConnection.connect(s);
    Results results = (await conn.prepared(
        "SELECT c.id,c.date , sum(v.qt*v.prix),  sum(v.qt*v.prix+ v.qt*v.prix/100*p.tva) , c.id_facture from facture f ,commade c ,vente v ,produit p where v.Id_commade =c.id and p.id = v.id_produit and f.id=$id and c.id_facture =f.id and v.indicat != 1 group by c.id ;",
        [1]));
    print(results.length);

    results.forEach((f) {
      print(f[0]);
      print("ttt");
      Map fact = {
        'id': f[0],
        'date': '${f[1]}',
        'total': f[2],
        'totaltva': f[3],
        'idfacture': f[4]
      };
      l.add(fact);
    });
    print(l);
    return l;
  }
}
