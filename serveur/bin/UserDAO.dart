import 'dart:io';
import 'dart:convert';
import 'dart:async';
import 'package:sqljocky5/auth/auth_handler.dart';
import 'package:sqljocky5/auth/character_set.dart';
import 'package:sqljocky5/auth/handshake_handler.dart';
import 'package:sqljocky5/auth/ssl_handler.dart';
import 'package:sqljocky5/common/logging.dart';
import 'package:sqljocky5/comm/buffered_socket.dart';
import 'package:sqljocky5/comm/comm.dart';
import 'package:sqljocky5/comm/common.dart';
import 'package:sqljocky5/comm/receiver.dart';
import 'package:sqljocky5/comm/sender.dart';
import 'package:sqljocky5/connection/connection.dart';
import 'package:sqljocky5/connection/impl.dart';
import 'package:sqljocky5/connection/settings.dart';
import 'package:sqljocky5/constants.dart';
import 'package:sqljocky5/exceptions/client_error.dart';
import 'package:sqljocky5/exceptions/exceptions.dart';
import 'package:sqljocky5/exceptions/mysql_exception.dart';
import 'package:sqljocky5/exceptions/protocol_error.dart';
import 'package:sqljocky5/handlers/debug_handler.dart';
import 'package:sqljocky5/handlers/handler.dart';
import 'package:sqljocky5/handlers/ok_packet.dart';
import 'package:sqljocky5/handlers/parameter_packet.dart';
import 'package:sqljocky5/handlers/ping_handler.dart';
import 'package:sqljocky5/handlers/quit_handler.dart';
import 'package:sqljocky5/handlers/use_db_handler.dart';
import 'package:sqljocky5/prepared_statements/close_statement_handler.dart';
import 'package:sqljocky5/prepared_statements/execute_query_handler.dart';
import 'package:sqljocky5/prepared_statements/prepared_query.dart';
import 'package:sqljocky5/prepared_statements/prepare_handler.dart';
import 'package:sqljocky5/prepared_statements/prepare_ok_packet.dart';
import 'package:sqljocky5/query/query_stream_handler.dart';
import 'package:sqljocky5/query/result_set_header_packet.dart';
import 'package:sqljocky5/results/binary_data_packet.dart';
import 'package:sqljocky5/results/blob.dart';
import 'package:sqljocky5/results/field.dart';
import 'package:sqljocky5/results/results.dart';
import 'package:sqljocky5/results/row.dart';
import 'package:sqljocky5/results/standard_data_packet.dart';
import 'package:sqljocky5/sqljocky.dart';
import 'package:sqljocky5/utils/buffer.dart';
import '../../website/web/classemetier/Client.dart';
import '../../website/web/classemetier/vente.dart';
import '../../website/web/classemetier/achat.dart';
import '../../website/web/classemetier/users.dart';
import '../../website/web/classemetier/produit.dart';
import '../../website/web/classemetier/fournisseur.dart';

import 'package:postgres/postgres.dart';

class UserDAO {

  var _Sqlcont = ConnectionSettings(
    user: "caisse",
    password: "8767ce246C",
    host: "localhost",
    port: 3306,
    db: "dart",
  );

  /**
   * ajoute un utilisateur dans la
   * @param l'utilisateur
   * @return le resulta
   */

  Future adduser(j) async {
    print(j);
    User p = new User.J(j);
    List<User> lu = await listeuser();
    for (int i = 0; i < lu.length; i++) {
      if(lu[i].Nom==p.Nom)
        throw new Exception("le nom est deja utiliser");
    }
    MySqlConnection conn;
    // create a connection
    var s =_Sqlcont;
    conn = await MySqlConnection.connect(s);
    Results results;

    //NSERT INTO `dart`.`users` (`Nom`, `Password`, `date`, `home`, `shop`, `Client`, `fournisseur`, `users`, `stat`, `produit`, `categorie`) VALUES ('0', '8', '0', '0', '0', '0', '0', '0', '0', '0', '0');
    results = (await conn.execute(
        "insert INTO users VALUES (null,'${p.Nom}' ,'${p.Password}','${p.Date}',${p.Home},${p.Shop},${p.Client},${p.Founisseur},${p.user} ,${p.Stat},${p.Produit},${p.Categorie});")) as Results;

    return results;
  }
  /**
   *  @return la liste des utilisateur
   */
  Future<List<User>> listeuser() async {
    List<User> c = new List();
    MySqlConnection conn;
    // create a connection
    var s =_Sqlcont;
    conn = await MySqlConnection.connect(s);
    Results results =
    (await conn.prepared("SELECT * FROM `users` where id!=6", [1])) as Results;
    results.forEach((Row row) {
      // Access columns by index

      // //NSERT INTO `dart`.`users` (`Nom`, `Password`, `date`, `home`, `shop`, `Client`, `fournisseur`, `users`, `stat`, `produit`, `categorie`) VALUES ('0', '8', '0', '0', '0', '0', '0', '0', '0', '0', '0');
      //new User(id, password, nom, date, shop, home, user, stat, founisseur, client, produit, categorie)
      print('Name: ${row.byName('id')}, email: ${row[1]}');
      // new Client(id, prenom, nom, gsm, tva, email, pourcent)

      User l = new User.v();
      l.Id = row.byName('id');
      l.Nom = row[1];
      l.Password = row[2];
      l.Date = row[3];
      if (row.byName('shop') == 1)
        l.Shop = true;
      else
        l.Shop = false;
      if (row.byName('home') == 1)
        l.Home = true;
      else
        l.Home = false;
      if (row.byName('Client') == 1)
        l.Client = true;
      else
        l.Client = false;
      if (row.byName('categorie') == 1)
        l.Categorie = true;
      else
        l.Categorie = false;
      if (row.byName('users') == 1)
        l.user = true;
      else
        l.user = false;
      if (row.byName('stat') == 1)
        l.Stat = true;
      else
        l.Stat = false;
      if (row.byName('fournisseur') == 1)
        l.Founisseur = true;
      else
        l.Founisseur = false;
      if (row.byName('produit') == 1)
        l.Produit = true;
      else
        l.Produit = false;

      // Access columns by name
      c.add(l);
    });
    return c;
  }
  /**
   *supprime un utilisateur
   *@param id du l'utilisateur
   *@return le resulta
   */
  Future suppruser(id) async {
    MySqlConnection conn;
    // create a connection
    var s =_Sqlcont;
    conn = await MySqlConnection.connect(s);
    Results results;

    DateTime d = DateTime.now();
    results = (await conn.execute("delete from users where id =$id")) as Results;
    return results;
  }
  /**
   * verifie si l'utilisteur existe
   * @param l'utisateur au forma json
   * @return l'utisateur au forma json si ok
   */
  Future<String> Authenticate(json2) async {
    List<User> c = new List();
    MySqlConnection conn;
    User u = new User.J(json2);
    // create a connection
    var s =_Sqlcont;
    conn = await MySqlConnection.connect(s);
    Results results = (await conn.prepared(
        "SELECT * FROM `users` where Nom = '${u.Nom}' and Password='${u.Password}'",
        [1])) as Results;
    var row = results.elementAt(0);
    // Access columns by index

    // //NSERT INTO `dart`.`users` (`Nom`, `Password`, `date`, `home`, `shop`, `Client`, `fournisseur`, `users`, `stat`, `produit`, `categorie`) VALUES ('0', '8', '0', '0', '0', '0', '0', '0', '0', '0', '0');
    //new User(id, password, nom, date, shop, home, user, stat, founisseur, client, produit, categorie)
    print('Name: ${row.byName('id')}, email: ${row[1]}');
    // new Client(id, prenom, nom, gsm, tva, email, pourcent)

    User l = new User.v();
    l.Id = row.byName('id');
    l.Nom = row[1];
    l.Password = row[2];
    l.Date = row[3];
    if (row.byName('shop') == 1)
      l.Shop = true;
    else
      l.Shop = false;
    if (row.byName('home') == 1)
      l.Home = true;
    else
      l.Home = false;
    if (row.byName('Client') == 1)
      l.Client = true;
    else
      l.Client = false;
    if (row.byName('categorie') == 1)
      l.Categorie = true;
    else
      l.Categorie = false;
    if (row.byName('users') == 1)
      l.user = true;
    else
      l.user = false;
    if (row.byName('stat') == 1)
      l.Stat = true;
    else
      l.Stat = false;
    if (row.byName('fournisseur') == 1)
      l.Founisseur = true;
    else
      l.Founisseur = false;
    if (row.byName('produit') == 1)
      l.Produit = true;
    else
      l.Produit = false;

    // Access columns by nam

    return l.json();
  }

}