import 'dart:io';
import 'dart:convert';
import 'dart:async';
import 'package:sqljocky5/auth/auth_handler.dart';
import 'package:sqljocky5/auth/character_set.dart';
import 'package:sqljocky5/auth/handshake_handler.dart';
import 'package:sqljocky5/auth/ssl_handler.dart';
import 'package:sqljocky5/common/logging.dart';
import 'package:sqljocky5/comm/buffered_socket.dart';
import 'package:sqljocky5/comm/comm.dart';
import 'package:sqljocky5/comm/common.dart';
import 'package:sqljocky5/comm/receiver.dart';
import 'package:sqljocky5/comm/sender.dart';
import 'package:sqljocky5/connection/connection.dart';
import 'package:sqljocky5/connection/impl.dart';
import 'package:sqljocky5/connection/settings.dart';
import 'package:sqljocky5/constants.dart';
import 'package:sqljocky5/exceptions/client_error.dart';
import 'package:sqljocky5/exceptions/exceptions.dart';
import 'package:sqljocky5/exceptions/mysql_exception.dart';
import 'package:sqljocky5/exceptions/protocol_error.dart';
import 'package:sqljocky5/handlers/debug_handler.dart';
import 'package:sqljocky5/handlers/handler.dart';
import 'package:sqljocky5/handlers/ok_packet.dart';
import 'package:sqljocky5/handlers/parameter_packet.dart';
import 'package:sqljocky5/handlers/ping_handler.dart';
import 'package:sqljocky5/handlers/quit_handler.dart';
import 'package:sqljocky5/handlers/use_db_handler.dart';
import 'package:sqljocky5/prepared_statements/close_statement_handler.dart';
import 'package:sqljocky5/prepared_statements/execute_query_handler.dart';
import 'package:sqljocky5/prepared_statements/prepared_query.dart';
import 'package:sqljocky5/prepared_statements/prepare_handler.dart';
import 'package:sqljocky5/prepared_statements/prepare_ok_packet.dart';
import 'package:sqljocky5/query/query_stream_handler.dart';
import 'package:sqljocky5/query/result_set_header_packet.dart';
import 'package:sqljocky5/results/binary_data_packet.dart';
import 'package:sqljocky5/results/blob.dart';
import 'package:sqljocky5/results/field.dart';
import 'package:sqljocky5/results/results.dart';
import 'package:sqljocky5/results/row.dart';
import 'package:sqljocky5/results/standard_data_packet.dart';
import 'package:sqljocky5/sqljocky.dart';
import 'package:sqljocky5/utils/buffer.dart';
import '../../website/web/classemetier/Client.dart';
import '../../website/web/classemetier/vente.dart';
import '../../website/web/classemetier/achat.dart';
import '../../website/web/classemetier/users.dart';
import '../../website/web/classemetier/produit.dart';
import '../../website/web/classemetier/fournisseur.dart';

import 'package:postgres/postgres.dart';

class AachatVenteCategorieDAO {

  var _Sqlcont = ConnectionSettings(
    user: "caisse",
    password: "8767ce246C",
    host: "localhost",
    port: 3306,
    db: "dart",
  );

  /**
   * @param dtfin :date de fin
   * @param dtb : date de debut
   * @return une liste au forma json du total des achat par categorie entre deux date pour les qui e sont pas des categorie de vente
   */
  Future<List> listecategorietotdnovente(dtfin, dtd) async {
    List<Map> c = new List();
    MySqlConnection conn;
    // create a connection
    var s =_Sqlcont;
    conn = await MySqlConnection.connect(s);
    Results results = (await conn.prepared(
        "SELECT distinct a.id ,a.nom ,if( sum(v.qt)>=(select sum(qt) from achat ) and  1<(select count(qt) from achat ) , 0, sum(v.qt*v.prix))"
            " FROM Categorie a ,facturefour b , achat v"
            " where"
            " ( not exists (select b.id from facturefour b"
            "   where a.id =b.idcategorie) or a.id =b.idcategorie"
            " and v.idfacture=b.id) and type !=1"
            " and b.date between '$dtd' and '$dtfin'"
            " group by a.id"
            ";",
        [1])) as Results;
    results.forEach((Row row) {
      // Access columns by index
      Map fact = {'id': row[0], 'nom': '${row[1]}', 'total': row[2]};
      c.add(fact);
      // new Client(id, prenom, nom, gsm, tva, email, pourcent)
    });
    return c;
  }

  /**
   * @param dtfin :date de fin
   * @param dtb : date de debut
   * @return une liste au forma json du total des Ventes par categorie entre deux date
   */
  Future<List> listecategorietotdvente(dtfin, dtd) async {
    List<Map> c = new List();
    MySqlConnection conn;
    // create a connection
    var s =_Sqlcont;
    conn = await MySqlConnection.connect(s);
    Results results = (await conn.prepared(
        "SELECT distinct a.id ,a.nom ,if( sum(v.qt)>=(select sum(qt) from vente ) , 0, sum(v.qt*v.prix))"
            " FROM Categorie a ,produit p , commade f , vente v"
            "  where"
            " (not exists (select b.id from produit b"
            "   where a.id =b.id_categorie) or a.id =p.id_categorie"
            " and v.Id_commade =f.id and v.id_produit=p.id "
            " and f.date between '$dtd' and '$dtfin' )and  a.type =1"
            " group by a.id"
            ";",
        [1])) as Results;
    results.forEach((Row row) {
      // Access columns by index
      Map fact = {'id': row[0], 'nom': '${row[1]}', 'total': row[2]};
      c.add(fact);
      // new Client(id, prenom, nom, gsm, tva, email, pourcent)
    });
    return c;
  }

  /**
   * @param dtfin :date de fin
   * @param dtb : date de debut
   * @return une liste au forma json du total des achat par categorie entre deux date pour les categorie de vente
   */
  Future<List> listecategorietotdachat(dtfin, dtd) async {
    List<Map> c = new List();
    MySqlConnection conn;
    // create a connection
    var s =_Sqlcont;
    conn = await MySqlConnection.connect(s);
    Results results = (await conn.prepared(
        "SELECT distinct a.id ,a.nom ,if( sum(v.qt)>=(select sum(qt) from achat ) , 0, sum(v.qt))"
            " FROM Categorie a ,facturefour b , achat v"
            " where"
            " ( not exists (select b.id from facturefour b"
            "   where a.id =b.idcategorie) or a.id =b.idcategorie"
            " and v.idfacture=b.id) and type =1"
            " and b.date between '$dtd' and '$dtfin'"
            " group by a.id"
            ";	",
        [1])) as Results;
    results.forEach((Row row) {
      // Access columns by index
      Map fact = {'id': row[0], 'nom': '${row[1]}', 'total': row[2]};
      c.add(fact);
      // new Client(id, prenom, nom, gsm, tva, email, pourcent)
    });
    return c;
  }
}