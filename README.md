Mini ERP - README

Mini ERP is an open-source software solution designed for managing various aspects of business operations, including invoicing, purchasing, and point of sale. It is developed using Dart, with both server-side and client-side components. This README file provides an overview of the project, installation instructions, and other important details.
Features

Mini ERP offers the following key features:

    Invoicing: Create and manage invoices, including generating and sending invoices to customers, tracking payment status, and generating financial reports.

    Purchasing: Manage the procurement process, including creating purchase orders, tracking inventory, managing suppliers, and generating purchase reports.

    Point of Sale: Utilize a user-friendly interface for processing sales transactions, managing inventory, and generating sales reports.

Requirements

Before installing Mini ERP, ensure that you have the following dependencies installed:

    Dart SDK
    Flutter SDK (for the client-side component)
    PostgreSQL database

Installation

To install and run Mini ERP, follow these steps:

    Clone the repository:

shell


shell

cd mini-erp

    Set up the server-side component:
        Navigate to the server directory: cd server
        Install the required dependencies: dart pub get
        Configure the database connection in lib/config.dart
        Run the server: dart bin/server.dart

    Set up the client-side component:
        Navigate to the client directory: cd ../client
        Install the required dependencies: flutter pub get
        Configure the server URL in lib/constants.dart
        Run the app: flutter run

    Access the Mini ERP client application in your browser at http://localhost:3000.

Configuration

Mini ERP uses environment variables for configuration. Customize the configuration by creating a .env file in the project root and providing the necessary details.
Contributing

Contributions to Mini ERP are welcome! If you'd like to contribute

License

Mini ERP is released under the MIT License. See LICENSE for more information.
